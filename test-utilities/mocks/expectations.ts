export function expectToThrow<TError extends Error>(
    act: () => unknown,
    expectedErrorType?: new(...args: any[]) => TError,
): TError {
    try {
        act();
    } catch (error) {
        return verifyReceivedError(error, expectedErrorType);
    }
    return onNothingThrown();
}

export async function expectToThrowAsync<TError extends Error>(
    act: () => Promise<unknown>,
    expectedErrorType?: new(...args: any[]) => TError,
): Promise<TError> {
    try {
        await act();
    } catch (error) {
        return verifyReceivedError(error, expectedErrorType);
    }
    onNothingThrown();
}

function verifyReceivedError<TError extends Error>(
    error: any,
    expectedErrorType: (new(...args: any[]) => TError) | undefined,
): TError {
    if (expectedErrorType) {
        expect(error.constructor.name).toBe(expectedErrorType.name);
    } else {
        expect(error).toBeInstanceOf(Error);
    }
    return error;
}

function onNothingThrown(): never {
    throw new Error(`Expected given function to throw Error but nothing was thrown.`);
}

export async function expectNext<T>(iterator: AsyncIterator<T>, expected: T): Promise<void> {
    const result = await iterator.next();
    expect(result.value).toBe(expected); // Check first to get nicer error.
    expect(result.done).toBeFalsy();
}

export async function expectNextDone(iterator: AsyncIterator<unknown>): Promise<void> {
    const result = await iterator.next();
    expect(result.value).toBeUndefined(); // Check first to get nicer error.
    expect(result.done).toBeTruthy();
}

export function describeMember<TTarget>(memberName: keyof TTarget & string, runTests: () => void): void {
    describe(memberName, () => {
        runTests();
    });
}

export function describeMemberFactory<TTarget>(): ((memberName: keyof TTarget & string, runTests: () => void) => void) {
    return (memberName, runTests) => describeMember<TTarget>(memberName, runTests);
}
