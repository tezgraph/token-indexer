import { BigMapInfo, Contract, MichelsonSchema } from '@tezos-dappetizer/indexer';

export function prepareContract(bigMapInfo: BigMapInfo[]): Contract {
    const contract = {
        address: 'KT',
        parameterSchemas: {
            entrypoints: {} as Record<string, MichelsonSchema>,
        },
    } as Contract;

    Object.defineProperty(contract, 'bigMaps', { get: () => bigMapInfo });
    return contract;
}

export function prepareUnknownBigMap(): BigMapInfo {
    return {
        name: 'custom_ledger',
    } as BigMapInfo;
}
