# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [1.2.1](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.2.0...v1.2.1) (2023-09-25)


### Bug Fixes

* Fixed intermittent issue of `Ledger` changes not being indexed for some contracts. It used to happen when a contract object expired in the cache. ([2c9cd51](https://gitlab.com/tezos-dappetizer/token-indexer/commit/2c9cd5193482a92ec75358210f0b67ff92e773c8))






# [1.2.0](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.1.1...v1.2.0) (2023-07-23)


### Features

* Optimized applying of changes to the database by skipping empty operations. ([b7448a6](https://gitlab.com/tezos-dappetizer/token-indexer/commit/b7448a681ff1d9626a05fc295685a944fa5ec520))
* Specified `[@tezos-dappetizer](https://gitlab.com/tezos-dappetizer)` and `typeorm` as peer dependencies as far as only one shared version must be installed in the indexer app. ([81da0e1](https://gitlab.com/tezos-dappetizer/token-indexer/commit/81da0e1c33d139bbf0073cbf14cf9bdc35951715))
* Upgraded to `@taquito` 17.0.0. ([5b3c4ae](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5b3c4aec8783734fb9092a9cab39cd195495d4d5))






## [1.1.1](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.1.0...v1.1.1) (2023-05-31)


### Bug Fixes

* Fixed token indexing if `option` Tezos type is used because of [the breaking change in [@taquito](https://gitlab.com/taquito) 16.2.0](https://github.com/ecadlabs/taquito/releases/tag/16.2.0). ([fa9ec40](https://gitlab.com/tezos-dappetizer/token-indexer/commit/fa9ec408e8cc0869aa7a89d4c9522db68c60734a))






# [1.1.0](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.0.3...v1.1.0) (2023-05-26)


### Bug Fixes

* Metrics reporting for TZIP metadata has issues: ([c950267](https://gitlab.com/tezos-dappetizer/token-indexer/commit/c95026796a55d109444f8cec99895ec0ef0ebc3b))
    * If metadata parsing fails, then both `Ok` and `Failed` are reported.
    * If metadata are no found, then nothing is reported.
    * If download fails, then duration is not reported.
    * Duration does not include metadata parsing.


### Features

* Added explicit configurable `contractNames` that should be indexed. They reference globally configured selective indexing in `IndexingDappetizerConfig.contracts`. ([a3ac364](https://gitlab.com/tezos-dappetizer/token-indexer/commit/a3ac364cdc91fb41921628cfaeff6d0158ad5cbc))
* Optimized memory usage by disposing contract metadata which were not used in recent blocks. ([5950412](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5950412af495250c58e7e6b99fb3e43a7846cb0a))
* Upgraded `@taquito` to 16.1.1. ([71d13a7](https://gitlab.com/tezos-dappetizer/token-indexer/commit/71d13a7b275e3f6e05087667937987acdb96b420))
* Upgraded to `@tezos-dappetizer` 2.8.0. ([89b579f](https://gitlab.com/tezos-dappetizer/token-indexer/commit/89b579fdeee824d72298efcc26e53c161fc73205))


### Reverts

* Revert "chore: Upgraded to dappetizer alpha packages" ([8d46cf3](https://gitlab.com/tezos-dappetizer/token-indexer/commit/8d46cf32aff4269198396fe387a874224730f737))
* Revert "chore: Upgraded to dappetizer alpha packages" ([7e164b9](https://gitlab.com/tezos-dappetizer/token-indexer/commit/7e164b9e7d63132b632628990e5e9f840d3fc476))






## [1.0.3](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.0.2...v1.0.3) (2022-10-04)


### Features

* Upgraded to `[@taquito](https://gitlab.com/taquito)` 14.0.0. ([da58e08](https://gitlab.com/tezos-dappetizer/token-indexer/commit/da58e088eca8e097d23025194269ff2cbad82b9c))
* Upgraded to `[@tezos-dappetizer](https://gitlab.com/tezos-dappetizer) 2.0.0` and all related dependencies. ([bbc76c4](https://gitlab.com/tezos-dappetizer/token-indexer/commit/bbc76c477d0fdd1281af9da24174bd4d8ff354f2))






## [1.0.2](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.0.0...v1.0.2) (2022-05-04)


### Features

* Upgraded dappetizer to v1.3.0. ([ddb199b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/ddb199b89f36a0bdbb31ae8fda14fb771aea8fc2))






## [1.0.1](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v1.0.0...v1.0.1) (2022-05-04)


### Features

* Added indexes for reorgs. ([6f97a44](https://gitlab.com/tezos-dappetizer/token-indexer/commit/6f97a44544cf56b18d95c1e4782aa93cf7dcbde8))
* Storing full token metadata in `metadata` column as JSON. ([504d630](https://gitlab.com/tezos-dappetizer/token-indexer/commit/504d630d5744c01adbd285e6699f3003f39f3701))





# [1.0.0](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v0.0.8...v1.0.0) (2022-04-05)

**Note:** Version bump only for package token-indexer






## [0.0.8](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v0.0.7...v0.0.8) (2022-04-04)


### Bug Fixes

* Fixed variable name and typo in feature. ([a31afbb](https://gitlab.com/tezos-dappetizer/token-indexer/commit/a31afbbefd9412059b823bee5b9869c1a4a02224))
* Renamed metric labels ([fd80154](https://gitlab.com/tezos-dappetizer/token-indexer/commit/fd801540a3a00b0293cd99ae9ea7ca8624f59c86))





## [0.0.7](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v0.0.6...v0.0.7) (2022-02-08)


### Bug Fixes

* Fixed dappetizer upgrade. ([73a5d00](https://gitlab.com/tezos-dappetizer/token-indexer/commit/73a5d00a7ba666483732955c388fd10a0af95585))


### Features

* Added get metadata metrics. ([08a6279](https://gitlab.com/tezos-dappetizer/token-indexer/commit/08a6279c66c2006145e0134d59e47cd88f7e606d))
* Added universal ledger processor. ([0498fe2](https://gitlab.com/tezos-dappetizer/token-indexer/commit/0498fe26fe38b346edda7e6f5b228ca37fb69b9b))
* Applied modules config. ([8d6a901](https://gitlab.com/tezos-dappetizer/token-indexer/commit/8d6a901b900f77c85f16748a453a631a7282fe17))
* Use temporary dir for temporary files. ([c5fda3e](https://gitlab.com/tezos-dappetizer/token-indexer/commit/c5fda3ec5b171819e8e109d37260b009dee09334))






## [0.0.6](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v0.0.5...v0.0.6) (2022-01-10)


### Bug Fixes

* duplicate MultiAssetSymbolMintProcessor ([50f83be](https://gitlab.com/tezos-dappetizer/token-indexer/commit/50f83bef98f99c14e4dae22dfe45218cc0cdfa82))
* Fixed missing address to in mint parameters. ([eb122f1](https://gitlab.com/tezos-dappetizer/token-indexer/commit/eb122f198493762cef03c3c195a97dc95a8a852d))
* Fixed missing indexing of `Burn` feature flag. ([898e48e](https://gitlab.com/tezos-dappetizer/token-indexer/commit/898e48e666cb865b169912418265f3ae9703b992))


### Features

* Added balance index. ([089264b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/089264b99fa62e66b58c1130940e7ec86d4b372c))
* Added defaulting to the unknown type in catch variables. ([9740012](https://gitlab.com/tezos-dappetizer/token-indexer/commit/97400120e5577de9e0cfa606777b4e65fbc06b57))
* Added index column to db action. ([6578865](https://gitlab.com/tezos-dappetizer/token-indexer/commit/657886518645bc8b5fbe5fd4c336f5691d3e9644))
* Added indexes. ([5e227ab](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5e227ab3db57bc86ec1c029986ad5c23ebe7970b))
* Added version info to DI. ([d25bc8b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/d25bc8b4a2a8ef40617d553e5822d092f66ed090))
* Completing all pending token metadata requests when configured `toBlockLevel` is reached. ([8266f31](https://gitlab.com/tezos-dappetizer/token-indexer/commit/8266f311ff4bf1689e6dd833f06b1e537f920020))
* Extended logging during indexer start. ([93af009](https://gitlab.com/tezos-dappetizer/token-indexer/commit/93af0099359ee11a99437649c941fcd77889efc7))
* Refactored mint and burn processors to universal approach. ([0e4dca7](https://gitlab.com/tezos-dappetizer/token-indexer/commit/0e4dca7fc80fae12523788a75016867669cc2c6a))
* Renamed feature flag nft . ([af8d650](https://gitlab.com/tezos-dappetizer/token-indexer/commit/af8d650ca6fbc6240e074dd5917e198261dfb398))
* Support for generic matching of contract schema(s) e.g. with annotations. ([bc06d92](https://gitlab.com/tezos-dappetizer/token-indexer/commit/bc06d9288a787cba8ffd645a6075814d5f222442))






## [0.0.5](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v0.0.4...v0.0.5) (2021-11-16)


### Bug Fixes

* Fixed dependency injection in `DbTokensUpdater`. ([fee0846](https://gitlab.com/tezos-dappetizer/token-indexer/commit/fee0846afc768c58a0934a3c298d494d44211819))
* Fixed indexing of NFT balances. ([1c23296](https://gitlab.com/tezos-dappetizer/token-indexer/commit/1c23296d765598d0cdb4a821430db07ae979190b))
* Fixed race condition between block indexing and token metadata retrieval. ([5ee4e50](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5ee4e5042a8b36a1d1ad3663e6d272a980bcd002))
* Fixed token id parameter type. ([5574e39](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5574e39c7a7708424d41e674452ced63581e6ea9))
* Fixed zero balance amount. ([7c2e7ce](https://gitlab.com/tezos-dappetizer/token-indexer/commit/7c2e7ce95c485f08e7eba68f2f9cc3bb915ef89a))
* Removed `DbCurrentBalance` because TypeORM is failing to be synchronize it after app restart. ([90d8771](https://gitlab.com/tezos-dappetizer/token-indexer/commit/90d877103703103d9bef36aa16c3c10b0bd55568))


### Features

* Added current balance view handling. ([cecc519](https://gitlab.com/tezos-dappetizer/token-indexer/commit/cecc519243afc1324a9dd52a4177d2708051b629))
* Added queue for token metadata updating on background. ([70f7188](https://gitlab.com/tezos-dappetizer/token-indexer/commit/70f7188f61c10eaf76a3326c955085c8c184c12b))
* Added tezos domains processor. ([3eb12f0](https://gitlab.com/tezos-dappetizer/token-indexer/commit/3eb12f0aeb0d25954ed822a04a27bbb1b48801c3))
* Added tzBTC token metadata. ([e07e866](https://gitlab.com/tezos-dappetizer/token-indexer/commit/e07e86672ee38b9c51f7446befdea2d29c33033a))
* Added version stamping. ([664406b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/664406b1ae990e9116a3409c203b07b19d9d045e))
* Fetching token metadata on the background from block indexing. ([9f82d49](https://gitlab.com/tezos-dappetizer/token-indexer/commit/9f82d4910895612ae9f031f691a424abf8836f92))
* Historic balances and support for rollback on reorg. ([a052e23](https://gitlab.com/tezos-dappetizer/token-indexer/commit/a052e234bd5283c5210bd0c088cc8091576b9cc9))
* Refactored enums. ([6c5c60f](https://gitlab.com/tezos-dappetizer/token-indexer/commit/6c5c60f35a8a2a1ceed0414fcff3caefc99e8a82))
* Removed unnecessary processors. ([e35ed59](https://gitlab.com/tezos-dappetizer/token-indexer/commit/e35ed594c1277c26a9cc0cb1533dfcd27d6711b8))
* Split FA2 and FA1_2 into strict flags ([0b86c0f](https://gitlab.com/tezos-dappetizer/token-indexer/commit/0b86c0f99025386af67977eb86740532c89ed875))






## [0.0.3](https://gitlab.com/tezos-dappetizer/token-indexer/compare/v0.0.2...v0.0.3) (2021-10-05)


### Bug Fixes

* Adapted db entities refactor. ([5ec5376](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5ec5376112755df9920d6dabba09c649888624f4))
* Added safe JSON serialization of `IndexedContract`. ([5eb42f9](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5eb42f9d34648b1df0b52e8ab69222c10fb3aaf6))
* Fixed config for `tokenIndexer.logIncompatibleContracts`. ([9e25732](https://gitlab.com/tezos-dappetizer/token-indexer/commit/9e25732fcbe5a2b544375e252f5df7d071cc8b1c))
* Fixed DbContractFeature by adding foreing key and proper limit for feature string length. ([2cbdd5e](https://gitlab.com/tezos-dappetizer/token-indexer/commit/2cbdd5ecc3d055c66b4dd82e1f497936ce769f17))
* Fixed failing deletion of balances because of BigNumber serialization. ([6ebd510](https://gitlab.com/tezos-dappetizer/token-indexer/commit/6ebd510d4226a020b1c6be859a10672e282f2e55))
* Fixed incompatible processors. ([08ce44e](https://gitlab.com/tezos-dappetizer/token-indexer/commit/08ce44e140019d7b3859bcf1ad5288131c3b750e))
* Fixed logging of incompatible contracts. ([9ff63a2](https://gitlab.com/tezos-dappetizer/token-indexer/commit/9ff63a2f94373931e737bae5501725e742c80d63))
* Fixed nullish `decimals` property in token metadata despite in TS schema it is non-nullish. ([4b9d9e5](https://gitlab.com/tezos-dappetizer/token-indexer/commit/4b9d9e5919dad54a0a426db81984c759dac6e404))
* Fixed processors. ([b88ff0d](https://gitlab.com/tezos-dappetizer/token-indexer/commit/b88ff0d5218b710cd5870388bb0c38145dd53719))
* Fixed processors. ([70957af](https://gitlab.com/tezos-dappetizer/token-indexer/commit/70957af27c6cdf3ef92cea9600525a2cdac838ca))
* Fixed processors. ([5a0e3fa](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5a0e3fa12aa74edc97cdeac2d659ed3668fe6949))
* Fixed token metadata errors and root value schema processors. ([88a6a4c](https://gitlab.com/tezos-dappetizer/token-indexer/commit/88a6a4c3537d2b543d918e303e636710d34b6627))
* Fixed typo in dappetizer.config.js. ([97d1028](https://gitlab.com/tezos-dappetizer/token-indexer/commit/97d102855be6fdc46b8b8092f642e0f44a485dd7))
* Hotfixed upgrade to new app-indexer alpha package. ([86f9d08](https://gitlab.com/tezos-dappetizer/token-indexer/commit/86f9d08f49fde1e0193c17ee5c5ed8209c0eda73))
* Hotfixed upgrade to new app-indexer alpha package. ([e7c46c8](https://gitlab.com/tezos-dappetizer/token-indexer/commit/e7c46c85c614ea35790e64b35cd00eb1501e2a65))
* Hotfixed upgrade to new app-indexer alpha package. ([86f2a03](https://gitlab.com/tezos-dappetizer/token-indexer/commit/86f2a0314229896f5e00e8dbcee086a307df51e5))
* Parsing token metadata decimals if it comes as a string. ([3c86cef](https://gitlab.com/tezos-dappetizer/token-indexer/commit/3c86ceff94441474232ef382bd319bbafacd80b3))


### Features

* Added collecting of contract features based on matched processors. Also added contract metadata: name and description. ([255f1f1](https://gitlab.com/tezos-dappetizer/token-indexer/commit/255f1f1f5d26fc051a54698782895723e4eb9411))
* Added incompatible mint, burn, transfer and ledger processors. ([e59d2a6](https://gitlab.com/tezos-dappetizer/token-indexer/commit/e59d2a61e0dac06f04070fa26ed42e9d5b98ca5b))
* Added missing contract metadata. ([3b74df8](https://gitlab.com/tezos-dappetizer/token-indexer/commit/3b74df85b8c0573dcf2e6f709485965b05eda899))
* Added reporting incompatible contracts with configured threshold. ([ec98554](https://gitlab.com/tezos-dappetizer/token-indexer/commit/ec985546fe9f56eae4d6b6a05299ff23e4473e21))
* Added warning about long DB and RPC calls. ([e61daaf](https://gitlab.com/tezos-dappetizer/token-indexer/commit/e61daaf0b46173e08a0db77ff03fae55087c813e))
* If contract or token metadata loading failed then nulls are stored to db with an error details. ([33fa5f4](https://gitlab.com/tezos-dappetizer/token-indexer/commit/33fa5f46268f69d960d5245c99164b4ff972a1a8))
* Indexing only FA contracts. ([75bfb94](https://gitlab.com/tezos-dappetizer/token-indexer/commit/75bfb946e86d4e55d4e064fc59b570329133cfa8))
* Running demo as web application. ([101e43a](https://gitlab.com/tezos-dappetizer/token-indexer/commit/101e43a72153c90d0e68cbe6726d1ea4fd2b5da1))
* Split contract feature to a dedicated DB table. ([edb3a59](https://gitlab.com/tezos-dappetizer/token-indexer/commit/edb3a59589940e1f533b051653815f4b94da3f2f))
* Upgraded [@app-indexer](https://gitlab.com/app-indexer) therefore added context with more properties to indexer classes. ([fe07e3c](https://gitlab.com/tezos-dappetizer/token-indexer/commit/fe07e3ccc38f5737759b8a57dac87c045a6240e7))
* Upgraded [@app-indexer](https://gitlab.com/app-indexer) to 0.0.4-alpha.23. ([90d90d0](https://gitlab.com/tezos-dappetizer/token-indexer/commit/90d90d00a827ed05475cb6b16f18b4b41801d2b2))
* Upgraded to newer [@app-indexer](https://gitlab.com/app-indexer) which uses dappetizer.config.js for a configuration. ([f4f76cc](https://gitlab.com/tezos-dappetizer/token-indexer/commit/f4f76cc2cee32663c51f1cbb8d555fb4c2a4e4c3))


### Reverts

* Revert "chore: Used error unknown type defaulting." ([52f0fdd](https://gitlab.com/tezos-dappetizer/token-indexer/commit/52f0fdd28fe3c0a601f42ddc3d460acf135ea5c3))






## 0.0.1 (2021-08-04)


### Bug Fixes

* Clean up ([f0586bf](https://gitlab.com/tezos-dappetizer/token-indexer/commit/f0586bf44c5f2662e2ad0c716290478bf5489d4a))
* Clean up ([8fb3a75](https://gitlab.com/tezos-dappetizer/token-indexer/commit/8fb3a7511091c9ca5f0ffe59f2cdb336b3584f66))
* Clean up ([90cd5da](https://gitlab.com/tezos-dappetizer/token-indexer/commit/90cd5dab83b6d17be9137c88ce54f96029682538))
* Clean up ([8741daf](https://gitlab.com/tezos-dappetizer/token-indexer/commit/8741daf51782ab58dcc42129fa1e655d91ac213b))
* Fixed `BigNumber` deserialization. ([4725b55](https://gitlab.com/tezos-dappetizer/token-indexer/commit/4725b55e4734acf4c8efb57359eb5580b62d8995))
* Fixed env variables for docker image ([596ed97](https://gitlab.com/tezos-dappetizer/token-indexer/commit/596ed97400688f7ced292249f8299fe718c2e322))
* Fixed indexing of multi-asset ledger. ([0c331d7](https://gitlab.com/tezos-dappetizer/token-indexer/commit/0c331d7d06ecf34b3b1db9abd3dc1a940d6900fb))
* Fixed integrity hashs ([4cb4189](https://gitlab.com/tezos-dappetizer/token-indexer/commit/4cb4189444b1fe957f1dd942fb0922a241ddcf7c))
* Fixed Michelson `BasedOn`. ([d58453b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/d58453b13bf20997a52add1b92ed9fa75b739eb4))
* Fixed missing lerna.json ([7e420ac](https://gitlab.com/tezos-dappetizer/token-indexer/commit/7e420ac9a35031dee1b9c05c4c4f6fc7fec21607))
* Made repo to contain package and demo only ([84a6100](https://gitlab.com/tezos-dappetizer/token-indexer/commit/84a6100f901eece5f517423ae647e88d83e97647))
* Made repo to contain package and demo only ([7f62b5d](https://gitlab.com/tezos-dappetizer/token-indexer/commit/7f62b5d314f44f5c1b18ab612637db0f9c3ee912))
* Made repo to contain package and demo only ([d30803c](https://gitlab.com/tezos-dappetizer/token-indexer/commit/d30803c2a4a1692a07553420ecbcb32bef06a216))
* Made repo to contain package and demo only ([f463458](https://gitlab.com/tezos-dappetizer/token-indexer/commit/f46345851a571ce9672c0287afe88aa314f67147))
* Redo build process ([8907cc1](https://gitlab.com/tezos-dappetizer/token-indexer/commit/8907cc1d3ac7486dfe755c3874de1d80e16e8375))
* Redo build process ([309b1d3](https://gitlab.com/tezos-dappetizer/token-indexer/commit/309b1d33fb1110da1b49f9ea20e04f54ab6a4dfd))
* Redo build process ([c5b3ba1](https://gitlab.com/tezos-dappetizer/token-indexer/commit/c5b3ba1656e20c55585aa81c388f3f0389cf2002))
* Redo build process ([7c7fbbc](https://gitlab.com/tezos-dappetizer/token-indexer/commit/7c7fbbc0db0d5f2b0f0b73b5722a0f3fd7980d96))


### Features

* Added (unfortunatelly not operational) indexing of *mint*. ([5b0fa0d](https://gitlab.com/tezos-dappetizer/token-indexer/commit/5b0fa0d58bf5c969ae905fb55235c9249b5266f2))
* Added `FA1_2` and `FA2` to `ContractFeature`. ([670a7fc](https://gitlab.com/tezos-dappetizer/token-indexer/commit/670a7fc2ad3f9ddf72620986ec19703b2adac0ce))
* Added `FA1_2` and `FA2` to `ContractFeature`. ([da6c2e6](https://gitlab.com/tezos-dappetizer/token-indexer/commit/da6c2e6e12cbe5b799e55330cd2fc50747f621b9))
* Added indexing of *transfer* smart contract calls. ([075aad9](https://gitlab.com/tezos-dappetizer/token-indexer/commit/075aad9263a7c584e352ad201b31e3a60b25baba))
* Added TZIP-7 ledger indexing. ([05858e0](https://gitlab.com/tezos-dappetizer/token-indexer/commit/05858e082eece4bb3a525b32c582024cd91867d2))
* Implemented *transfer* indexing correctly. ([e078e67](https://gitlab.com/tezos-dappetizer/token-indexer/commit/e078e674492650628ea4269c8e2891c2622aebee))
* Implemented transfer indexing correctly and also for TZIP-7. ([4a8634a](https://gitlab.com/tezos-dappetizer/token-indexer/commit/4a8634aa1d51c69f5187fb3e967a17afd84f4297))
* Improved error handling if failed to get token or contract metadata. If it's persistent error then it's logged as debug. ([02bc3cc](https://gitlab.com/tezos-dappetizer/token-indexer/commit/02bc3ccd48287a4503681677371b4127894a8e29))
* Improved ledger indexing if it has different annotations. ([dc18fb6](https://gitlab.com/tezos-dappetizer/token-indexer/commit/dc18fb6a300f02e0bc43a1d4b578e431ddf81de9))
* Improved origination to get more data - features and all tokens if available. ([23253ac](https://gitlab.com/tezos-dappetizer/token-indexer/commit/23253ac46d14ed18aebdbbb3d975bb2497ecf0de))
* Indexing block by block from configured `INDEX_FROM_BLOCK_LEVEL`. ([d0b610b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/d0b610b3b0ba5ad6ce2ca79b565c20c647080118))
* Indexing burns. ([f625dfb](https://gitlab.com/tezos-dappetizer/token-indexer/commit/f625dfb562ce1ccb4f14be13beab261343c26bbb))
* Indexing mints. ([16eb51b](https://gitlab.com/tezos-dappetizer/token-indexer/commit/16eb51be83231c5383b7e9d3d9227ddd507e8359))
* Initial impl of *ledger* indexing. ([f30c309](https://gitlab.com/tezos-dappetizer/token-indexer/commit/f30c3093458345b37b4cd6d38ce4b5e1c76a4c2e))
* Minor improvements esp. in origination indexing. ([382af37](https://gitlab.com/tezos-dappetizer/token-indexer/commit/382af378612f6295387d2fadde45c2c3e73b9f5c))
* Not indexing contracts that can't have any tokens. ([ab7e14d](https://gitlab.com/tezos-dappetizer/token-indexer/commit/ab7e14d70ae3245310b9b40e0aed34772a43f400))


### Reverts

* Revert "chore:release" ([6fe485f](https://gitlab.com/tezos-dappetizer/token-indexer/commit/6fe485f08538bb0bd8a0dfc68c2b26632e6b6f37))
