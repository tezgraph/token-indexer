import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
import { TokenIndexerModuleConfig } from './packages/token-indexer/src/public-api';

const tokenIndexerModuleConfig: TokenIndexerModuleConfig = {
    id: '@tezos-dappetizer/token-indexer',
    config: {
        logIncompatibleContracts: true,
        logIncompatibleContractsThreshold: 100,
    },
};

const config: DappetizerConfigUsingDb = {
    modules: [tokenIndexerModuleConfig],
    networks: {
        mainnet: {
            indexing: {
                fromBlockLevel: 889027,
                retryIndefinitely: false,
            },
            tezosNode: {
                url: 'https://prod.tcinfra.net/rpc/mainnet/',
                // 'https://mainnet-tezos.giganode.io',
            },
        },
    },
    database: {
        type: 'postgres',
        host: 'localhost',
        port: 5432,
        username: 'postgres',
        password: 'postgrespassword',
        database: 'postgres',
        schema: 'token_indexer_demo',
    },
    logging: {
        file: {
            minLevel: 'information',
            path: 'logs/log.jsonl',
            maxSize: '100m',
            maxFiles: '7d',
        },
    },
    httpServer: {
        enabled: true,
    },
};

export default config;
