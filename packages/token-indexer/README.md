# Tezos Dappetizer Token Indexer package

`@tezos-dappetizer/token-indexer` is an npm package that provides developers token indexer module for dappetizer. 

The `registerTokenIndexer` method registers dappetizer token indexer related components to run indexer.

```ts
import { registerTokenIndexer } from '@tezos-dappetizer/token-indexer';

registerTokenIndexer();
```

See the top-level [https://gitlab.com/tezos-dappetizer/dappetizer](https://gitlab.com/tezos-dappetizer/dappetizer) file for details on reporting issues, contributing and versioning.

## API Documentation

GitBook style documentation is available on-line [here](https://docs.dappetizer.dev/command-line)

## Disclaimer

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.