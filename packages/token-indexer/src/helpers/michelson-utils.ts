import { createPrim, isRawValue, Michelson } from '@tezos-dappetizer/indexer';
import { isReadOnlyArray } from '@tezos-dappetizer/utils';

export function removeAnnots(michelson: Michelson): Michelson {
    if (isRawValue(michelson)) {
        return michelson;
    }
    if (isReadOnlyArray(michelson)) {
        return michelson.map(removeAnnots);
    }

    const argsNoAnnots = michelson.args?.map(removeAnnots);
    return createPrim(michelson.prim, argsNoAnnots, undefined); // Annots are omitted.
}
