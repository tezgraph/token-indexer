import { hasConstructor, isNullish, isWhiteSpace, Nullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { isString } from 'lodash';

const seeStack = 'See the error stack for more details about the value.';

export const rpcGuard = {
    object<T extends object>(value: Nullish<T>): T {
        if (typeof value !== 'object' || value === null) {
            throw new Error(`Value must be an object. ${seeStack}`);
        }
        return value;
    },

    notNullish<T>(value: Nullish<T>): T {
        if (isNullish(value)) {
            throw new Error(`Value cannot be nullish. ${seeStack}`);
        }
        return value;
    },

    notWhiteSpace(value: Nullish<string>): string {
        if (isWhiteSpace(value)) {
            throw new Error(`The string cannot be nullish nor white-space. ${seeStack}`);
        }
        return value;
    },

    string(value: string): string {
        return validate(value, 'string', isString);
    },

    integer(value: number): number {
        return validate(value, 'integer', isInteger);
    },

    positiveBigNumberInteger(value: BigNumber): BigNumber {
        return validate(value, 'BigNumber integer greater than or equal to zero', isPositiveBigNumberInteger);
    },
};

function isPositiveBigNumberInteger(value: unknown): value is BigNumber {
    return hasConstructor(value, BigNumber)
        && value.isInteger()
        && !value.isNegative();
}

function isInteger(value: unknown): value is number {
    return typeof value === 'number'
        && (value % 1) === 0;
}

function validate<T>(
    value: unknown,
    expectedInfo: string,
    isValid: (value: unknown) => value is T,
): T {
    if (!isValid(value)) {
        throw new Error(`The value must be ${expectedInfo} but it is ${JSON.stringify(value)} of type ${typeof value}. ${seeStack}`);
    }
    return value;
}
