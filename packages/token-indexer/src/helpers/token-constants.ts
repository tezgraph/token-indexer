import BigNumber from 'bignumber.js';

export const SINGLE_ASSET_TOKEN_ID = new BigNumber(0);

export const NFT_DEFAULT_AMOUNT = new BigNumber(1);
