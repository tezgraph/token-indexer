import { StrictExtract } from 'ts-essentials';

export type PickChangingType<TObject, TPropertyName extends keyof TObject, TResultProperty> =
    Record<StrictExtract<keyof TObject, TPropertyName>, TResultProperty>;
