import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';
import { InjectionToken } from 'tsyringe';

import { HardcodedTokenMetadataProvider } from './hardcoded-token-metadata-provider';
import { TokenMetadata } from './token-metadata';
import { TzipMetadataProvider } from '../helpers/tzip-metadata-provider';

export type TokenMetadataProvider = TzipMetadataProvider<TokenMetadata, [ContractAbstraction, BigNumber]>;

export const TOKEN_METADATA_PROVIDER_DI_TOKEN: InjectionToken<TokenMetadataProvider> = HardcodedTokenMetadataProvider;
