import {
    DB_DATA_SOURCE_PROVIDER_DI_TOKEN,
    DB_EXECUTOR_DI_TOKEN,
    DbDataSourceProvider,
    DbExecutor,
} from '@tezos-dappetizer/database';
import { CONTRACT_PROVIDER_DI_TOKEN, ContractProvider } from '@tezos-dappetizer/indexer';
import { injectLogger, Logger } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { TokenMetadataProvider, TOKEN_METADATA_PROVIDER_DI_TOKEN } from './token-metadata-provider';
import { UpdateTokenMetadataCommand } from '../../db-updaters/commands/update-token-metadata-command';
import { DbTokenKey } from '../../entities/db-token';

@singleton()
export class TokenMetadataBackgroundUpdater {
    constructor(
        @inject(CONTRACT_PROVIDER_DI_TOKEN) private readonly contractProvider: ContractProvider,
        @inject(TOKEN_METADATA_PROVIDER_DI_TOKEN) private readonly tokenMetadataProvider: TokenMetadataProvider,
        @inject(DB_EXECUTOR_DI_TOKEN) private readonly dbExecutor: DbExecutor,
        @inject(DB_DATA_SOURCE_PROVIDER_DI_TOKEN) private readonly dbDataSourceProvider: DbDataSourceProvider,
        @injectLogger(TokenMetadataBackgroundUpdater) private readonly logger: Logger,
    ) {}

    async update(tokenKey: DbTokenKey): Promise<void> {
        await this.logger.runWithData({ updatedToken: tokenKey }, async () => {
            this.logger.logDebug('Updating metadata for {updatedToken}.');

            try {
                const contract = await this.contractProvider.getContract(tokenKey.contractAddress);
                const metadata = await this.tokenMetadataProvider.get(contract.abstraction, tokenKey.id);

                const dbDataSource = await this.dbDataSourceProvider.getDataSource();
                await this.dbExecutor.execute(dbDataSource.manager, new UpdateTokenMetadataCommand(tokenKey, metadata));

                this.logger.logDebug('Updated metadata for {updatedToken}.');
            } catch (error) {
                this.logger.logError('Failed to update metadata of {updatedToken}. {error}', { error });
            }
        });
    }
}
