import * as taquitoTzip12 from '@taquito/tzip12';
import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import {
    Constructor,
    errorToString,
    hasConstructor,
    injectLogger,
    Logger,
    runInPerformanceSection,
} from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { singleton } from 'tsyringe';

import { emptyTokenMetadata, TokenMetadata } from './token-metadata';
import { TokenMetadataProvider } from './token-metadata-provider';
import { MetadataState } from '../../entities/metadata-state';
import { rpcGuard } from '../../helpers/rpc-guard';
import { MetadataSanitizer } from '../helpers/metadata-sanitizer';

const notFoundErrorTypes: readonly Constructor[] = [
    taquitoTzip12.TokenMetadataNotFound,
    taquitoTzip12.TokenIdNotFound,
];

@singleton()
export class Tzip12TokenMetadataProvider implements TokenMetadataProvider {
    constructor(
        private readonly sanitizer: MetadataSanitizer,
        @injectLogger(Tzip12TokenMetadataProvider) private readonly logger: Logger,
    ) {}

    async get(contract: ContractAbstraction, tokenId: BigNumber): Promise<TokenMetadata> {
        try {
            if (tokenId.isGreaterThan(new BigNumber(Number.MAX_SAFE_INTEGER))) {
                throw new Error(`Specified token ID is greater than the max safe integer ${Number.MAX_SAFE_INTEGER}`
                    + ` which is not supported by @taquito API.`);
            }

            return await runInPerformanceSection(this.logger, async () => {
                const rawMetadata = await contract.tzip12().getTokenMetadata(tokenId.toNumber());
                return this.createMetadata(rawMetadata);
            });
        } catch (error) {
            if (notFoundErrorTypes.some(t => hasConstructor(error, t))) {
                this.logger.logDebug('Received not-found response for {token} of {contract} hence using empty metadata.', {
                    token: tokenId,
                    contract: contract.address,
                });
                return emptyTokenMetadata.Ok;
            }

            this.logger.logError('Failed to get TZIP-12 metadata for {token} of {contract}. {error}', {
                token: tokenId,
                contract: contract.address,
                error,
            });
            return emptyTokenMetadata.Failed;
        }
    }

    private createMetadata(rawMetadata: taquitoTzip12.TokenMetadata): TokenMetadata {
        try {
            rpcGuard.object(rawMetadata);
            return {
                metadataState: MetadataState.Ok,
                metadata: JSON.stringify(rawMetadata),
                decimals: this.sanitizer.sanitizeInteger(rawMetadata.decimals),
                name: this.sanitizer.sanitizeString(rawMetadata.name),
                symbol: this.sanitizer.sanitizeString(rawMetadata.symbol),
            };
        } catch (error) {
            const details = errorToString(error, { onlyMessage: true });
            throw new Error(`Failed to deserialize metadata from response: ${JSON.stringify(rawMetadata)}\n${details}`);
        }
    }
}
