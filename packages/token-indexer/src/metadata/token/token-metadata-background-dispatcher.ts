import { singleton } from 'tsyringe';

import { TokenMetadataBackgroundUpdater } from './token-metadata-background-updater';
import { TokenIndexerInternalConfig } from '../../config/token-indexer-internal-config';
import { DbTokenKey } from '../../entities/db-token';
import { TokenIndexerMetrics } from '../../helpers/token-indexer-metrics';

@singleton()
export class TokenMetadataBackgroundDispatcher {
    private readonly allCompletedCallbacks: (() => void)[] = [];
    private readonly tokenKeysWaiting: DbTokenKey[] = [];
    private updatingCount = 0;

    constructor(
        private readonly updater: TokenMetadataBackgroundUpdater,
        private readonly metrics: TokenIndexerMetrics,
        private readonly config: TokenIndexerInternalConfig,
    ) {}

    get remainingCount(): number {
        return this.updatingCount + this.tokenKeysWaiting.length;
    }

    updateOnBackground(tokenKey: DbTokenKey): void {
        this.tokenKeysWaiting.push(tokenKey);
        this.metrics.tokenMetadataUpdating.inc();

        this.dispatchQueued();
    }

    async completeAllRemaining(): Promise<void> {
        if (this.remainingCount > 0) {
            await new Promise<void>(resolve => {
                this.allCompletedCallbacks.push(resolve);
            });
        }
    }

    private dispatchQueued(): void {
        while (this.tokenKeysWaiting[0] && this.updatingCount < this.config.metadataUpdateParallelism) {
            void this.dispatchUpdate(this.tokenKeysWaiting[0]);

            this.tokenKeysWaiting.shift();
            this.updatingCount++;
        }
    }

    private async dispatchUpdate(tokenKey: DbTokenKey): Promise<void> {
        await this.updater.update(tokenKey);

        this.metrics.tokenMetadataUpdating.dec();
        this.updatingCount--;

        if (this.remainingCount === 0) {
            this.allCompletedCallbacks.forEach(c => c());
            this.allCompletedCallbacks.length = 0;
        } else {
            this.dispatchQueued();
        }
    }
}
