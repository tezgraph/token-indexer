import {
    DB_DATA_SOURCE_PROVIDER_DI_TOKEN,
    DB_EXECUTOR_DI_TOKEN,
    DbDataSourceProvider,
    DbExecutor,
} from '@tezos-dappetizer/database';
import { BlockLevelIndexingListener } from '@tezos-dappetizer/indexer';
import { BackgroundWorker, getConstructorName, injectLogger, Logger } from '@tezos-dappetizer/utils';
import { inject, singleton } from 'tsyringe';

import { TokenMetadataBackgroundDispatcher } from './token-metadata-background-dispatcher';
import { TokenIndexerInternalConfig } from '../../config/token-indexer-internal-config';
import { SelectTokenKeysWherePendingMetadata } from '../../db-updaters/commands/select-token-keys-where-pending-metadata';

@singleton()
export class TokenMetadataBackgroundWorker implements BackgroundWorker, BlockLevelIndexingListener {
    readonly name = getConstructorName(this);

    constructor(
        private readonly dispatcher: TokenMetadataBackgroundDispatcher,
        @inject(DB_EXECUTOR_DI_TOKEN) private readonly dbExecutor: DbExecutor,
        @inject(DB_DATA_SOURCE_PROVIDER_DI_TOKEN) private readonly dbDataSourceProvider: DbDataSourceProvider,
        @injectLogger(TokenMetadataBackgroundWorker) private readonly logger: Logger,
        private readonly config: TokenIndexerInternalConfig,
    ) {}

    async start(): Promise<void> {
        if (this.config.skipGetMetadata) {
            return;
        }

        const dbDataSource = await this.dbDataSourceProvider.getDataSource();
        const tokenKeysPendingMetadata = await this.dbExecutor.execute(dbDataSource.manager, new SelectTokenKeysWherePendingMetadata());

        for (const tokenKey of tokenKeysPendingMetadata) {
            this.dispatcher.updateOnBackground(tokenKey);
        }
    }

    async onToBlockLevelReached(): Promise<void> {
        this.logger.logInformation('Waiting for {remainingCount} of token metadata requests to complete.', {
            remainingCount: this.dispatcher.remainingCount,
        });
        await this.dispatcher.completeAllRemaining();
    }
}
