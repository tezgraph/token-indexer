import { Clock } from '@tezos-dappetizer/utils';

import { TzipMetadata, TzipMetadataProvider } from './tzip-metadata-provider';
import { MetadataState } from '../../entities/metadata-state';
import { TokenIndexerMetrics } from '../../helpers/token-indexer-metrics';

export class MetricsReportingMetadataProvider<TMetadata extends TzipMetadata, TArgs extends unknown[]>
implements TzipMetadataProvider<TMetadata, TArgs> {
    constructor(
        private readonly nextProvider: TzipMetadataProvider<TMetadata, TArgs>,
        private readonly metrics: TokenIndexerMetrics,
        private readonly clock: Clock,
        private readonly labels: { feature: string },
    ) {}

    async get(...args: TArgs): Promise<TMetadata> {
        const startTimestamp = this.clock.getNowTimestamp();

        const metadata = await this.nextProvider.get(...args);

        const duration = this.clock.getNowTimestamp() - startTimestamp;
        this.metrics.getMetadataCallDuration.observe(this.labels, duration);

        const counter = metadata.metadataState === MetadataState.Ok ? this.metrics.getMetadataOkCount : this.metrics.getMetadataFailedCount;
        counter.inc(this.labels);

        return metadata;
    }
}
