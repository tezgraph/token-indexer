import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { inject, singleton } from 'tsyringe';

import { ContractMetadata } from './contract-metadata';
import { ContractMetadataProvider } from './contract-metadata-provider';
import { MetricsReportingContractMetadataProvider } from './metrics-reporting-contract-metadata-provider';
import { MetadataState } from '../../entities/metadata-state';
import { contractAddresses } from '../../helpers/contract-addresses';

@singleton()
export class HardcodedContractMetadataProvider implements ContractMetadataProvider {
    constructor(@inject(MetricsReportingContractMetadataProvider) private readonly nextProvider: ContractMetadataProvider) {}

    async get(contract: ContractAbstraction): Promise<ContractMetadata> {
        const name = hardcodedContractNames[contract.address];
        return name
            ? { metadataState: MetadataState.Ok, name, description: null }
            : this.nextProvider.get(contract);
    }
}

const hardcodedContractNames: Readonly<Record<string, string>> = {
    [contractAddresses.ETH_TZ]: 'ETHtez',
    [contractAddresses.SIRS]: 'LB Token',
    [contractAddresses.TEZOS_DOMAINS]: 'Tezos Domains Registry',
    [contractAddresses.TZ_BTC]: 'tzBTC',
    [contractAddresses.TZ_COLORS]: 'TzColors',
    [contractAddresses.USD_TZ]: 'USDtz',
    [contractAddresses.W_XTZ]: 'wXTZ',
};
