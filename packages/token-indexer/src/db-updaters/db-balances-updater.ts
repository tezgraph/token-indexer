import { DB_EXECUTOR_DI_TOKEN, DbExecutor, InsertCommand } from '@tezos-dappetizer/database';
import { Block } from '@tezos-dappetizer/indexer';
import { deduplicate, isNotNullish, isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';
import { execPipe, filter, map, reverse } from 'iter-tools';
import { inject, singleton } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { UpdateValidUntilBlockOnBalanceCommand } from './commands/update-valid-until-block-on-balance-command';
import { DbUpdater } from './db-updater';
import { DbBalance, DbBalanceValues } from '../entities/db-balance';
import { ContractFeature, IndexedChangeType, LedgerUpdateChange } from '../indexed-data';
import { TokenIndexingData } from '../token-indexing-data';

const bigZero = new BigNumber(0);

@singleton()
export class DbBalancesUpdater implements DbUpdater {
    constructor(
        @inject(DB_EXECUTOR_DI_TOKEN) private readonly dbExecutor: DbExecutor,
    ) {}

    async applyChanges(dbManager: EntityManager, data: TokenIndexingData, block: Block): Promise<void> {
        const ledgerUpdates = execPipe(
            data.changes,
            map(change => (change.type === IndexedChangeType.Ledger ? change : null)),
            filter(isNotNullish),
            reverse, // Latter ledger changes prevail.
            deduplicate(getUniqueKey),
        );

        // Apply one-by-one b/c it's complicated to evalute.
        for (const ledger of ledgerUpdates) {
            await this.dbExecutor.execute(dbManager, new UpdateValidUntilBlockOnBalanceCommand({
                filter: {
                    contractAddress: ledger.contract.address,
                    tokenId: ledger.tokenId,
                    // If NFT -> match any owner -> invalidate it.
                    ...isNft(ledger) || !ledger.ownerAddress ? {} : { ownerAddress: ledger.ownerAddress },
                },
                validUntilBlockHashToSet: block.hash,
            }));

            // Zero amounts are not written b/c it is same as non-existent balance.
            if (!isNullish(ledger.ownerAddress) && !isNullish(ledger.amount) && ledger.amount.isGreaterThan(bigZero)) {
                const dbBalance: DbBalanceValues = {
                    token: {
                        contractAddress: ledger.contract.address,
                        id: ledger.tokenId,
                    },
                    ownerAddress: ledger.ownerAddress,
                    amount: ledger.amount,
                    operationGroupHash: ledger.operationGroupHash,
                    validFromBlock: { hash: block.hash },
                    validUntilBlock: null,
                };

                await this.dbExecutor.execute(dbManager, new InsertCommand(DbBalance, dbBalance));
            }
        }
    }
}

function getUniqueKey(ledger: LedgerUpdateChange): string {
    const ownerKey = isNft(ledger) ? '' : ledger.ownerAddress ?? '';
    return `${ledger.contract.address}:${ledger.tokenId.toFixed()}:${ownerKey}`;
}

function isNft(ledger: LedgerUpdateChange): boolean {
    return ledger.contract.features.includes(ContractFeature.Ledger_Nft_like);
}
