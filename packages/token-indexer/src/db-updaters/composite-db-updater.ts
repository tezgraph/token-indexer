import { Block } from '@tezos-dappetizer/indexer';
import { Logger, errorToString, getConstructorName, injectLogger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { DbActionsUpdater } from './db-actions-updater';
import { DbBalancesUpdater } from './db-balances-updater';
import { DbContractsUpdater } from './db-contracts-updater';
import { DbTokensUpdater } from './db-tokens-updater';
import { DbUpdater } from './db-updater';
import { TokenIndexingData } from '../token-indexing-data';

@singleton()
export class CompositeDbUpdater implements DbUpdater {
    private readonly updaters: readonly DbUpdater[];

    constructor(
        contractsUpdater: DbContractsUpdater,
        tokensUpdater: DbTokensUpdater,
        balancesUpdater: DbBalancesUpdater,
        actionsUpdater: DbActionsUpdater,
        @injectLogger(CompositeDbUpdater) private readonly logger: Logger,
    ) {
        // The order is siginificant.
        this.updaters = [contractsUpdater, tokensUpdater, balancesUpdater, actionsUpdater];
    }

    async applyChanges(dbManager: EntityManager, data: TokenIndexingData, block: Block): Promise<void> {
        for (const updater of this.updaters) {
            try {
                this.logger.logDebug('Applying changes using {updater}.', { updater: getConstructorName(updater) });

                await updater.applyChanges(dbManager, data, block);

                this.logger.logDebug('Successfully applied changes using {updater}.', { updater: getConstructorName(updater) });
            } catch (error) {
                throw new Error(`${updater.constructor.name} failed to apply changes. ${errorToString(error)}`);
            }
        }
    }
}
