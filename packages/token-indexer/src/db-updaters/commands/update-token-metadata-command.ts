import { DbCommand } from '@tezos-dappetizer/database';
import { dumpToString, getConstructorName, isNullish } from '@tezos-dappetizer/utils';
import { EntityManager } from 'typeorm';

import { DbToken, DbTokenKey } from '../../entities/db-token';
import { TokenMetadata } from '../../metadata/token/token-metadata';

export class UpdateTokenMetadataCommand implements DbCommand {
    readonly description: string;

    constructor(
        readonly tokenKey: DbTokenKey,
        readonly metadataToSet: TokenMetadata,
    ) {
        this.description = `${getConstructorName(this)} ${dumpToString({ tokenKey, metadataToSet: { ...metadataToSet, metadata: '(too long)' } })}`;
    }

    async execute(dbManager: EntityManager): Promise<void> {
        const updateResult = await dbManager.createQueryBuilder()
            .update(DbToken)
            .set(this.metadataToSet)
            .whereInIds(this.tokenKey)
            .execute();

        if (!isNullish(updateResult.affected) && updateResult.affected !== 1) {
            throw new Error(`DB updated ${updateResult.affected} tokens intead of a single token ${JSON.stringify(this.tokenKey)}.`);
        }
    }
}
