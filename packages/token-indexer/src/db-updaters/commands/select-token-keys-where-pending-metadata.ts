import { DbCommand } from '@tezos-dappetizer/database';
import { getConstructorName, keyof } from '@tezos-dappetizer/utils';
import { EntityManager } from 'typeorm';

import { DbToken, DbTokenKey } from '../../entities/db-token';
import { MetadataState } from '../../entities/metadata-state';

export class SelectTokenKeysWherePendingMetadata implements DbCommand<readonly DbTokenKey[]> {
    readonly description = getConstructorName(this);

    async execute(dbManager: EntityManager): Promise<readonly DbTokenKey[]> {
        return dbManager.createQueryBuilder()
            .select(`t.${keyof<DbToken>('id')}`)
            .addSelect(`t.${keyof<DbToken>('contractAddress')}`)
            .from(DbToken, 't')
            .where(`t.${keyof<DbToken>('metadataState')} = '${MetadataState.Pending}'`)
            .getMany();
    }
}
