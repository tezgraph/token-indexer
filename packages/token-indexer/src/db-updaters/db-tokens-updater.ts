import { DB_EXECUTOR_DI_TOKEN, DbExecutor, InsertCommand } from '@tezos-dappetizer/database';
import { Block } from '@tezos-dappetizer/indexer';
import { deduplicate, typed } from '@tezos-dappetizer/utils';
import { execPipe, map, reverse } from 'iter-tools';
import { inject, singleton } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { SelectExistingTokenKeys } from './commands/select-existing-token-keys';
import { DbUpdater } from './db-updater';
import { TokenIndexerInternalConfig } from '../config/token-indexer-internal-config';
import { DbToken, DbTokenKey, DbTokenValues } from '../entities/db-token';
import { emptyTokenMetadata } from '../metadata/token/token-metadata';
import { TokenIndexingData } from '../token-indexing-data';

@singleton()
export class DbTokensUpdater implements DbUpdater {
    constructor(
        @inject(DB_EXECUTOR_DI_TOKEN) private readonly dbExecutor: DbExecutor,
        private readonly config: TokenIndexerInternalConfig,
    ) {}

    async applyChanges(dbManager: EntityManager, data: TokenIndexingData, block: Block): Promise<void> {
        const metadata = this.config.skipGetMetadata ? emptyTokenMetadata.Ok : emptyTokenMetadata.Pending;
        const dbTokens = Array.from(execPipe(
            reverse(data.changes), // Latter tokens are the most up-to-date.
            deduplicate(change => `${change.contract.address}:${change.tokenId.toFixed()}`), // Insert/update only once.
            map(change => typed<DbTokenValues>({
                ...metadata,
                contract: { address: change.contract.address },
                id: change.tokenId,
                firstBlock: { hash: block.hash },
                firstOperationGroupHash: change.operationGroupHash,
            })),
        ));
        if (!dbTokens.length) {
            return;
        }

        const existingKeys = await this.dbExecutor.execute(dbManager, new SelectExistingTokenKeys(dbTokens.map(getTokenKey)));
        const existingKeyStrs = new Set(map(serializeTokenKey, existingKeys));
        const dbTokensToInsert = dbTokens.filter(t => !existingKeyStrs.has(serializeTokenKey(getTokenKey(t))));
        if (!dbTokensToInsert.length) {
            return;
        }

        await this.dbExecutor.execute(dbManager, new InsertCommand(DbToken, dbTokensToInsert));

        if (!this.config.skipGetMetadata) {
            for (const token of dbTokensToInsert) {
                data.keysOfTokensPendingMetadata.push(getTokenKey(token));
            }
        }
    }
}

function getTokenKey(token: DbTokenValues): DbTokenKey {
    return {
        contractAddress: token.contract.address,
        id: token.id,
    };
}

function serializeTokenKey(tokenKey: DbTokenKey): string {
    return `${tokenKey.contractAddress}:${tokenKey.id.toFixed()}`;
}
