import { DB_EXECUTOR_DI_TOKEN, DbExecutor, InsertCommand } from '@tezos-dappetizer/database';
import { Block } from '@tezos-dappetizer/indexer';
import { deduplicate, typed } from '@tezos-dappetizer/utils';
import { execPipe, flatMap, map } from 'iter-tools';
import { inject, singleton } from 'tsyringe';
import { EntityManager } from 'typeorm';

import { DbUpdater } from './db-updater';
import { DbContract, DbContractValues } from '../entities/db-contract';
import { DbContractFeature, DbContractFeatureValues } from '../entities/db-contract-feature';
import { TokenIndexingData } from '../token-indexing-data';

@singleton()
export class DbContractsUpdater implements DbUpdater {
    constructor(
        @inject(DB_EXECUTOR_DI_TOKEN) private readonly dbExecutor: DbExecutor,
    ) {}

    async applyChanges(dbManager: EntityManager, data: TokenIndexingData, block: Block): Promise<void> {
        const contractChanges = Array.from(execPipe(
            data.changes,
            deduplicate(change => change.contract.address), // Insert/update only once.
        ));
        if (!contractChanges.length) {
            return;
        }

        // Upsert contracts.
        const dbContracts = contractChanges.map(change => typed<DbContractValues>({
            ...change.contract.metadata,
            address: change.contract.address,
            firstBlock: { hash: block.hash },
            firstOperationGroupHash: change.operationGroupHash,
        }));
        await this.dbExecutor.execute(dbManager, new InsertCommand(DbContract, dbContracts, { ignoreIfExists: true }));

        // Upsert contract features.
        const dbContractFeatures = Array.from(execPipe(
            contractChanges,
            flatMap(change => execPipe(
                change.contract.features,
                deduplicate(feature => feature),
                map(feature => typed<DbContractFeatureValues>({
                    contract: { address: change.contract.address },
                    feature,
                })),
            )),
        ));
        await this.dbExecutor.execute(dbManager, new InsertCommand(DbContractFeature, dbContractFeatures, { ignoreIfExists: true }));
    }
}
