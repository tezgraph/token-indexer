import { MergeN } from 'ts-essentials';
import { Entity, ManyToOne, PrimaryColumn } from 'typeorm';

import { contractAddressOptions, DbContract, DbContractKey } from './db-contract';
import { PickChangingType } from '../helpers/utils';

@Entity()
export class DbContractFeature {
    @PrimaryColumn({ type: 'varchar', length: 64 })
        feature!: string;

    // Contract relation:
    @ManyToOne(() => DbContract, { onDelete: 'CASCADE' })
        contract!: DbContract;

    @PrimaryColumn(contractAddressOptions)
        contractAddress!: string;
}

export type DbContractFeatureValues = MergeN<[
    Pick<DbContractFeature, 'feature'>,
    PickChangingType<DbContractFeature, 'contract', DbContractKey>,
]>;
