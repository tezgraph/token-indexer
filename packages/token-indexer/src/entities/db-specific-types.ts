import { ColumnType } from 'typeorm';

export const dbSpecificTypes: { json: ColumnType } = {
    json: 'jsonb',
};
