import { DbEntity } from '@tezos-dappetizer/database';

import { DbContractFeature } from './db-contract-feature';
import { DbAction } from '../../src/entities/db-action';
import { DbBalance } from '../../src/entities/db-balance';
import { DbContract } from '../../src/entities/db-contract';
import { DbToken } from '../../src/entities/db-token';

export const tokenIndexerDbEntities: readonly DbEntity[] = Object.freeze([
    DbAction,
    DbBalance,
    DbContract,
    DbContractFeature,
    DbToken,
]);
