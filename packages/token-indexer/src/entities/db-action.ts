import { commonDbColumns, DbBlock } from '@tezos-dappetizer/database';
import { BigNumber } from 'bignumber.js';
import { DeepNonNullable, MergeN } from 'ts-essentials';
import { Column, Entity, Index, ManyToOne, PrimaryColumn } from 'typeorm';

import { DbBlockKey } from './db-block-key';
import { contractAddressOptions } from './db-contract';
import { DbToken, DbTokenKey, tokenIdOptions } from './db-token';
import { PickChangingType } from '../helpers/utils';

@Entity()
export class DbAction {
    @PrimaryColumn()
        order!: number;

    @Column({ type: 'varchar', length: 32 })
        type!: string;

    @Column(commonDbColumns.operationGroupHash)
        operationGroupHash!: string;

    @Column({ ...commonDbColumns.bigNumber, nullable: true })
        amount!: BigNumber | null;

    @Index('IDX_action_from_address')
    @Column({ ...commonDbColumns.address, nullable: true })
        fromAddress!: string | null;

    @Index('IDX_action_to_address')
    @Column({ ...commonDbColumns.address, nullable: true })
        toAddress!: string | null;

    @Index('IDX_action_owner_address')
    @Column({ ...commonDbColumns.address, nullable: true })
        ownerAddress!: string | null;

    // Block relation:
    @ManyToOne(() => DbBlock, { onDelete: 'CASCADE' })
        block!: DbBlock;

    @Index('IDX_action_block_hash')
    @PrimaryColumn(commonDbColumns.blockHash)
        blockHash!: string;

    // Token relation:
    @ManyToOne(() => DbToken, { onDelete: 'CASCADE' })
        token!: DbToken;

    @Column(tokenIdOptions)
        tokenId!: BigNumber;

    @Column(contractAddressOptions)
        tokenContractAddress!: string;
}

export type DbActionAddresses =
    Pick<DbAction, 'ownerAddress'>
    | DeepNonNullable<Pick<DbAction, 'fromAddress'>>
    | DeepNonNullable<Pick<DbAction, 'toAddress'>>;

export type DbActionValues = MergeN<[
    Pick<DbAction, 'order' | 'type' | 'operationGroupHash' | 'amount'>,
    DbActionAddresses,
    PickChangingType<DbAction, 'block', DbBlockKey>,
    PickChangingType<DbAction, 'token', DbTokenKey>,
]>;
