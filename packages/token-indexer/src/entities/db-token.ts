import { commonDbColumns, DbBlock } from '@tezos-dappetizer/database';
import { BigNumber } from 'bignumber.js';
import { MergeN, StrictOmit } from 'ts-essentials';
import { Column, Entity, Index, ManyToOne, PrimaryColumn } from 'typeorm';

import { DbBlockKey } from './db-block-key';
import { contractAddressOptions, DbContract, DbContractKey } from './db-contract';
import { dbSpecificTypes } from './db-specific-types';
import { MetadataState, metadataStateColumnOptions } from './metadata-state';
import { PickChangingType } from '../helpers/utils';

export const tokenIdOptions = commonDbColumns.bigNumber;

@Entity()
export class DbToken {
    @PrimaryColumn(tokenIdOptions)
        id!: BigNumber;

    @Column({ type: 'varchar', nullable: true })
        name!: string | null;

    @Column({ type: 'varchar', nullable: true })
        symbol!: string | null;

    @Column({ type: 'integer', nullable: true })
        decimals!: number | null;

    @Column({ type: dbSpecificTypes.json, nullable: true })
        metadata!: string | null;

    @Column(metadataStateColumnOptions)
        metadataState!: MetadataState;

    @Column(commonDbColumns.operationGroupHash)
        firstOperationGroupHash!: string;

    // Contract relation:
    @ManyToOne(() => DbContract, { onDelete: 'CASCADE' })
        contract!: DbContract;

    @PrimaryColumn(contractAddressOptions)
        contractAddress!: string;

    // First block relation:
    @ManyToOne(() => DbBlock, { onDelete: 'CASCADE' })
        firstBlock!: DbBlock;

    @Index('IDX_token_first_block_hash')
    @Column(commonDbColumns.blockHash)
        firstBlockHash!: string;
}

export type DbTokenKey = Pick<DbToken, 'contractAddress' | 'id'>;

export type DbTokenValues = MergeN<[
    StrictOmit<DbToken, 'contractAddress' | 'firstBlockHash'>,
    PickChangingType<DbToken, 'contract', DbContractKey>,
    PickChangingType<DbToken, 'firstBlock', DbBlockKey>,
]>;
