import { commonDbColumns, DbBlock } from '@tezos-dappetizer/database';
import { BigNumber } from 'bignumber.js';
import { MergeN } from 'ts-essentials';
import { Column, Entity, Index, ManyToOne, PrimaryColumn } from 'typeorm';

import { DbBlockKey } from './db-block-key';
import { contractAddressOptions } from './db-contract';
import { DbToken, DbTokenKey, tokenIdOptions } from './db-token';
import { PickChangingType } from '../helpers/utils';

@Index('IDX_balance_token_contract_address_token_id', ['tokenContractAddress', 'tokenId'])
@Index('IDX_balance_token_contract_address_token_id_owner_address', ['tokenContractAddress', 'tokenId', 'ownerAddress'])
@Entity()
export class DbBalance {
    @Index('IDX_balance_owner_address')
    @PrimaryColumn(commonDbColumns.address)
        ownerAddress!: string;

    @Column(commonDbColumns.bigNumber)
        amount!: BigNumber;

    @Column(commonDbColumns.operationGroupHash)
        operationGroupHash!: string;

    // Token relation:
    @ManyToOne(() => DbToken, { onDelete: 'CASCADE' })
        token!: DbToken;

    @PrimaryColumn(tokenIdOptions)
        tokenId!: BigNumber;

    @PrimaryColumn(contractAddressOptions)
        tokenContractAddress!: string;

    // Valid-from block relation:
    @ManyToOne(() => DbBlock, { onDelete: 'CASCADE' })
        validFromBlock!: DbBlock;

    @Index('IDX_balance_valid_from_block_hash')
    @PrimaryColumn(commonDbColumns.blockHash)
        validFromBlockHash!: string;

    // Valid-until block relation:
    @ManyToOne(() => DbBlock, { nullable: true, onDelete: 'SET NULL' })
        validUntilBlock!: DbBlock | null;

    @Index('IDX_balance_valid_until_block_hash')
    @Column({ ...commonDbColumns.blockHash, nullable: true })
        validUntilBlockHash!: string | null;
}

export type DbBalanceValues = MergeN<[
    Pick<DbBalance, 'ownerAddress' | 'amount' | 'operationGroupHash'>,
    PickChangingType<DbBalance, 'token', DbTokenKey>,
    PickChangingType<DbBalance, 'validFromBlock', DbBlockKey>,
    PickChangingType<DbBalance, 'validUntilBlock', DbBlockKey | null>,
]>;
