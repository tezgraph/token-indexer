import { getEnumValues } from '@tezos-dappetizer/utils';
import { ColumnOptions } from 'typeorm';

export enum MetadataState {
    /** The block has been indexed and metadata has been fetched successfully. */
    Ok = 'Ok',

    /** The block has been indexed, but there was an error fetching metadata - see logs. */
    Failed = 'Failed',

    /** The block has been indexed, but metadata has not been fetched yet. */
    Pending = 'Pending',
}

export const metadataStateColumnOptions: Readonly<ColumnOptions> = {
    type: 'varchar',
    length: Math.max(...getEnumValues(MetadataState).map(v => v.length)),
};
