import { DbBlock } from '@tezos-dappetizer/database';

export type DbBlockKey = Pick<DbBlock, 'hash'>;
