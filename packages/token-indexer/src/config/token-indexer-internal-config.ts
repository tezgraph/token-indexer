import { ConfigElement, ConfigWithDiagnostics, typed } from '@tezos-dappetizer/utils';
import { NonEmptyArray } from 'ts-essentials';

import { TokenIndexerConfig, TokenIndexerModuleConfig } from './token-indexer-config';

export const TOKEN_INDEXER_MODULE_ID = typed<TokenIndexerModuleConfig['id']>('@tezos-dappetizer/token-indexer');

export class TokenIndexerInternalConfig implements ConfigWithDiagnostics {
    static readonly rootName = 'tokenIndexerModule';

    readonly configFilePath: string;
    readonly configPropertyPath: string;

    readonly logIncompatibleContracts: boolean;
    readonly logIncompatibleContractsThreshold: number;
    readonly skipGetMetadata: boolean;
    readonly metadataUpdateParallelism: number;
    readonly contractNames: Readonly<NonEmptyArray<string>> | null;
    readonly contractMetadataCacheMillis: number;

    constructor(rootElement: ConfigElement) {
        const thisElement = rootElement.value !== undefined ? rootElement.asObject<keyof TokenIndexerConfig>() : null;
        this.configFilePath = rootElement.filePath;
        this.configPropertyPath = rootElement.propertyPath;

        this.logIncompatibleContracts = thisElement?.getOptional('logIncompatibleContracts')?.asBoolean()
            ?? false;
        this.logIncompatibleContractsThreshold = thisElement?.getOptional('logIncompatibleContractsThreshold')?.asInteger()
            ?? 10;
        this.skipGetMetadata = thisElement?.getOptional('skipGetMetadata')?.asBoolean()
            ?? false;
        this.metadataUpdateParallelism = thisElement?.getOptional('metadataUpdateParallelism')?.asInteger({ min: 10 })
            ?? 100;
        this.contractNames = (thisElement?.getOptional('contractNames')?.asArray('NotEmpty').map(e => e.asString('NotWhiteSpace'))
            ?? null) as NonEmptyArray<string> | null;
        this.contractMetadataCacheMillis = thisElement?.getOptional('contractMetadataCacheMillis')?.asInteger({ min: 0 })
            ?? 600_000;
    }

    getDiagnostics(): [string, unknown] {
        return [TokenIndexerInternalConfig.rootName, this];
    }
}
