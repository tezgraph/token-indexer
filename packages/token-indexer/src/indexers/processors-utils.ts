import * as taquitoMichelCodec from '@taquito/michel-codec';
import { Michelson, MichelsonSchema } from '@tezos-dappetizer/indexer';

export function unpack(parameter: string): Michelson {
    return taquitoMichelCodec.unpackData(extractNumbersFromBytesString(parameter));
}

function extractNumbersFromBytesString(bytes: string): number[] {
    const match = bytes.match(/.{1,2}/gu);
    if (match === null) {
        throw new Error(`Failed to extract numbers from bytes: ${bytes}`);
    }

    return match.map(c => parseInt(c, 16));
}

const tokenMetadataField = 'token_metadata';

export class FieldHandler<T> {
    constructor(
        readonly supportedNames: readonly string[],
    ) {}

    existsIn(schema: MichelsonSchema['generated']): boolean {
        for (const fieldName of this.supportedNames) {
            if (schema.__michelsonType === 'pair') { // eslint-disable-line no-underscore-dangle
                if (schema.schema[fieldName]) {
                    return true;
                }

                const metadataSchema = schema.schema[tokenMetadataField];
                if (metadataSchema?.__michelsonType === 'pair' && metadataSchema.schema[fieldName]) { // eslint-disable-line no-underscore-dangle
                    return true;
                }
            }
        }
        return false;
    }

    getValue(instance: object): T {
        const value = this.getValueOrNull(instance);

        if (value === null) {
            throw new Error(`Could not get mandatory value from ${JSON.stringify(instance)}`
                + ` for supported field names ${JSON.stringify(this.supportedNames)}.`);
        }
        return value;
    }

    getValueOrNull(instance: object): T | null {
        for (const [key, value] of Object.entries(instance as Record<string, unknown>)) {
            if (this.supportedNames.includes(key)) {
                return value as T;
            }

            if (key === tokenMetadataField) {
                const metadataValue = this.getValueOrNull(value as object);
                if (metadataValue !== null) {
                    return metadataValue;
                }
            }
        }
        return null;
    }
}
