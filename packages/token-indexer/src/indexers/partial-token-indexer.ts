import { BigMapUpdate, Contract, TransactionParameter } from '@tezos-dappetizer/indexer';
import { StrictOmit } from 'ts-essentials';
import { InjectionToken } from 'tsyringe';

import { ContractFeature, IndexedChange } from '../indexed-data';
import { TokenBigMapUpdateIndexingContext, TokenTransactionIndexingContext } from '../token-indexing-data';

export interface PartialTokenIndexer<TContextData = unknown> {
    readonly name: string;

    shouldIndex(contract: Contract): false | WithFeatures<TContextData>;

    indexTransaction?(
        transactionParameter: TransactionParameter,
        context: TokenTransactionIndexingContext<TContextData>,
    ): Iterable<PartialIndexedChange>;

    indexBigMapUpdate?(
        bigMapUpdate: BigMapUpdate,
        context: TokenBigMapUpdateIndexingContext<TContextData>,
    ): Iterable<PartialIndexedChange>;
}

export interface WithFeatures<TContextData> {
    readonly data: TContextData;
    readonly contractFeatures: readonly ContractFeature[];
}

export const PARTIAL_TOKEN_INDEXERS_DI_TOKEN: InjectionToken<PartialTokenIndexer> = 'Dappetizer:TokenIndexer:PartialTokenIndexers';

export type PartialIndexedChange = DistributiveOmit<IndexedChange, 'contract' | 'operationGroupHash'>;

type DistributiveOmit<T, K extends keyof T> = T extends object
    ? StrictOmit<T, K>
    : never;
