import { BigMapInfo, Contract, LazyMichelsonValue } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { ContractFeature } from '../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../token-indexing-data';
import { ContractType, ContractTypeResolver } from '../contract-type-resolver';

export abstract class LedgerProcessor implements ContractTypeResolver {
    readonly contractFeatures: readonly ContractFeature[] = [];

    resolveContractType(_contract: Contract): ContractType | null {
        return null;
    }

    abstract shouldProcess(
        bigMap: BigMapInfo,
        address: string,
    ): boolean;

    abstract deserializeRawUpdates(
        key: LazyMichelsonValue,
        value: LazyMichelsonValue | null,
        context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate>;
}

export interface RawLedgerUpdate {
    readonly tokenId: BigNumber;
    readonly ownerAddress: string | null;
    readonly amount: BigNumber | null;
}
