import { BigMapInfo } from '@tezos-dappetizer/indexer';
import { findNonNullish } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { LedgerProcessor } from './ledger-processor';
import { SingleAssetLedgerProcessor } from './processors/incompatible/single-asset-ledger-processor';
import { TezosDomainsLedgerProcessor } from './processors/incompatible/tezos-domains-ledger-processor';
import { tzBTCLedgerProcessor } from './processors/incompatible/tz-btc-ledger-processor';
import { TzColorsRaribleLedgerProcessor } from './processors/incompatible/tzcolors-rarible-ledger-processor';
import { Tzip12MultiAssetLedgerProcessor } from './processors/tzip12-multi-asset-ledger-processor';
import { Tzip12NftLedgerProcessor } from './processors/tzip12-nft-asset-ledger-processor';
import { Tzip12SingleAssetLedgerProcessor } from './processors/tzip12-single-asset-ledger-processor';
import { Tzip7LedgerProcessor } from './processors/tzip7-ledger-processor';

@singleton()
export class CompositeLedgerProcessor {
    readonly processors: readonly LedgerProcessor[] = [
        new Tzip7LedgerProcessor(),
        new Tzip12MultiAssetLedgerProcessor(),
        new Tzip12NftLedgerProcessor(),
        new Tzip12SingleAssetLedgerProcessor(),
        new SingleAssetLedgerProcessor(),
        new TzColorsRaribleLedgerProcessor(),
        new TezosDomainsLedgerProcessor(),
        new tzBTCLedgerProcessor(),
    ];

    findProcessor(
        bigMaps: readonly BigMapInfo[],
        address: string,
    ): LedgerProcessorMatch | null {
        return findNonNullish(this.processors, processor => {
            const ledgerBigMap = bigMaps.find(m => processor.shouldProcess(m, address));
            return ledgerBigMap ? { processor, ledgerBigMap } : null;
        });
    }
}

export interface LedgerProcessorMatch {
    readonly processor: LedgerProcessor;
    readonly ledgerBigMap: BigMapInfo;
}
