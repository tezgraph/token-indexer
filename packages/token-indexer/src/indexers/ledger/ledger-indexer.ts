import { BigMapInfo, BigMapUpdate, Contract } from '@tezos-dappetizer/indexer';
import { ToJSON, getConstructorName } from '@tezos-dappetizer/utils';
import { isEqual } from 'lodash';
import { singleton } from 'tsyringe';

import { CompositeLedgerProcessor } from './composite-ledger-processor';
import { LedgerProcessor } from './ledger-processor';
import { TokenIndexerMetrics } from '../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../token-indexing-data';
import { PartialIndexedChange, PartialTokenIndexer, WithFeatures } from '../partial-token-indexer';

@singleton()
export class LedgerIndexer implements PartialTokenIndexer<LedgerData> {
    readonly name = 'ledger';

    constructor(
        private readonly processors: CompositeLedgerProcessor,
        private readonly metrics: TokenIndexerMetrics,
    ) {}

    shouldIndex(contract: Contract): WithFeatures<LedgerData> | false {
        const processorMatch = this.processors.findProcessor(contract.bigMaps, contract.address);
        if (!processorMatch) {
            this.metrics.processorUsage.inc({ name: 'unknown', feature: 'ledger' });
            return false;
        }

        const processor = processorMatch.processor;
        this.metrics.processorUsage.inc({ name: getConstructorName(processor), feature: 'ledger' });
        return {
            data: new LedgerData(processor, processorMatch.ledgerBigMap),
            contractFeatures: [
                ContractFeature.Ledger,
                ...processor.contractFeatures,
                getConstructorName(processor) as ContractFeature,
            ],
        };
    }

    *indexBigMapUpdate?(
        bigMapUpdate: BigMapUpdate,
        context: TokenBigMapUpdateIndexingContext<LedgerData>,
    ): Iterable<PartialIndexedChange> {
        if (!bigMapUpdate.key
            || !bigMapUpdate.bigMap
            || !isEqual(bigMapUpdate.bigMap.path, context.contractData.bigMap.path)) {
            return;
        }

        for (const rawUpdate of context.contractData.processor.deserializeRawUpdates(bigMapUpdate.key, bigMapUpdate.value, context)) {
            yield {
                type: IndexedChangeType.Ledger,
                tokenId: rawUpdate.tokenId,
                ownerAddress: rawUpdate.ownerAddress,
                amount: rawUpdate.amount,
            };
        }
    }
}

export class LedgerData implements ToJSON {
    constructor(
        readonly processor: LedgerProcessor,
        readonly bigMap: BigMapInfo,
    ) {}

    toJSON(): unknown {
        return {
            processor: getConstructorName(this.processor),
            bigMap: {
                lastKnownId: this.bigMap.lastKnownId,
                path: this.bigMap.path,
            },
        };
    }
}
