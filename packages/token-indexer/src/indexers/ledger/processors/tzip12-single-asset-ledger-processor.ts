import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';

import { LedgerProcessorCompatible } from './ledger-processor-compatible';
import { rpcGuard } from '../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../helpers/token-constants';
import { ContractFeature } from '../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../token-indexing-data';
import { ContractType } from '../../contract-type-resolver';
import { RawLedgerUpdate } from '../ledger-processor';

export class Tzip12SingleAssetLedgerProcessor extends LedgerProcessorCompatible {
    override readonly contractFeatures = [
        ContractFeature.Ledger_Tzip12,
        ContractFeature.SingleAsset,
    ] as const;

    protected override readonly contractType = ContractType.FA2_like;

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'address', // Owner.
        }),
        value: new MichelsonSchema({
            prim: 'nat', // Amount.
        }),
    };

    override *deserializeRawUpdatesFromNoAnnots(
        key: string,
        value: BigNumber | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        yield {
            tokenId: SINGLE_ASSET_TOKEN_ID,
            ownerAddress: rpcGuard.string(key),
            amount: !isNullish(value) ? rpcGuard.positiveBigNumberInteger(value) : null,
        };
    }
}
