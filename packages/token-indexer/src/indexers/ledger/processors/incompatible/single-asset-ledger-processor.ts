import { BigMapInfo, LazyMichelsonValue } from '@tezos-dappetizer/indexer';
import BigNumber from 'bignumber.js';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { ContractFeature } from '../../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../../token-indexing-data';
import { FieldHandler } from '../../../processors-utils';
import { LedgerProcessor, RawLedgerUpdate } from '../../ledger-processor';

export class SingleAssetLedgerProcessor extends LedgerProcessor {
    override readonly contractFeatures = [
        ContractFeature.SingleAsset,
    ] as const;

    private readonly supportedLedgerBigMapNames = [
        'ledger',
        'balances',
    ];

    private readonly amountField = new FieldHandler<BigNumber>([
        'amount',
        'balance',
    ]);

    shouldProcess(bigMap: BigMapInfo, _address: string): boolean {
        if (bigMap.name === null || !this.supportedLedgerBigMapNames.includes(bigMap.name)) {
            return false;
        }

        const keyContainsOwner = bigMap.keySchema.generated.__michelsonType === 'address'; // eslint-disable-line no-underscore-dangle
        return keyContainsOwner && this.amountField.existsIn(bigMap.valueSchema.generated);
    }

    *deserializeRawUpdates(
        key: LazyMichelsonValue,
        value: LazyMichelsonValue | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        const ownerAddress = rpcGuard.string(key.convert());

        const amount = value
            ? rpcGuard.positiveBigNumberInteger(this.amountField.getValue(value.convert()))
            : null;

        yield { tokenId: SINGLE_ASSET_TOKEN_ID, ownerAddress, amount };
    }
}
