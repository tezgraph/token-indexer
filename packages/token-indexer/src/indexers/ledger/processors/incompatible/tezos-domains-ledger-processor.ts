import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT } from '../../../../helpers/token-constants';
import { ContractFeature } from '../../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../../token-indexing-data';
import { RawLedgerUpdate } from '../../ledger-processor';
import { LedgerProcessorNoAnnots } from '../ledger-processor-no-annots';

export class TezosDomainsLedgerProcessor extends LedgerProcessorNoAnnots {
    override readonly contractFeatures = [
        ContractFeature.Ledger_Nft_like,
    ] as const;

    protected override readonly supportedContractAddresses = [contractAddresses.TEZOS_DOMAINS] as const;

    protected override readonly ledgerBigMapName = 'records';

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'bytes', // Name
        }),
        value: new MichelsonSchema({
            prim: 'pair',
            args: [
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                {
                                    prim: 'option',
                                    args: [{ prim: 'address' }], // 0
                                },
                                {
                                    prim: 'map', // 1
                                    args: [
                                        { prim: 'string' },
                                        { prim: 'bytes' },
                                    ],
                                },
                            ],
                        },
                        {
                            prim: 'option',
                            args: [{ prim: 'bytes' }], // 2
                        },
                        {
                            prim: 'map', // 3
                            args: [
                                { prim: 'string' },
                                { prim: 'bytes' },
                            ],
                        },
                    ],
                },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat' }, // 4
                        { prim: 'address' }, // 5
                    ],
                },
                {
                    prim: 'option',
                    args: [{ prim: 'nat' }], // 6
                },
            ],
        }),
    };

    protected override *deserializeRawUpdatesFromNoAnnots(
        _key: string,
        value: LedgerValue,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        const tokenId = value[6];
        if (tokenId === null) {
            return;
        }

        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(tokenId.Some),
            ownerAddress: rpcGuard.string(value[5]),
            amount: NFT_DEFAULT_AMOUNT,
        };
    }
}

interface LedgerValue {
    '5': string; // Owner.
    '6': { Some: BigNumber } | null; // Token ID.
}
