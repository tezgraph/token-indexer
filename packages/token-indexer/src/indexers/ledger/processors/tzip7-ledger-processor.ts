import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isNullish } from '@tezos-dappetizer/utils';
import { BigNumber } from 'bignumber.js';

import { LedgerProcessorCompatible } from './ledger-processor-compatible';
import { rpcGuard } from '../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../helpers/token-constants';
import { ContractFeature } from '../../../indexed-data';
import { TokenBigMapUpdateIndexingContext } from '../../../token-indexing-data';
import { ContractType } from '../../contract-type-resolver';
import { RawLedgerUpdate } from '../ledger-processor';

/**
 * Index where key is the owner's address and value is the pair [amount, map of allowances].
 * big_map %ledger address (pair nat (map address nat))
 * @see https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-7/tzip-7.md#token-balance-updates
 */
export class Tzip7LedgerProcessor extends LedgerProcessorCompatible {
    override readonly contractFeatures = [
        ContractFeature.Ledger_Tzip7,
        ContractFeature.SingleAsset,
    ] as const;

    protected override readonly contractType = ContractType.FA1_2_like;

    protected override readonly schemaNoAnnots = {
        key: new MichelsonSchema({
            prim: 'address', // Owner.
        }),
        value: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'nat' }, // Amount.
                {
                    prim: 'map', // Allowances.
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                },
            ],
        }),
    };

    override *deserializeRawUpdatesFromNoAnnots(
        key: string,
        value: LedgerValue | null,
        _context: TokenBigMapUpdateIndexingContext,
    ): Iterable<RawLedgerUpdate> {
        yield {
            tokenId: SINGLE_ASSET_TOKEN_ID,
            ownerAddress: rpcGuard.string(key),
            amount: !isNullish(value) ? rpcGuard.positiveBigNumberInteger(value[0]) : null,
        };
    }
}

export interface LedgerValue {
    '0': BigNumber; // Amount.
}
