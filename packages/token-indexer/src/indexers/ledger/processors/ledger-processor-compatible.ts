import { Contract, MichelsonSchema } from '@tezos-dappetizer/indexer';
import { isEqual } from 'lodash';

import { LedgerProcessorNoAnnots } from './ledger-processor-no-annots';
import { removeAnnots } from '../../../helpers/michelson-utils';
import { ContractType } from '../../contract-type-resolver';

export abstract class LedgerProcessorCompatible extends LedgerProcessorNoAnnots {
    protected abstract readonly contractType: ContractType;

    override resolveContractType(contract: Contract): ContractType | null {
        const ledgerSchema = this.getLedgerSchema(contract);

        const hasEqualSchemas = ledgerSchema !== null
            && isEqual(removeAnnots(ledgerSchema.key.michelson), this.schemaNoAnnots.key.michelson)
            && isEqual(removeAnnots(ledgerSchema.value.michelson), this.schemaNoAnnots.value.michelson);

        return hasEqualSchemas ? this.contractType : null;
    }

    private getLedgerSchema(contract: Contract): { key: MichelsonSchema; value: MichelsonSchema } | null {
        for (const bigMap of contract.bigMaps) {
            if (bigMap.name === this.ledgerBigMapName) {
                return { key: bigMap.keySchema, value: bigMap.valueSchema };
            }
        }

        return null;
    }
}
