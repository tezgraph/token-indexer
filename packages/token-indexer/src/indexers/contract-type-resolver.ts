import { Contract } from '@tezos-dappetizer/indexer';

export interface ContractTypeResolver {
    resolveContractType(contract: Contract): ContractType | null;
}

export enum ContractType {
    FA1_2_like = 'FA1_2_like', // The contract is Financial Application 1.2 according to TZIP-7 - has respective ledger or transfer entrypoint.
    FA2_like = 'FA2_like', // The contract is Financial Application 2 according to TZIP-12 - has respective ledger or transfer entrypoint.
}
