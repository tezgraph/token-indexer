import { Contract } from '@tezos-dappetizer/indexer';
import { findNonNullish } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { ContractType, ContractTypeResolver } from './contract-type-resolver';
import { CompositeTransferProcessor } from './entrypoints/transfer/composite-transfer-processor';
import { CompositeLedgerProcessor } from './ledger/composite-ledger-processor';

@singleton()
export class CompositeContractTypeResolver implements ContractTypeResolver {
    private readonly resolvers: ContractTypeResolver[] = [
        ...new CompositeTransferProcessor().processors,
        ...new CompositeLedgerProcessor().processors,
    ];

    resolveContractType(contract: Contract): ContractType | null {
        return findNonNullish(this.resolvers, r => r.resolveContractType(contract));
    }
}
