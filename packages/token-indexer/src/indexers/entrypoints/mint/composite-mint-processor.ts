import { singleton } from 'tsyringe';

import { KalamintMintProcessor } from './processors/incompatible/kalamint-mint-processor';
import { PaulMintProcessor } from './processors/incompatible/paul-mint-processor';
import { TezosMandalaMintProcessor } from './processors/incompatible/tezos-mandala-mint-processor';
import { ListMintProcessor } from './processors/list-mint-processor';
import { MintProcessor } from './processors/mint-processor';
import { RawMint } from './raw-mint';
import { CompositeEntrypointProcessor } from '../composite-entrypoint-processor';

@singleton()
export class CompositeMintProcessor extends CompositeEntrypointProcessor<RawMint> {
    constructor() {
        super([
            new KalamintMintProcessor(),
            new PaulMintProcessor(),
            new TezosMandalaMintProcessor(),
            new MintProcessor(),
            new ListMintProcessor(),
        ]);
    }
}
