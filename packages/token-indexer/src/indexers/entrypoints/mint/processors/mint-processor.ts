import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../token-indexing-data';
import { MintProcessorWithAnnots } from '../mint-processor-with-annots';
import { RawMint } from '../raw-mint';

export class MintProcessor extends MintProcessorWithAnnots {
    override shouldProcess(parameterSchema: MichelsonSchema, _address: string): boolean {
        return this.toField.existsIn(parameterSchema.generated);
    }

    override *deserializeRawChanges(transactionParameter: TransactionParameter, context: TokenTransactionIndexingContext): Iterable<RawMint> {
        const parameters = transactionParameter.value.convert<NonNullish>();

        // eslint-disable-next-line deprecation/deprecation
        const to = this.toField.getValueOrNull(parameters) ?? context.operationWithResult.sourceAddress;
        const amount = this.amountField.getValueOrNull(parameters) ?? NFT_DEFAULT_AMOUNT;
        const tokenId = this.tokenIdField.getValueOrNull(parameters) ?? SINGLE_ASSET_TOKEN_ID;

        yield {
            tokenId: rpcGuard.positiveBigNumberInteger(tokenId),
            toAddress: rpcGuard.string(to),
            amount: rpcGuard.positiveBigNumberInteger(amount),
        };
    }
}
