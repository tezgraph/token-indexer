import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { contractAddresses } from '../../../../../helpers/contract-addresses';
import { rpcGuard } from '../../../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../../helpers/token-constants';
import { TokenTransactionIndexingContext } from '../../../../../token-indexing-data';
import { MintProcessorNoAnnots } from '../../mint-processor-no-annots';
import { RawMint } from '../../raw-mint';

export class PaulMintProcessor extends MintProcessorNoAnnots {
    protected override readonly supportedContractAddresses = [contractAddresses.PAUL] as const;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({
        prim: 'list',
        args: [
            {
                prim: 'pair',
                args: [
                    { prim: 'address' },
                    { prim: 'nat' },
                ],
            },
        ],
    });

    protected override *deserializeRawChangesFromNoAnnots(mints: MintDto[], _context: TokenTransactionIndexingContext): Iterable<RawMint> {
        for (const mint of mints) {
            yield {
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: rpcGuard.string(mint[0]),
                amount: rpcGuard.positiveBigNumberInteger(mint[1]),
            };
        }
    }
}

interface MintDto {
    '0': string; // To
    '1': BigNumber; // Amount.
}
