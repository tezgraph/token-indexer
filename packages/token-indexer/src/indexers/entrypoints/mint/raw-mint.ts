import { BigNumber } from 'bignumber.js';

export interface RawMint {
    tokenId: BigNumber;
    toAddress: string;
    amount: BigNumber;
}
