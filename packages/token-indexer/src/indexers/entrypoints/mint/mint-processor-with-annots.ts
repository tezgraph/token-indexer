import BigNumber from 'bignumber.js';

import { RawMint } from './raw-mint';
import { FieldHandler } from '../../processors-utils';
import { EntrypointProcessor } from '../entrypoint-processor';

export abstract class MintProcessorWithAnnots extends EntrypointProcessor<RawMint> {
    protected readonly toField = new FieldHandler<string>([
        'address',
        'owner',
        'to',
        'to_',
        'user',
        'issuer',
    ]);

    protected readonly amountField = new FieldHandler<BigNumber>([
        'amount',
        'value',
        'volume',
        'token_amount',
    ]);

    protected readonly tokenIdField = new FieldHandler<BigNumber>([
        'token_id',
    ]);
}
