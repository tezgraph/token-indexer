import { singleton } from 'tsyringe';

import { BurnProcessor } from './processors/burn-processor';
import { PaulBurnProcessor } from './processors/incompatible/paul-burn-processor';
import { SingleAssetNoFromListBurnProcessor } from './processors/incompatible/single-asset-no-from-list-burn-processor';
import { ListBurnProcessor } from './processors/list-burn-processor';
import { RawBurn } from './raw-burn';
import { CompositeEntrypointProcessor } from '../composite-entrypoint-processor';

@singleton()
export class CompositeBurnProcessor extends CompositeEntrypointProcessor<RawBurn> {
    constructor() {
        super([
            new SingleAssetNoFromListBurnProcessor(),
            new PaulBurnProcessor(),
            new BurnProcessor(),
            new ListBurnProcessor(),
        ]);
    }
}
