import { BigNumber } from 'bignumber.js';

export interface RawBurn {
    tokenId: BigNumber;
    fromAddress: string;
    amount: BigNumber;
}
