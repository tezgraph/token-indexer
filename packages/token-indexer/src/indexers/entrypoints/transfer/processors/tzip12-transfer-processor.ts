import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { ContractFeature } from '../../../../indexed-data';
import { TokenTransactionIndexingContext } from '../../../../token-indexing-data';
import { ContractType } from '../../../contract-type-resolver';
import { RawTransfer, TransferProcessorNoAnnots } from '../transfer-processor-no-annots';

export class Tzip12TransferProcessor extends TransferProcessorNoAnnots {
    override readonly contractFeatures = [
        ContractFeature.Transfer_Tzip12,
        ContractFeature.FA2,
    ] as const;

    protected override readonly contractType = ContractType.FA2_like;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({
        prim: 'list',
        args: [
            {
                prim: 'pair',
                args: [
                    { prim: 'address' }, // From.
                    {
                        prim: 'list', // Destinations.
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    { prim: 'address' }, // To.
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'nat' }, // Token ID.
                                            { prim: 'nat' }, // Amount.
                                        ],
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
    });

    override *deserializeRawChangesFromNoAnnots(transferDtos: TransferDto[], _context: TokenTransactionIndexingContext): Iterable<RawTransfer> {
        for (const transferDto of transferDtos) {
            const fromAddress = rpcGuard.string(transferDto[0]);

            for (const destinationDto of transferDto[1]) {
                yield {
                    tokenId: rpcGuard.positiveBigNumberInteger(destinationDto[1]),
                    fromAddress,
                    toAddress: rpcGuard.string(destinationDto[0]),
                    amount: rpcGuard.positiveBigNumberInteger(destinationDto[2]),
                };
            }
        }
    }
}

interface TransferDto {
    '0': string; // From.
    '1': TransferDestinationDto[];
}

interface TransferDestinationDto {
    '0': string; // To.
    '1': BigNumber; // Token ID.
    '2': BigNumber; // Amount.
}
