import { MichelsonSchema } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';

import { rpcGuard } from '../../../../helpers/rpc-guard';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../helpers/token-constants';
import { ContractFeature } from '../../../../indexed-data';
import { TokenTransactionIndexingContext } from '../../../../token-indexing-data';
import { ContractType } from '../../../contract-type-resolver';
import { RawTransfer, TransferProcessorNoAnnots } from '../transfer-processor-no-annots';

export class Tzip7TransferProcessor extends TransferProcessorNoAnnots {
    override readonly contractFeatures = [
        ContractFeature.Transfer_Tzip7,
        ContractFeature.FA1_2,
    ];

    protected override readonly contractType = ContractType.FA1_2_like;

    protected override readonly parameterSchemaNoAnnots = new MichelsonSchema({
        prim: 'pair',
        args: [
            { prim: 'address' }, // From.
            {
                prim: 'pair',
                args: [
                    { prim: 'address' }, // To.
                    { prim: 'nat' }, // Value.
                ],
            },
        ],
    });

    override *deserializeRawChangesFromNoAnnots(transfer: TransferDto, _context: TokenTransactionIndexingContext): Iterable<RawTransfer> {
        yield {
            tokenId: SINGLE_ASSET_TOKEN_ID,
            fromAddress: rpcGuard.string(transfer[0]),
            toAddress: rpcGuard.string(transfer[1]),
            amount: rpcGuard.positiveBigNumberInteger(transfer[2]),
        };
    }
}

interface TransferDto {
    '0': string; // From.
    '1': string; // To.
    '2': BigNumber; // Value.
}
