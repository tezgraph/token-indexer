import { singleton } from 'tsyringe';

import { CompositeTransferProcessor } from './composite-transfer-processor';
import { RawTransfer, TransferProcessorNoAnnots } from './transfer-processor-no-annots';
import { TokenIndexerMetrics } from '../../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../indexed-data';
import { IncompatibleContractReporter } from '../../incompatible-contract-reporter';
import { PartialIndexedChange } from '../../partial-token-indexer';
import { EntrypointIndexer } from '../entrypoint-indexer';

@singleton()
export class TransferIndexer extends EntrypointIndexer<RawTransfer> {
    override readonly name = 'transfer';

    override readonly contractFeatures = [
        ContractFeature.Transfer,
    ] as const;

    override readonly supportedEntrypoints = [TransferProcessorNoAnnots.supportedTransferEntrypoint];

    constructor(processors: CompositeTransferProcessor, reporter: IncompatibleContractReporter, metrics: TokenIndexerMetrics) {
        super(processors, reporter, metrics);
    }

    override createIndexedChange(rawTransfer: RawTransfer): PartialIndexedChange {
        return {
            type: IndexedChangeType.Transfer,
            tokenId: rawTransfer.tokenId,
            fromAddress: rawTransfer.fromAddress,
            toAddress: rawTransfer.toAddress,
            amount: rawTransfer.amount,
        };
    }
}
