import { Contract, MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { getConstructorName, isNullish, Nullish } from '@tezos-dappetizer/utils';

import { CompositeEntrypointProcessor } from './composite-entrypoint-processor';
import { EntrypointProcessor } from './entrypoint-processor';
import { TokenIndexerMetrics } from '../../../src/helpers/token-indexer-metrics';
import { ContractFeature } from '../../indexed-data';
import { TokenTransactionIndexingContext } from '../../token-indexing-data';
import { IncompatibleContractReporter } from '../incompatible-contract-reporter';
import { PartialIndexedChange, PartialTokenIndexer, WithFeatures } from '../partial-token-indexer';

export abstract class EntrypointIndexer<TRawChange> implements PartialTokenIndexer<EntrypointData<TRawChange>> {
    abstract readonly name: string;
    protected abstract readonly contractFeatures: readonly ContractFeature[];
    protected abstract readonly supportedEntrypoints: readonly string[];

    constructor(
        private readonly processors: CompositeEntrypointProcessor<TRawChange>,
        private readonly incompatibleContractReporter: IncompatibleContractReporter,
        private readonly metrics: TokenIndexerMetrics,
    ) {}

    shouldIndex(contract: Contract): WithFeatures<EntrypointData<TRawChange>> | false {
        const entrypoint = this.findSupportedEntrypoint(contract);
        if (!entrypoint) {
            return false;
        }

        const processor = this.processors.findProcessor(entrypoint.parameterSchema, contract.address);
        if (!processor) {
            this.incompatibleContractReporter.report(contract.address, entrypoint.entrypoint, entrypoint.parameterSchema.michelson);
            this.metrics.processorUsage.inc({ name: 'unknown', feature: entrypoint.entrypoint });
            return false;
        }

        const processorName = getConstructorName(processor);
        this.metrics.processorUsage.inc({ name: processorName, feature: entrypoint.entrypoint });
        return {
            data: new EntrypointData(entrypoint.entrypoint, processor),
            contractFeatures: [
                ...this.contractFeatures,
                ...processor.contractFeatures,
                processorName as ContractFeature,
            ],
        };
    }

    *indexTransaction(
        transactionParameter: TransactionParameter,
        context: TokenTransactionIndexingContext<EntrypointData<TRawChange>>,
    ): Iterable<PartialIndexedChange> {
        if (transactionParameter.entrypoint !== context.contractData.entrypoint) {
            return;
        }

        for (const rawChange of context.contractData.processor.deserializeRawChanges(transactionParameter, context)) {
            yield this.createIndexedChange(rawChange);
        }
    }

    private findSupportedEntrypoint(contract: Contract): Nullish<{ entrypoint: string; parameterSchema: MichelsonSchema }> {
        for (const entrypoint of this.supportedEntrypoints) {
            const parameterSchema = contract.parameterSchemas.entrypoints[entrypoint];
            if (!isNullish(parameterSchema)) {
                return { entrypoint, parameterSchema };
            }
        }
        return null;
    }

    protected abstract createIndexedChange(rawChange: TRawChange): PartialIndexedChange;
}

export class EntrypointData<TRawChange> {
    constructor(
        readonly entrypoint: string,
        readonly processor: EntrypointProcessor<TRawChange>,
    ) {}

    toJSON(): unknown {
        return {
            entrypoint: this.entrypoint,
            processor: getConstructorName(this.processor),
        };
    }
}
