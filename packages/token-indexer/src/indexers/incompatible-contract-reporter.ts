import { Michelson } from '@tezos-dappetizer/indexer';
import { injectLogger, isNullish, Logger } from '@tezos-dappetizer/utils';
import { singleton } from 'tsyringe';

import { TokenIndexerInternalConfig } from '../config/token-indexer-internal-config';

@singleton()
export class IncompatibleContractReporter {
    private readonly existing: {
        [key: string]: {
            count: number;
            reported: boolean;
        };
    } = {};

    constructor(
        private readonly config: TokenIndexerInternalConfig,
        @injectLogger(IncompatibleContractReporter) private readonly logger: Logger,
    ) {}

    report(contractAddress: string, feature: string, schemaMichelson: Michelson): void {
        if (!this.config.logIncompatibleContracts) {
            return;
        }

        const key = `${contractAddress}-${feature}`;
        if (!this.shouldReport(key)) {
            return;
        }

        this.logger.logWarning('No matching processor found for {contract} despite it has {feature} with {schema}.', {
            contract: contractAddress,
            feature,
            schema: JSON.stringify(schemaMichelson),
        });

        const logReport = this.existing[key];
        if (!isNullish(logReport)) {
            logReport.reported = true;
        }
    }

    private shouldReport(key: string): boolean {
        let value = this.existing[key];
        if (!isNullish(value)) {
            if (value.reported) {
                return false;
            }

            value.count += 1;
        } else {
            value = {
                count: 1,
                reported: false,
            };
            this.existing[key] = value;
        }

        if (value.count >= this.config.logIncompatibleContractsThreshold) {
            return true;
        }

        return false;
    }
}
