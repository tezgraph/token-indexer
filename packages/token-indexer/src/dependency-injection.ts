import {
    IndexerModuleUsingDb,
    IndexerModuleUsingDbFactory,
    registerDappetizerWithDatabase,
    registerDbInitializer,
} from '@tezos-dappetizer/database';
import { INDEXER_MODULES_DI_TOKEN, registerBlockLevelIndexingListener } from '@tezos-dappetizer/indexer';
import {
    registerBackgroundWorker,
    registerLoggedPackageInfo,
    registerOnce,
    registerSingleton,
} from '@tezos-dappetizer/utils';
import { container as globalDIContainer, DependencyContainer } from 'tsyringe';

import { TokenIndexerConfigValidator } from './config/token-indexer-config-validator';
import { TOKEN_INDEXER_MODULE_ID, TokenIndexerInternalConfig } from './config/token-indexer-internal-config';
import { tokenIndexerDbEntities } from './entities/db-entities';
import { TokenIndexerDbInitializer } from './helpers/token-indexer-db-initializer';
import { CompositeTokenIndexer } from './indexers/composite-token-indexer';
import { BurnIndexer } from './indexers/entrypoints/burn/burn-indexer';
import { MintIndexer } from './indexers/entrypoints/mint/mint-indexer';
import { TransferIndexer } from './indexers/entrypoints/transfer/transfer-indexer';
import { LedgerIndexer } from './indexers/ledger/ledger-indexer';
import { PARTIAL_TOKEN_INDEXERS_DI_TOKEN } from './indexers/partial-token-indexer';
import { TokenMetadataBackgroundWorker } from './metadata/token/token-metadata-background-worker';
import { TokenIndexingData } from './token-indexing-data';
import { TokenIndexingHandler } from './token-indexing-handler';
import { versionInfo } from './version';

export function registerTokenIndexer(diContainer: DependencyContainer = globalDIContainer): void {
    registerOnce(diContainer, 'TokenIndexer', () => {
        registerDappetizerWithDatabase(diContainer);

        registerTokenIndexerDependencies(diContainer);
        registerSingleton(diContainer, INDEXER_MODULES_DI_TOKEN, { useFactory: createTokenIndexerModule });
    });
}

export const indexerModule: IndexerModuleUsingDbFactory<TokenIndexingData> = options => {
    options.diContainer.registerInstance(TokenIndexerInternalConfig, new TokenIndexerInternalConfig(options.configElement));
    registerTokenIndexerDependencies(options.diContainer);
    return createTokenIndexerModule(options.diContainer);
};

function createTokenIndexerModule(diContainer: DependencyContainer): IndexerModuleUsingDb<TokenIndexingData> {
    return {
        name: TOKEN_INDEXER_MODULE_ID,
        dbEntities: tokenIndexerDbEntities,
        indexingCycleHandler: diContainer.resolve(TokenIndexingHandler),
        contractIndexers: [diContainer.resolve(CompositeTokenIndexer)],
    };
}

function registerTokenIndexerDependencies(diContainer: DependencyContainer): void {
    // Inject to Dappetizer features:
    registerBackgroundWorker(diContainer, { useToken: TokenIndexerConfigValidator });
    registerBackgroundWorker(diContainer, { useToken: TokenMetadataBackgroundWorker });
    registerDbInitializer(diContainer, { useToken: TokenIndexerDbInitializer });
    registerLoggedPackageInfo(diContainer, { useValue: { name: 'token-indexer', ...versionInfo } });
    registerBlockLevelIndexingListener(diContainer, { useToken: TokenMetadataBackgroundWorker });

    // Internal services:
    diContainer.register(PARTIAL_TOKEN_INDEXERS_DI_TOKEN, { useToken: BurnIndexer });
    diContainer.register(PARTIAL_TOKEN_INDEXERS_DI_TOKEN, { useToken: LedgerIndexer });
    diContainer.register(PARTIAL_TOKEN_INDEXERS_DI_TOKEN, { useToken: MintIndexer });
    diContainer.register(PARTIAL_TOKEN_INDEXERS_DI_TOKEN, { useToken: TransferIndexer });
}
