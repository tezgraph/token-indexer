# Story: Indexing of Tokens

**As** a Token Indexer user,  
**When** indexing a block  
**I want** Token Indexer to collect respective tokens,  
**So that** I can use them further.

## Scenario: Indexing Blocks from 1875165 to 1875166

**When** Token Indexer indexes blocks from 1875165 to 1875166  
**Then** Token Indexer collects following tokens:

| Id | Contract Address | Tzip 12 Name | Tzip 12 Symbol | Operation Group Hash |
| --- | --- | --- | --- | --- |
| 17 | KT18fp5rcTW7mbWDmzFwjLDUhs5MeJmagDSZ | Wrapped USDC | wUSDC | onvpKU2pqCaPLDMYSF3FEHduac7yM2rC2VWy4n63WQKMMdPkAH5 |
| 0 | KT1AFA2mwNUMNd4SsujE1YYp29vd8BZejyKW | hic et nunc DAO | hDAO | opJzi1yGQ6uRkBZXdfCeBV9TMPRXCVUk1y7izYQ5av2YfHJc1rf |
| 43 | KT1Ak8AFA54waFVMyPXNE925cUaQjFEqxuYN | HOLLOW #43 | HLWS | oo5vpttMmtfLHeVDc3rzdijaD9XMtqME97Kyk5qVukfYLQSN7SE |
| 1 | KT1DUpMhmqjJZZo58dMqmKufGbFrZJBvzpkX | Gate of Prosperity - Creation Time | OBJKTCOM | ooHJ3XKzzYzVdj1VgrB2pww4vZih4939vjz9chbMgFvm8GyWStP |
| 0 | KT1E8CrG6uznYAG9vZVGtApMJwwTScxPEUKq | PLENTY-uUSD LP | PLP | opWNKpWXxv7vYmXLwKakj2bKx9ny3JMjYbM4FmWnfJ91AKyWw3Q |
| 0 | KT1GRSvLoikDsXujKgZPsGLX8k8VvR2Tq95b | Plenty DAO | PLENTY | ooqtph98j72juYT1ueY2xMbNTii6N5JzTBkWTt7XuAiMWrAS6zX |
| 17 | KT1J7pmSsdwpxfa3f9uQuwUKhi8qy3mQW5gU | screentime 17 | OBJKTCOM | oocrZyVexgtowom6ZY874aMVg7xRUJjvHQCuCDhEQAVopaX4kve |
| 0 | KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV | Kolibri USD | kUSD | onuHbmuN7QkWfTPw5XpWLwbB288XjkZDpqjTKmwWffCr7FgiQAq |
| 32956 | KT1KEa8z6vWXDJrVqtMrAeDVzsvxat3kHaCE | RIO #44 | GENTK | op4Ze2K2vebMsB3RJFneiDWyUxysYRczvBZkLJELu5vHCT6gfjf |
| 5 | KT1LL4HvGebpNriz4nj8wtTzP1QHhktRVo8z | DOS THRASH #E006 | OBJKTCOM | op8PhnqyD8XrucH8QBtBfJDb2fUNaCJvMWedxg72giHYHQDZvso |
| 10 | KT1PvehkyHwypGNnWSyGnkSfsSjekjWDvxQi | Anguilla Fishing Boat | OBJKTCOM | ooxfZ6VKKRCRbyGvGubZj9Zni11VZHRV1dCfErqcsSiASLVjMs2 |
| 139062 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Wild Swimming | OBJKT | oomRw2gP3WGkei7f3RxT5bda6DqETDVjSBTc2PRG5D8Ea6rUM3u |
| 147630 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Noname | OBJKT | ooGMH7bBrPWMwydojeErLLGMcaEX1v1TCsUp91JCBKsg3GWAShU |
| 228058 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Window Still Life 078 | OBJKT | ooEC6EyKWv6pUxwvLTFF4RZMuUeHnunJ9q5ECb4ZFR7R6jTFxw3 |
| 284506 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Aviator | OBJKT | oozSnGf82sxEfQwrYjAJ5hs9H8yknhF3nWKfuzqiVKPGZjpP27s |
| 289220 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Desasosiego 191 | OBJKT | opHugM2emEBmzhDfdWofUNJBpeYLrHxiuKCwFYmg9TRxnDWapxw |
| 291445 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Time | OBJKT | oo3XBv6sfinT6UMokXdGS29WMHqCbN4ctFgCkZxD1pUkPNYFXXf |
| 372968 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Lakshmi | OBJKT | ooo13fc5mMky827WcwmmJEVRKVbxJFDS4TxmvxER9HswM6xzHVz |
| 431399 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Neighbours | OBJKT | ooLf3YZaHGAmNCsCrx6e5YQ2YKwjJ1w2pDBRMMMrXE9DDkChwJ3 |
| 436382 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | SCHOOL PICTURE DAY | OBJKT | ooBBpUaYVnndkroMBauuJcAJYndAjyB8bdk8CDwKuvWJCV1CcMt |
| 455560 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | end of the party  | OBJKT | opDPUF89HRXZwETasswRjNy8etN8nLFarHCR3Xnh1gBVHeKkyAD |
| 494129 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Dot Matrix Punks #4156 | OBJKT | ooGMH7bBrPWMwydojeErLLGMcaEX1v1TCsUp91JCBKsg3GWAShU |
| 510209 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | yaramas | OBJKT | oo4zi88XLnqrNoTGrr5xfxWZ1ju5HaPL7SXyhsBEdtcCjchMuAP |
| 511084 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Tezos Turtlez - Inception Token | OBJKT | opYV3kYXf5YJCdjBpN5jLMHvtWsG3k1Vjuo41pxQwz8wVx6NRWK |
| 521203 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | As You Can Feel | OBJKT | onwcAPYwUQd9EC275CdPf9Y5xwfoArtU5rZ1CeheREe3zAgf2D7 |
| 522253 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | C O J | OBJKT | oo2rYQ86SVuEsmcGxCq6rhD1jEkXNQzx3NTmzZnjBsFDhH7Dpky |
| 527929 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | 1986 ONE GOURD DGS Jeff Reardon Series 7 Insert #DGSJR2 (ORANGE) | OBJKT | opRx31uZptbRy5vAA5b9FwK249G34sdwt3JXEL2QShMme9vgHHv |
| 536144 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Fomorobo JiiG | OBJKT | opGNqLqX9jEqBSKsZBsJCVDna3jEweXd4aZDpVSgQP2mYNaYsU5 |
| 538489 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | dwarf hairgrass | OBJKT | oomeEqMWmtY9LXRTBHjAGvwr3WDepT1uahB6drhW15WWeKXUQT4 |
| 540949 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Chomp Shroom | OBJKT | ooeXGgkPCtGPMpkCLvgxoDunZxUBy8M9LnsjEKiFHArbPaEp3vx |
| 541976 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | The Conqueror | OBJKT | ookh8dLmLGEpoTa4qfhdREngCRAAQa77DGUwM3wkrL9wBJCWfb4 |
| 542145 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Green Eyes | OBJKT | onhL4pwMMUMpSLSXVCMkfmDUNGBp4K7wz5ktLD2YGZNUCsBMCoo |
| 542636 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Palette Cycle City | OBJKT | op4DGhy9rUrALYw2xNWJEugtGFKYzVudVGV744JXPoF4TQqWxqJ |
| 542666 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | S0lst1ce 2021 Poster | OBJKT | onoTPHECtegjBprJikCzaMg2mmsBmQYE3o7FELytc5ceeb8i8N3 |
| 542700 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | SQUIDHEAD #SHU085 | OBJKT | op9fHmGEyhmGETq7kaJbFn9C5bJkTCFPXJEC5f8b4B21T4QTzFi |
| 542777 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Colors | OBJKT | oopUdfY12U82haWtTVam81PkGPcsXJxcgpCc3A22hZAbFNqQoFN |
| 542780 | KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton | Ryuk #1 | OBJKT | opCsneE2sQkpidiuAT9iu7TG8DWowFurwmkD4mDrNYgfrRvpean |
| 0 | KT1Rpviewjg82JgjGfAKFneSupjAR1kUhbza | xPLENTY | xPLENTY | ooqtph98j72juYT1ueY2xMbNTii6N5JzTBkWTt7XuAiMWrAS6zX |
| 0 | KT1ViVwoVfGSCsDaxjwoovejm1aYSGz7s2TZ | Tezotop 0000 | TZTOP | op3KUkb6hHWARo29L6qQxUjVWg6QLNcY1QD9mniHxU8defPZRw4 |
| 12875 | KT1ViVwoVfGSCsDaxjwoovejm1aYSGz7s2TZ | Rocks | TZTOP | op3KUkb6hHWARo29L6qQxUjVWg6QLNcY1QD9mniHxU8defPZRw4 |
| 1019 | KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD | Rutherford B. Hayes (C) | POTUS19-C | ontrwAVyULriVh8rDRH7ggEwG2rZkprdjV6rD6NDkBiFaApPubt |
| 1027 | KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD | William H. Taft (C) | POTUS27-C | op1WcfxbniH6w4osJnrhTyptuxfFKk7c5jmGy7wA1k3n2VcDeUi |
| 1037 | KT1WGDVRnff4rmGzJUbdCRAJBmYt12BrPzdD | Richard Nixon (C) | POTUS37-C | ooiKCnnqPmLNK8oX3a1sa1efEpo5c8XEdW5SDZaukNE1GkAK1xt |
| 59 | KT1XHyDd8ScUteYRWNocHgwZtj2BwBcoCZZo | CUBE HEAD #170 | OBJKTCOM | onmBD4cvv1BT5WBpfZFnCRe3X1h7SLMnoZYdnWYY7YBi4EnG9a8 |
| 0 | KT1Xobej4mc6XgEjDoJoHtTKgbD1ELMvcQuL | youves YOU Governance | YOU | ooDTXyNLKf5f1fzDJsLKXx2yEokdbZDTfqf36neC8nfVjuiooEq |
| 0 | KT1XRPEPXbZK25r3Htzp2o1x7xdMMmfocKNW | youves uUSD | uUSD | ooDTXyNLKf5f1fzDJsLKXx2yEokdbZDTfqf36neC8nfVjuiooEq |