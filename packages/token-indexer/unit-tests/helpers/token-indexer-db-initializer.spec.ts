import '../../../../test-utilities/mocks/set-sqlite-db-types';

import { describeMember, expectToThrowAsync, TestLogger } from '../../../../test-utilities/mocks';
import { TokenIndexerDbInitializer } from '../../src/helpers/token-indexer-db-initializer';
import { getCmdTestHelper, TestDatabase } from '../test-database';

describe(TokenIndexerDbInitializer.name, () => {
    let target: TokenIndexerDbInitializer;
    let db: TestDatabase;

    beforeEach(async () => {
        target = new TokenIndexerDbInitializer(new TestLogger());
        db = await getCmdTestHelper();
    });

    afterEach(async () => {
        await db.destroy();
    });

    describeMember<typeof target>('beforeSynchronization', () => {
        it('should not fail drop the view if not exists', async () => {
            await target.beforeSynchronization(db.dataSource);
        });

        it('should drop the view if exists', async () => {
            await db.dataSource.query(`CREATE VIEW current_balance AS SELECT * FROM balance;`);

            await target.beforeSynchronization(db.dataSource);

            await expectToThrowAsync(async () => db.dataSource.query('SELECT * FROM current_balance'));
        });
    });

    describeMember<typeof target>('afterSynchronization', () => {
        it('should create the view', async () => {
            await target.afterSynchronization(db.dataSource);

            await db.dataSource.query('SELECT * FROM current_balance');
        });
    });
});
