import { DAPPETIZER_DB_ENTITIES, dappetizerNamingStrategy, DbCommand, DbEntity } from '@tezos-dappetizer/database';
import { BigNumber } from 'bignumber.js';
import crypto from 'crypto';
import fs from 'fs';
import { DataSource } from 'typeorm';

import { tokenIndexerDbEntities } from '../src/entities/db-entities';
import { MetadataState } from '../src/entities/metadata-state';

export interface TestDatabase {
    readonly dataSource: DataSource;
    readonly firstBlockHash: string;
    readonly firstOperationGroupHash: string;

    executeCmd<T>(cmd: DbCommand<T>): Promise<T>;
    verifyData(sql: string, expectedData: unknown[]): Promise<void>;
    destroy(): Promise<void>;

    insertBlock(values: { hash: string }): Promise<void>;

    insertContract(values: {
        addr: string;
        firstBlockHash?: string;
    }): Promise<void>;

    insertToken(values: {
        contractAddr: string;
        id: BigNumber;
        firstBlockHash?: string;
        metadata?: MetadataState;
    }): Promise<void>;

    insertBalance(values: {
        contractAddr: string;
        tokenId: BigNumber;
        ownerAddr: string;
        amount?: number;
        validFromBlockHash?: string;
        validUntilBlockHash?: string;
    }): Promise<void>;
}

export async function getCmdTestHelper(): Promise<TestDatabase> {
    const dbFile = `.tmp-db/test-${crypto.randomBytes(16).toString('hex')}.sqlite`;
    const entities: DbEntity[] = [...DAPPETIZER_DB_ENTITIES, ...tokenIndexerDbEntities];
    const dataSource = new DataSource({
        type: 'sqlite',
        database: dbFile,
        entities,
        synchronize: true,
        namingStrategy: dappetizerNamingStrategy,
    });

    const firstBlockHash = 'BM4T2LHhYwfP5rz8ucXev9Rdoo5RmCBb88mTrAoSd4CGFxKx4Bt';
    const ogh = 'onvgKt5BTxWunRXjno36XeBNwVnjuyeYGZYjGTcpFAoPxt5yG3n';
    let blockLevelCounter = 1;

    const helper: TestDatabase = {
        dataSource,
        firstBlockHash,
        firstOperationGroupHash: ogh,

        executeCmd: async cmd => {
            return cmd.execute(dataSource.manager);
        },

        verifyData: async (sql, expectedData) => {
            const dbData = await dataSource.query(sql);
            expect(dbData).toEqual(expectedData);
        },

        destroy: async () => {
            await dataSource.destroy();
            fs.unlinkSync(dbFile);
        },

        insertBlock: async values => {
            await dataSource.query(`INSERT INTO block (hash, level, predecessor, timestamp)`
                + ` VALUES ('${values.hash}', ${blockLevelCounter++}, '${values.hash}', '${new Date().toISOString()}')`);
        },

        insertContract: async values => {
            const firstBlock = values.firstBlockHash ?? firstBlockHash;

            await dataSource.query(`INSERT INTO contract (address, first_operation_group_hash, first_block_hash, metadata_state)`
                + ` VALUES ('${values.addr}', '${ogh}', '${firstBlock}', '${MetadataState.Ok}')`);
        },

        insertToken: async values => {
            const id = values.id.toFixed();
            const firstBlockSql = values.firstBlockHash ?? firstBlockHash;
            const metadata = values.metadata ?? MetadataState.Ok;

            await dataSource.query(`INSERT INTO token (contract_address, id, first_operation_group_hash, first_block_hash, metadata_state)`
                + ` VALUES ('${values.contractAddr}', '${id}', '${ogh}', '${firstBlockSql}', '${metadata}')`);
        },

        insertBalance: async values => {
            const { contractAddr, ownerAddr } = values;
            const tokenId = values.tokenId.toFixed();
            const amount = values.amount ?? 123;
            const validFromSql = `'${values.validFromBlockHash ?? firstBlockHash}'`;
            const validUntilSql = values.validUntilBlockHash ? `'${values.validUntilBlockHash}'` : 'NULL';

            await dataSource.query(`INSERT INTO balance (`
                + `token_contract_address, token_id, owner_address, amount, operation_group_hash, valid_from_block_hash, valid_until_block_hash)`
                + ` VALUES ('${contractAddr}', '${tokenId}', '${ownerAddr}', ${amount}, '${ogh}', ${validFromSql}, ${validUntilSql})`);
        },
    };

    await dataSource.initialize();
    await helper.insertBlock({ hash: firstBlockHash });
    return helper;
}
