import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { NFT_DEFAULT_AMOUNT } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import { Tzip12MultiAssetLedgerProcessor } from '../../src/indexers/ledger/processors/tzip12-multi-asset-ledger-processor';

export const hicEtNuncOBJKTs: TestContract = {
    address: 'KT1RJ6PbjHpwc3M5rw5s2Nbmefwbuwbdxton',
    name: 'Hic et Nunc OBJKTs',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    ledger: {
        expectedProcessor: Tzip12MultiAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                },
                { prim: 'nat' },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: {
                    prim: 'Pair',
                    args: [
                        { bytes: '0162d817497a95fe1e45d315d5a1ac85c328ad980e00' },
                        { int: '172072' },
                    ],
                },
                value: { int: '48' },
            },
            expected: [{
                tokenId: new BigNumber('172072'),
                ownerAddress: 'KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn',
                amount: new BigNumber('48'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from_'] },
                    {
                        prim: 'list',
                        args: [{
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%to_'] },
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'nat', annots: ['%token_id'] },
                                        { prim: 'nat', annots: ['%amount'] },
                                    ],
                                },
                            ],
                        }],
                        annots: ['%txs'],
                    },
                ],
            }],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '0162d817497a95fe1e45d315d5a1ac85c328ad980e00' },
                    [
                        {
                            prim: 'Pair',
                            args: [
                                { bytes: '000009b4a5e8118ec9c88ac59a11988ee4e130ad20c1' },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '121154' },
                                        { int: '1' },
                                    ],
                                },
                            ],
                        },
                    ],
                ],
            }],
            expected: [{
                tokenId: new BigNumber('121154'),
                fromAddress: 'KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn',
                toAddress: 'tz1LXMBVbKdGTjZickA8zvcMYCPEQ1UvtjDe',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%address'] },
                        { prim: 'nat', annots: ['%amount'] },
                    ],
                },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: ['%token_id'] },
                        {
                            prim: 'map',
                            args: [
                                { prim: 'string' },
                                { prim: 'bytes' },
                            ],
                            annots: ['%token_info'],
                        },
                    ],
                },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '0000399b5389e87ecfe328b2f17750c1b0bb2d11592f' },
                            { int: '1' },
                        ],
                    },
                    {
                        prim: 'Pair',
                        args: [
                            { int: '175824' },
                            [{
                                prim: 'Elt',
                                args: [
                                    { string: '' },
                                    { bytes: '697066733a2f2f516d586e61776872715453343570325454787a7a795a6e6d42656f556770584a6553375648427873545166594434' }, // eslint-disable-line max-len
                                ],
                            }],
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: new BigNumber('175824'),
                toAddress: 'tz1QtdHKG7bNxYvo9KN5xc94BpmQvtYrVqkU',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
};
