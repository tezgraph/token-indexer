import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { Tzip12SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/tzip12-single-asset-ledger-processor';

export const wXTZ: TestContract = {
    address: 'KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH',
    name: 'wXTZ',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: Tzip12SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                { prim: 'nat' },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '01eb6844466e9456a0d4beb961d783f646a4d347d400' },
                value: { int: '56211626031' },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'KT1W3VGRUjvS869r4ror8kdaxqJAZUbPyjMT',
                amount: new BigNumber(56211626031),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%to'] },
                        { prim: 'nat', annots: ['%value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '00009f0db6d3288088ad2138b2013b530d4743bbaa98' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '01eb6844466e9456a0d4beb961d783f646a4d347d400' },
                            { int: '100000000' },
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1a92ZVmHDSqZ2dYbcrTZG9baK7bXSMRazZ',
                toAddress: 'KT1W3VGRUjvS869r4ror8kdaxqJAZUbPyjMT',
                amount: new BigNumber('100000000'),
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%to'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '0000f9a7a524e46555b0ca9bab361dea6329ce685aa2' },
                    { int: '29770' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: 'tz1iQ5n52oSKgdtdYtqEJgKmQYbL1nn2poB8',
                amount: new BigNumber('29770'),
            }],
        },
    },
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%burn'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '00009dc822d6662206afe5d255c19a7ad8d29798cd67' },
                    { int: '237887203' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1a2JXwDALzGrGh5A78ZLAs2vDkV1kFQmD4',
                amount: new BigNumber('237887203'),
            }],
        },
    },
};
