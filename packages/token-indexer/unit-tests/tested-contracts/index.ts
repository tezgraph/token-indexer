import { ETHtz } from './eth-tz';
import { fDAOToken } from './f-dao-token';
import { hDAO } from './h-dao';
import { hicEtNuncOBJKTs } from './hic-et-nunc-objkts';
import { kUSD } from './k-usd';
import { kalamintArtHouse } from './kalamint-art-house';
import { MAGToken } from './mag-token';
import { PAUL } from './paul';
import { PLENTY } from './plenty';
import { QuipuSwapCRUNCH } from './quipu-swap-crunch';
import { QuipuSwapPLENTY } from './quipu-swap-plenty';
import { SebuhNet } from './sebuh-net';
import { SergioPerezSpecialEdition } from './sergio-perez-special-edition';
import { STKR } from './stkr';
import { TestContract } from './test-contract';
import { TezosDomains } from './tezos-domains';
import { TezosMandalaV2 } from './tezos-mandala';
import { tzBTC } from './tz-btc';
import { unsupported } from './unsupported';
import { USDtz } from './usd-tz';
import { wXTZ } from './w-xtz';
import { WrapProtocolGovernanceToken } from './wrap-protocol-governance-token';
import { YouvesSyntheticAsset } from './youves-synthetic-asset';

export const testContracts: readonly TestContract[] = [
    ETHtz,
    fDAOToken,
    hDAO,
    hicEtNuncOBJKTs,
    kalamintArtHouse,
    kUSD,
    MAGToken,
    PAUL,
    PLENTY,
    QuipuSwapCRUNCH,
    QuipuSwapPLENTY,
    SebuhNet,
    SergioPerezSpecialEdition,
    STKR,
    TezosDomains,
    TezosMandalaV2,
    tzBTC,
    unsupported,
    USDtz,
    WrapProtocolGovernanceToken,
    wXTZ,
    YouvesSyntheticAsset,
];
