import { TestContract } from './test-contract';

export const unsupported: TestContract = {
    address: 'abc',
    name: 'Bullshit',
    bigMapName: 'ledger',
    contractType: null,
    ledger: {
        expectedProcessor: undefined,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address', annots: [':owner'] },
                { prim: 'address', annots: [':user'] },
            ],
            annots: ['%ledger'],
        },
    },
    transfer: {
        expectedProcessor: undefined,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'nat', annots: ['%value1'] },
                { prim: 'nat', annots: ['%value2'] },
            ],
            annots: ['%transfer'],
        },
    },
    mint: {
        expectedProcessor: undefined,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'nat' },
                { prim: 'nat' },
                { prim: 'nat' },
            ],
            annots: ['%mint'],
        },
    },
    burn: {
        expectedProcessor: undefined,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address' },
                { prim: 'address' },
                { prim: 'address' },
            ],
            annots: ['%burn'],
        },
    },
};
