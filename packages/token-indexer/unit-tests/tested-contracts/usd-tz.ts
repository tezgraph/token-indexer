import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { Tzip7LedgerProcessor } from '../../src/indexers/ledger/processors/tzip7-ledger-processor';

export const USDtz: TestContract = {
    address: contractAddresses.USD_TZ,
    name: 'USDtz',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: Tzip7LedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address', annots: [':user'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: [':balance'] },
                        {
                            prim: 'map',
                            args: [
                                { prim: 'address', annots: [':spender'] },
                                { prim: 'nat', annots: [':value'] },
                            ],
                            annots: [':approvals'],
                        },
                    ],
                },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '01d34d372316d8f6aea54887b8b1b4eac1167255ac00' },
                value: {
                    prim: 'Pair',
                    args: [
                        { int: '5528150949' },
                        [],
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'KT1Tr2eG3eVmPRbymrbU2UppUmKjFPXomGG9',
                amount: new BigNumber(5528150949),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: [':to'] },
                        { prim: 'nat', annots: [':value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '00006f88d2d49a1db259b2ad51fec6df605c91d1f8b3' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '018a437098fc89e91d970831da98e70f33d08b93f300' },
                            { int: '62443000' },
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1Vomc5aWD74WLdvXb88CfKMiFD2MA4jQd5',
                toAddress: 'KT1MBqc3GHpApBXaBZyvY63LF6eoFyTWtySn',
                amount: new BigNumber('62443000'),
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':to'] },
                { prim: 'nat', annots: [':value'] },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1VcTGqexTzm7P4PERtfiR4Ey6HLxSCCTqN' },
                    { int: '10000000000' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: 'tz1VcTGqexTzm7P4PERtfiR4Ey6HLxSCCTqN',
                amount: new BigNumber('10000000000'),
            }],
        },
    },
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':from'] },
                { prim: 'nat', annots: [':value'] },
            ],
            annots: ['%burn'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1VcTGqexTzm7P4PERtfiR4Ey6HLxSCCTqN' },
                    { int: '11639000000' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1VcTGqexTzm7P4PERtfiR4Ey6HLxSCCTqN',
                amount: new BigNumber('11639000000'),
            }],
        },
    },
};
