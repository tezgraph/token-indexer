import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { NFT_DEFAULT_AMOUNT } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import { Tzip12MultiAssetLedgerProcessor } from '../../src/indexers/ledger/processors/tzip12-multi-asset-ledger-processor';

export const hDAO: TestContract = {
    address: 'KT1AFA2mwNUMNd4SsujE1YYp29vd8BZejyKW',
    name: 'hDAO',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    ledger: {
        expectedProcessor: Tzip12MultiAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                },
                { prim: 'nat' },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: {
                    prim: 'Pair',
                    args: [
                        { bytes: '0000e1cf16ce52257f2f9a0578c93ac484e36d94145e' },
                        { int: '7' },
                    ],
                },
                value: { int: '58474360' },
            },
            expected: [{
                tokenId: new BigNumber('7'),
                ownerAddress: 'tz1gDzp5mM7G7CW5CBeyG4pxHcv4jN8MM5Df',
                amount: new BigNumber('58474360'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from_'] },
                        {
                            prim: 'list',
                            args: [
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'address', annots: ['%to_'] },
                                        {
                                            prim: 'pair',
                                            args: [
                                                { prim: 'nat', annots: ['%token_id'] },
                                                { prim: 'nat', annots: ['%amount'] },
                                            ],
                                        },
                                    ],
                                },
                            ],
                            annots: ['%txs'],
                        },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '00000f0549a76ece3039c35308fc07f467b9b980ef73' },
                    [
                        {
                            prim: 'Pair',
                            args: [
                                { bytes: '01b39686f116bb35115559f7e781200850e02854c400' },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '8' },
                                        { int: '2000000' },
                                    ],
                                },
                            ],
                        },
                    ],
                ],
            }],
            expected: [{
                tokenId: new BigNumber('8'),
                fromAddress: 'tz1M1TAPmAWgZGR4hcC1MFALjAVSih3YCXE2',
                toAddress: 'KT1QxLqukyfohPV5kPkw97Rs6cw1DDDvYgbB',
                amount: new BigNumber('2000000'),
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%address'] },
                        { prim: 'nat', annots: ['%amount'] },
                    ],
                },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: ['%token_id'] },
                        {
                            prim: 'map',
                            args: [
                                { prim: 'string' },
                                { prim: 'bytes' },
                            ],
                            annots: ['%token_info'],
                        },
                    ],
                },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '0000399b5389e87ecfe328b2f17750c1b0bb2d11592f' },
                            { int: '1' },
                        ],
                    },
                    {
                        prim: 'Pair',
                        args: [
                            { int: '175824' },
                            [{
                                prim: 'Elt',
                                args: [
                                    { string: '' },
                                    { bytes: '697066733a2f2f516d586e61776872715453343570325454787a7a795a6e6d42656f556770584a6553375648427873545166594434' }, // eslint-disable-line max-len
                                ],
                            }],
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: new BigNumber('175824'),
                toAddress: 'tz1QtdHKG7bNxYvo9KN5xc94BpmQvtYrVqkU',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
};
