import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/incompatible/single-asset-ledger-processor';

export const QuipuSwapPLENTY: TestContract = {
    address: 'KT1X1LgNkQShpF9nRLYw3Dgdy4qp38MX617z',
    name: 'QuipuSwap PLENTY',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                {
                                    prim: 'map',
                                    args: [
                                        { prim: 'address' },
                                        { prim: 'nat' },
                                    ],
                                    annots: ['%allowances'],
                                },
                                { prim: 'nat', annots: ['%balance'] },
                            ],
                        },
                        { prim: 'nat', annots: ['%frozen_balance'] },
                    ],
                },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '0000421731d40a24e13087a9e3bcc9879540b4a6c4f8' },
                value: {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                [
                                    {
                                        prim: 'Elt',
                                        args: [
                                            { bytes: '0121c8bb49467f12ea97fb825ee8c7cc8205f751b000' },
                                            { int: '2000000' },
                                        ],
                                    },
                                ],
                                { int: '2323280' },
                            ],
                        },
                        { int: '0' },
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz1RfV5NXXaRFBBd36R1TjZBMnWxJpEjJ92u',
                amount: new BigNumber('2323280'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                { prim: 'address', annots: ['%to'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '01f5f864796ed4e26c71560f3fa17ca493e8b616a500' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '00000000000001ea0615329d18b762780e255b438545' },
                            { int: '2815455870' },
                        ],
                    },
                ],
            },
            expected: [{
                fromAddress: 'KT1X1LgNkQShpF9nRLYw3Dgdy4qp38MX617z',
                toAddress: 'tz1Ke2h7sFBHspKKtHSK5P9icx4AHLrq1n8b',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(2815455870),
            }],
        },
    },
};
