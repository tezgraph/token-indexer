import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import { SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/incompatible/single-asset-ledger-processor';

export const QuipuSwapCRUNCH: TestContract = {
    address: 'KT1RRgK6eXvCWCiEGWhRZCSVGzhDzwXEEjS4',
    name: 'QuipuSwap CRUNCH',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    ledger: {
        expectedProcessor: SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                {
                                    prim: 'set',
                                    args: [{ prim: 'address' }],
                                    annots: ['%allowances'],
                                },
                                { prim: 'nat', annots: ['%balance'] },
                            ],
                        },
                        { prim: 'nat', annots: ['%frozen_balance'] },
                    ],
                },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '00018fd2aaba7863aff0c56aafa9a4410d7bf922e447' },
                value: {
                    prim: 'Pair',
                    args: [
                        {
                            prim: 'Pair',
                            args: [
                                [{ bytes: '01f1a3f37892b5cd5f2de31520501d7bf176fef0fb00' }],
                                { int: '5034152' },
                            ],
                        },
                        { int: '0' },
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz2MRhfAKUyNZDKH8XBzmXyCwd2h5ySbgZJJ',
                amount: new BigNumber('5034152'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%from_'] },
                        {
                            prim: 'list',
                            args: [
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'address', annots: ['%to_'] },
                                        { prim: 'nat', annots: ['%token_id'] },
                                        { prim: 'nat', annots: ['%amount'] },
                                    ],
                                },
                            ],
                            annots: ['%txs'],
                        },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '00000000000001ea0615329d18b762780e255b438545' },
                    [{
                        prim: 'Pair',
                        args: [
                            { bytes: '01b8c202811399ee25a65d2b4c25ffcf420b33f57d00' },
                            {
                                prim: 'Pair',
                                args: [
                                    { int: '0' },
                                    { int: '117346915861336' },
                                ],
                            },
                        ],
                    }],
                ],
            }],
            expected: [{
                fromAddress: 'tz1Ke2h7sFBHspKKtHSK5P9icx4AHLrq1n8b',
                toAddress: 'KT1RRgK6eXvCWCiEGWhRZCSVGzhDzwXEEjS4',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(117346915861336),
            }],
        },
    },
};
