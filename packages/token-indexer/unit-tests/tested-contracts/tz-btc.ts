import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { tzBTCLedgerProcessor } from '../../src/indexers/ledger/processors/incompatible/tz-btc-ledger-processor';

export const tzBTC: TestContract = {
    address: contractAddresses.TZ_BTC,
    name: 'tzBTC',
    bigMapName: '0',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: tzBTCLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'bytes' },
                { prim: 'bytes' },
            ],
        },
        sample: {
            input: {
                key: { bytes: '05070701000000066c65646765720a0000001600009472982d7f6b096bc57d6da95e0b8ec8ee37e72f' },
                value: { bytes: '05070700b0e7ad5f0200000000' },
            },
            expected: [{
                tokenId: new BigNumber(0),
                ownerAddress: 'tz1ZAwyfujwED4yUhQAtc1eqm4gW5u2Xiw77',
                amount: new BigNumber(99990000),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: [':to'] },
                        { prim: 'nat', annots: [':value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '01ece491d1ef313570a669e1c6d96af52d1ce0785c00' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '0000fc03043aaa24e72df909fd0cc3c550e8cbefb24f' },
                            { int: '233553' },
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'KT1WBLrLE2vG8SedBqiSJFm4VVAZZBytJYHc',
                toAddress: 'tz1icYb3y2M4LGTCoAJMvyQAvpqvaaCbzTzc',
                amount: new BigNumber('233553'),
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':to'] },
                { prim: 'nat', annots: [':value'] },
            ],
            annots: ['%mint'],
        },
    },
};
