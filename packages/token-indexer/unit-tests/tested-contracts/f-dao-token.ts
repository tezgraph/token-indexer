import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import { SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/incompatible/single-asset-ledger-processor';

export const fDAOToken: TestContract = {
    address: 'KT1KPoyzkj82Sbnafm6pfesZKEhyCpXwQfMc',
    name: 'fDAO Token',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    ledger: {
        expectedProcessor: SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'set',
                            args: [{ prim: 'address' }],
                            annots: ['%allowances'],
                        },
                        { prim: 'nat', annots: ['%balance'] },
                    ],
                },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '00006e4b0cd4b0629214edb59b746d6683fbd0b8ac75' },
                value: {
                    prim: 'Pair',
                    args: [
                        [
                            { bytes: '017c1c989860f669c280050458aa6418f10bffbe6800' },
                            { bytes: '01a2dc39be5a81bf161f10709e3d6c6523f256e07f00' },
                        ],
                        { int: '681245' },
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz1VhCvo2M7ne6GihA46hqhEoPceFo1Kbhg5',
                amount: new BigNumber('681245'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from_'] },
                    {
                        prim: 'list',
                        args: [{
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%to_'] },
                                {
                                    prim: 'pair',
                                    args: [
                                        { prim: 'nat', annots: ['%token_id'] },
                                        { prim: 'nat', annots: ['%amount'] },
                                    ],
                                },
                            ],
                        }],
                        annots: ['%txs'],
                    },
                ],
            }],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '00000000000001ea0615329d18b762780e255b438545' },
                    [{
                        prim: 'Pair',
                        args: [
                            { bytes: '0124db78d6ed72ef937df9ba68e7d230ccf8dbfc3800' },
                            {
                                prim: 'Pair',
                                args: [
                                    { int: '0' },
                                    { int: '98234694' },
                                ],
                            },
                        ],
                    }],
                ],
            }],
            expected: [{
                fromAddress: 'tz1Ke2h7sFBHspKKtHSK5P9icx4AHLrq1n8b',
                toAddress: 'KT1BweorZK1CJDEu76SyKcxfzeiAxip73Kot',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(98234694),
            }],
        },
    },
};
