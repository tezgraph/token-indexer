import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/incompatible/single-asset-ledger-processor';

export const MAGToken: TestContract = {
    address: 'KT1H5KJDxuM9DURSfttepebb6Cn7GbvAAT45',
    name: 'MAG Token',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                {
                                    prim: 'map',
                                    args: [
                                        { prim: 'address' },
                                        { prim: 'nat' },
                                    ],
                                    annots: ['%approvals'],
                                },
                                { prim: 'nat', annots: ['%balance'] },
                                { prim: 'nat', annots: ['%frozenBalance'] },
                            ],
                        },
                        { prim: 'nat', annots: ['%reward'] },
                        { prim: 'nat', annots: ['%rewardDebt'] },
                        { prim: 'nat', annots: ['%usedVotes'] },
                    ],
                },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '00000634079a41b082c26418063e106a3acf44f7a610' },
                value: {
                    prim: 'Pair',
                    args: [
                        {
                            prim: 'Pair',
                            args: [
                                [
                                    {
                                        prim: 'Elt',
                                        args: [
                                            { bytes: '01ef852dcd37c99d0cc06a8d8348dbbfb8f0464b0500' },
                                            { int: '0' },
                                        ],
                                    },
                                ],
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '29654952848' },
                                        { int: '0' },
                                    ],
                                },
                            ],
                        },
                        { int: '439951664767358' },
                        { int: '0' },
                        { int: '0' },
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz1LCq5Mez7i4hBd3DHxYXSa1mU2WZUhfR9S',
                amount: new BigNumber('29654952848'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%to'] },
                        { prim: 'nat', annots: ['%value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
    },
};
