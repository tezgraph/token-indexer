import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { Tzip12SingleAssetLedgerProcessor } from '../../src/indexers/ledger/processors/tzip12-single-asset-ledger-processor';

export const STKR: TestContract = {
    address: contractAddresses.STKR,
    name: 'STKR',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: Tzip12SingleAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address' },
                { prim: 'nat' },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '00008e25b51981722550a7362ea005098ed29c698eb2' },
                value: { int: '148108862589775798267913' },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz1YbdpKKM75iwNGksA1N3UYnTjaCCUBCQC2',
                amount: new BigNumber('148108862589775798267913'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%to'] },
                        { prim: 'nat', annots: ['%value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '000079f2550dd057c5939b509ae4a10d441eb757c65a' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '0000757d52d422735928e53096777fb02dfb47473d8c' },
                            { int: '495701226500206707' },
                        ],
                    },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1WkpksR3M5fLDDzh22Cca3pvJPYejJBhU6',
                toAddress: 'tz1WMFtCLLeRiayH19ZiaErKq4Zu9p5N73Sr',
                amount: new BigNumber('495701226500206707'),
            }],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%to'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '0000e5541113098b3803b7ed4b3ff438f9b201a40284' },
                    { int: '600000000000000000000000' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: 'tz1gYc972xvf6dmEj3xxbXXiZnGoUezyC22N',
                amount: new BigNumber('600000000000000000000000'),
            }],
        },
    },
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                { prim: 'nat', annots: ['%value'] },
            ],
            annots: ['%burn'],
        },
    },
};
