import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { BurnProcessor } from '../../src/indexers/entrypoints/burn/processors/burn-processor';
import { MintProcessor } from '../../src/indexers/entrypoints/mint/processors/mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';
import { Tzip7LedgerProcessor } from '../../src/indexers/ledger/processors/tzip7-ledger-processor';

export const ETHtz: TestContract = {
    address: contractAddresses.ETH_TZ,
    name: 'ETHtz',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    ledger: {
        expectedProcessor: Tzip7LedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                { prim: 'address', annots: [':user'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'nat', annots: [':balance'] },
                        {
                            prim: 'map',
                            args: [
                                { prim: 'address', annots: [':spender'] },
                                { prim: 'nat', annots: [':value'] },
                            ],
                            annots: [':approvals'],
                        },
                    ],
                },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: { bytes: '000091269f32ce3257307d72d56ce6cd4e1093ab4946' },
                value: {
                    prim: 'Pair',
                    args: [
                        { int: '211216104353843293' },
                        [
                            {
                                prim: 'Elt',
                                args: [
                                    { bytes: '010afd0efda97f9fe8bb4d9d82ae7f31f09a9915d400' },
                                    { int: '211000000000000000' },
                                ],
                            },
                        ],
                    ],
                },
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: 'tz1YsWwRqmFmKGsu8N8ePRbqkoMuJkVK2D9E',
                amount: new BigNumber('211216104353843293'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: [':to'] },
                        { prim: 'nat', annots: [':value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '01459e49b0ef03257ed145507173ccb3c61eeee4c700' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '00004d697a8f557757bb4b1d37a963098d80adbdf88c' },
                            { int: '12029337261905684' },
                        ],
                    },
                ],
            },
            expected: [
                {
                    tokenId: SINGLE_ASSET_TOKEN_ID,
                    fromAddress: 'KT1Evsp2yA19Whm24khvFPcwimK6UaAJu8Zo',
                    toAddress: 'tz1ShM5gWyLSD997WV89nzZWxPXFGBMJ2yRw',
                    amount: new BigNumber('12029337261905684'),
                },
            ],
        },
    },
    mint: {
        expectedProcessor: MintProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':to'] },
                { prim: 'nat', annots: [':value'] },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ' },
                    { int: '20377661960000000000' },
                ],
            },
            expected: [
                {
                    tokenId: SINGLE_ASSET_TOKEN_ID,
                    toAddress: 'tz1NGQWcfuY9WfZmPjwjn2Vy3kbAcNLtFqhQ',
                    amount: new BigNumber('20377661960000000000'),
                },
            ],
        },
    },
    burn: {
        expectedProcessor: BurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: [':from'] },
                { prim: 'nat', annots: [':value'] },
            ],
            annots: ['%burn'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1gc7PsWoBiBib1kekAV7wEX5zFSshTicmZ' },
                    { int: '5000000000000000000' },
                ],
            },
            expected: [
                {
                    tokenId: SINGLE_ASSET_TOKEN_ID,
                    fromAddress: 'tz1gc7PsWoBiBib1kekAV7wEX5zFSshTicmZ',
                    amount: new BigNumber('5000000000000000000'),
                },
            ],
        },
    },
};
