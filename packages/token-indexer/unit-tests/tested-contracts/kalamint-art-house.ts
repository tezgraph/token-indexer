import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { NFT_DEFAULT_AMOUNT } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { KalamintMintProcessor } from '../../src/indexers/entrypoints/mint/processors/incompatible/kalamint-mint-processor';
import { Tzip12TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import { Tzip12MultiAssetLedgerProcessor } from '../../src/indexers/ledger/processors/tzip12-multi-asset-ledger-processor';

export const kalamintArtHouse: TestContract = {
    address: contractAddresses.KALAMINT,
    name: 'Kalamint Art House',
    bigMapName: 'ledger',
    contractType: ContractType.FA2_like,
    ledger: {
        expectedProcessor: Tzip12MultiAssetLedgerProcessor,
        schema: {
            prim: 'big_map',
            args: [
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                },
                { prim: 'nat' },
            ],
            annots: ['%ledger'],
        },
        sample: {
            input: {
                key: {
                    prim: 'Pair',
                    args: [
                        { bytes: '0162d817497a95fe1e45d315d5a1ac85c328ad980e00' },
                        { int: '172072' },
                    ],
                },
                value: { int: '48' },
            },
            expected: [{
                tokenId: new BigNumber('172072'),
                ownerAddress: 'KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn',
                amount: new BigNumber('48'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip12TransferProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%from_'] },
                    {
                        prim: 'list',
                        args: [
                            {
                                prim: 'pair',
                                args: [
                                    { prim: 'address', annots: ['%to_'] },
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'nat', annots: ['%token_id'] },
                                            { prim: 'nat', annots: ['%amount'] },
                                        ],
                                    },
                                ],
                            },
                        ],
                        annots: ['%txs'],
                    },
                ],
            }],
            annots: ['%transfer'],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { bytes: '0162d817497a95fe1e45d315d5a1ac85c328ad980e00' },
                    [
                        {
                            prim: 'Pair',
                            args: [
                                { bytes: '000009b4a5e8118ec9c88ac59a11988ee4e130ad20c1' },
                                {
                                    prim: 'Pair',
                                    args: [
                                        { int: '121154' },
                                        { int: '1' },
                                    ],
                                },
                            ],
                        },
                    ],
                ],
            }],
            expected: [{
                tokenId: new BigNumber('121154'),
                fromAddress: 'KT1HbQepzV1nVGg8QVznG7z4RcHseD5kwqBn',
                toAddress: 'tz1LXMBVbKdGTjZickA8zvcMYCPEQ1UvtjDe',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
    mint: {
        expectedProcessor: KalamintMintProcessor,
        schema: {
            prim: 'pair',
            args: [
                {
                    prim: 'pair',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'string', annots: ['%category'] },
                                { prim: 'nat', annots: ['%collection_id'] },
                                { prim: 'string', annots: ['%collection_name'] },
                            ],
                        },
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'string', annots: ['%creator_name'] },
                                { prim: 'nat', annots: ['%creator_royalty'] },
                            ],
                        },
                        { prim: 'nat', annots: ['%editions'] },
                        { prim: 'string', annots: ['%ipfs_hash'] },
                    ],
                },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'string', annots: ['%keywords'] },
                        { prim: 'string', annots: ['%name'] },
                        { prim: 'bool', annots: ['%on_sale'] },
                    ],
                },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'mutez', annots: ['%price'] },
                        { prim: 'string', annots: ['%symbol'] },
                    ],
                },
                { prim: 'nat', annots: ['%token_id'] },
                { prim: 'bytes', annots: ['%token_metadata_url'] },
            ],
            annots: ['%mint'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    {
                        prim: 'Pair',
                        args: [
                            {
                                prim: 'Pair',
                                args: [
                                    { string: 'category' },
                                    { int: '888' },
                                    { string: 'collection name' },
                                ],
                            },
                            {
                                prim: 'Pair',
                                args: [
                                    { string: 'creator name' },
                                    { int: '5' },
                                ],
                            },
                            { int: '4' },
                            { string: 'ipfs hash' },

                        ],
                    },
                    {
                        prim: 'Pair',
                        args: [
                            { string: 'keywords' },
                            { string: 'name' },
                            { string: 'true' },
                        ],
                    },
                    {
                        prim: 'Pair',
                        args: [
                            { int: '2000' },
                            { string: 'symb' },
                        ],
                    },
                    { int: '175824' },
                    { bytes: '000009b4a5e8118ec9c88ac59a11988ee4e130ad20c1' },
                ],
            },
            expected: [{
                tokenId: new BigNumber('175824'),
                toAddress: 'operation result parent source',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        },
    },
};
