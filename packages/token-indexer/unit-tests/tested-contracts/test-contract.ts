import { Michelson, MichelsonPrim } from '@tezos-dappetizer/indexer';
import { Constructor } from '@tezos-dappetizer/utils';

import { ContractType } from '../../src/indexers/contract-type-resolver';
import { RawBurn } from '../../src/indexers/entrypoints/burn/raw-burn';
import { RawMint } from '../../src/indexers/entrypoints/mint/raw-mint';
import { RawTransfer } from '../../src/indexers/entrypoints/transfer/transfer-processor-no-annots';
import { RawLedgerUpdate } from '../../src/indexers/ledger/ledger-processor';

export interface TestContract {
    readonly address: string;
    readonly name: string;
    readonly bigMapName: string;
    readonly contractType: ContractType | null;
    readonly ledger?: {
        readonly expectedProcessor: Constructor | undefined;
        readonly schema: MichelsonPrim;
        readonly sample?: {
            readonly input: {
                readonly key: Michelson;
                readonly value: Michelson;
            };
            readonly expected: RawLedgerUpdate[];
        };
    };
    readonly transfer?: TestEntrypoint<readonly RawTransfer[]>;
    readonly mint?: TestEntrypoint<RawMint[]>;
    readonly burn?: TestEntrypoint<RawBurn[]>;
}

export interface TestEntrypoint<TExpected> {
    readonly expectedProcessor: Constructor | undefined;
    readonly schema: MichelsonPrim;
    readonly sample?: {
        readonly input: Michelson;
        readonly expected: TExpected;
    };
}
