import { BigNumber } from 'bignumber.js';

import { TestContract } from './test-contract';
import { contractAddresses } from '../../src/helpers/contract-addresses';
import { SINGLE_ASSET_TOKEN_ID } from '../../src/helpers/token-constants';
import { ContractType } from '../../src/indexers/contract-type-resolver';
import { PaulBurnProcessor } from '../../src/indexers/entrypoints/burn/processors/incompatible/paul-burn-processor';
import { PaulMintProcessor } from '../../src/indexers/entrypoints/mint/processors/incompatible/paul-mint-processor';
import { Tzip7TransferProcessor } from '../../src/indexers/entrypoints/transfer/processors/tzip7-transfer-processor';

export const PAUL: TestContract = {
    address: contractAddresses.PAUL,
    name: 'PAUL',
    bigMapName: 'ledger',
    contractType: ContractType.FA1_2_like,
    mint: {
        expectedProcessor: PaulMintProcessor,
        schema: {
            prim: 'list',
            args: [{
                prim: 'pair',
                args: [
                    { prim: 'address' },
                    { prim: 'nat' },
                ],
            }],
        },
        sample: {
            input: [{
                prim: 'Pair',
                args: [
                    { string: 'tz1burnburnburnburnburnburnburjAYjjX' },
                    { int: '4000001' },
                ],
            }],
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                toAddress: 'tz1burnburnburnburnburnburnburjAYjjX',
                amount: new BigNumber('4000001'),
            }],
        },
    },
    burn: {
        expectedProcessor: PaulBurnProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address' },
                { prim: 'nat' },
            ],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { string: 'tz1burnburnburnburnburnburnburjAYjjX' },
                    { int: '100000000000000000000' },
                ],
            },
            expected: [{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                fromAddress: 'tz1burnburnburnburnburnburnburjAYjjX',
                amount: new BigNumber('100000000000000000000'),
            }],
        },
    },
    transfer: {
        expectedProcessor: Tzip7TransferProcessor,
        schema: {
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%from'] },
                {
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%to'] },
                        { prim: 'nat', annots: ['%value'] },
                    ],
                },
            ],
            annots: ['%transfer'],
        },
        sample: {
            input: {
                prim: 'Pair',
                args: [
                    { bytes: '00002a86d0d3e0bad8004a5dba65cd46d73e892c3eac' },
                    {
                        prim: 'Pair',
                        args: [
                            { bytes: '0173a0fbfd4afaa8ea2ed6a054055cdd8bd3b3928200' },
                            { int: '194484839489' },
                        ],
                    },
                ],
            },
            expected: [{
                fromAddress: 'tz1PWtaLXKiHXhXGvpuS8w4sVveNRKedTRSe',
                toAddress: 'KT1K8A8DLUTVuHaDBCZiG6AJdvKJbtH8dqmN',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: new BigNumber(194484839489),
            }],
        },
    },
};
