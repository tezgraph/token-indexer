import '../../../../../test-utilities/mocks/set-sqlite-db-types';

import { BigNumber } from 'bignumber.js';

import { describeMember } from '../../../../../test-utilities/mocks';
import { UpdateTokenMetadataCommand } from '../../../src/db-updaters/commands/update-token-metadata-command';
import { DbTokenKey } from '../../../src/entities/db-token';
import { MetadataState } from '../../../src/entities/metadata-state';
import { emptyTokenMetadata, TokenMetadata } from '../../../src/metadata/token/token-metadata';
import { getCmdTestHelper, TestDatabase } from '../../test-database';

describe(UpdateTokenMetadataCommand.name, () => {
    describeMember<UpdateTokenMetadataCommand>('description', () => {
        it('should contain class name and arguments', () => {
            const tokenKey: DbTokenKey = { contractAddress: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9', id: new BigNumber(123) };
            const metadataToSet: TokenMetadata = {
                metadataState: MetadataState.Ok,
                metadata: 'jsonShouldBeExcluded',
                name: 'USD',
                symbol: '$',
                decimals: 4,
            };

            const target = new UpdateTokenMetadataCommand(tokenKey, metadataToSet);

            expect(target.description).toIncludeMultiple([
                UpdateTokenMetadataCommand.name,
                `'${tokenKey.contractAddress}'`,
                `'${metadataToSet.name}'`,
            ]);
            expect(target.description).not.toInclude('jsonShouldBeExcluded');
        });
    });

    describeMember<UpdateTokenMetadataCommand>('execute', () => {
        let db: TestDatabase;

        beforeEach(async () => {
            db = await getCmdTestHelper();
        });

        afterEach(async () => {
            await db.destroy();
        });

        it('should set metadata of particular token', async () => {
            const contractAddr = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
            const unrelatedContractAddr = 'KT1QxLqukyfohPV5kPkw97Rs6cw1DDDvYgbB';
            const tokenId = new BigNumber(111);
            const unrelatedTokenId = new BigNumber(222);

            await db.insertContract({ addr: contractAddr });
            await db.insertContract({ addr: unrelatedContractAddr });
            await db.insertToken({ contractAddr, id: tokenId });
            await db.insertToken({ contractAddr, id: unrelatedTokenId });
            await db.insertToken({ contractAddr: unrelatedContractAddr, id: tokenId });
            await db.insertToken({ contractAddr: unrelatedContractAddr, id: unrelatedTokenId });

            const tokenKey: DbTokenKey = { contractAddress: contractAddr, id: tokenId };
            const metadataToSet: TokenMetadata = Object.freeze({
                metadataState: MetadataState.Failed,
                metadata: 'metadataJson',
                name: 'USD',
                symbol: '$',
                decimals: 4,
            });

            await db.executeCmd(new UpdateTokenMetadataCommand(tokenKey, metadataToSet));

            await db.verifyData('SELECT contract_address, decimals, id, metadata, metadata_state AS metadataState, name, symbol FROM token', [
                { contract_address: contractAddr, id: tokenId.toNumber(), ...metadataToSet },
                { contract_address: contractAddr, id: unrelatedTokenId.toNumber(), ...emptyTokenMetadata.Ok },
                { contract_address: unrelatedContractAddr, id: tokenId.toNumber(), ...emptyTokenMetadata.Ok },
                { contract_address: unrelatedContractAddr, id: unrelatedTokenId.toNumber(), ...emptyTokenMetadata.Ok },
            ]);
        });
    });
});
