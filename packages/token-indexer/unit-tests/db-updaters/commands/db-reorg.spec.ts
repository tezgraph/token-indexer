import '../../../../../test-utilities/mocks/set-sqlite-db-types';

import { BigNumber } from 'bignumber.js';

import { getCmdTestHelper, TestDatabase } from '../../test-database';

describe('DB reorg', () => {
    let db: TestDatabase;

    beforeEach(async () => {
        db = await getCmdTestHelper();
    });

    afterEach(async () => {
        await db.destroy();
    });

    it('should rollback all related entities on reorg when block is deleted', async () => {
        const contractAddr = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
        const tokenId = new BigNumber(111);
        const ownerAddr = 'tz1efdBSPksB8RykXwUFG9rb5qLPbfYcwrtN';

        const reorgedBlockHash = 'BKyrNsvS6Q8KDmmqpQWWuakSwh2HC9CachoYjMtM1W6QoWfvAcv';
        const reorgedContractAddr = 'KT1QxLqukyfohPV5kPkw97Rs6cw1DDDvYgbB';
        const reorgedTokenId = new BigNumber(222);

        await db.insertBlock({ hash: reorgedBlockHash });

        await db.insertContract({ addr: contractAddr });
        await db.insertToken({ contractAddr, id: tokenId });
        await db.insertBalance({ contractAddr, tokenId, ownerAddr, validUntilBlockHash: reorgedBlockHash });

        await db.insertContract({ addr: reorgedContractAddr, firstBlockHash: reorgedBlockHash });
        await db.insertToken({ contractAddr: reorgedContractAddr, id: reorgedTokenId, firstBlockHash: reorgedBlockHash });
        await db.insertBalance({ contractAddr: reorgedContractAddr, tokenId: reorgedTokenId, ownerAddr, validFromBlockHash: reorgedBlockHash });
        await db.insertBalance({ contractAddr, tokenId, ownerAddr, validFromBlockHash: reorgedBlockHash });

        // Simulate block deletion on reorg by Dappetizer.
        await db.dataSource.query(`DELETE FROM block WHERE hash = '${reorgedBlockHash}'`);

        await db.verifyData(
            'SELECT hash FROM block',
            [{ hash: db.firstBlockHash }],
        );
        await db.verifyData(
            'SELECT address FROM contract',
            [{ address: contractAddr }],
        );
        await db.verifyData(
            'SELECT contract_address, id FROM token',
            [{ contract_address: contractAddr, id: tokenId.toNumber() }],
        );
        await db.verifyData(
            'SELECT token_contract_address, token_id, valid_until_block_hash FROM balance',
            [{ token_contract_address: contractAddr, token_id: tokenId.toNumber(), valid_until_block_hash: null }],
        );
    });
});
