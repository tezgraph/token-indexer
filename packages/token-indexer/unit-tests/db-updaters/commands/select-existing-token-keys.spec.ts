import '../../../../../test-utilities/mocks/set-sqlite-db-types';

import { BigNumber } from 'bignumber.js';

import { describeMember } from '../../../../../test-utilities/mocks';
import { SelectExistingTokenKeys } from '../../../src/db-updaters/commands/select-existing-token-keys';
import { DbTokenKey } from '../../../src/entities/db-token';
import { getCmdTestHelper, TestDatabase } from '../../test-database';

describe(SelectExistingTokenKeys.name, () => {
    describeMember<SelectExistingTokenKeys>('description', () => {
        it('should contain class name and arguments', () => {
            const key: DbTokenKey = { contractAddress: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9', id: new BigNumber(11) };

            const target = new SelectExistingTokenKeys([key]);

            expect(target.description).toIncludeMultiple([
                SelectExistingTokenKeys.name,
                `'${key.contractAddress}'`,
            ]);
        });
    });

    describeMember<SelectExistingTokenKeys>('execute', () => {
        let db: TestDatabase;

        beforeEach(async () => {
            db = await getCmdTestHelper();
        });

        afterEach(async () => {
            await db.destroy();
        });

        it('should return given keys that exist in db', async () => {
            const contractAddr1 = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
            const contractAddr2 = 'KT1QxLqukyfohPV5kPkw97Rs6cw1DDDvYgbB';
            const tokenId1 = new BigNumber(11);
            const tokenId2 = new BigNumber(22);

            await db.insertContract({ addr: contractAddr1 });
            await db.insertContract({ addr: contractAddr2 });
            await db.insertToken({ contractAddr: contractAddr1, id: tokenId1 });
            await db.insertToken({ contractAddr: contractAddr1, id: tokenId2 });
            await db.insertToken({ contractAddr: contractAddr2, id: tokenId1 });
            await db.insertToken({ contractAddr: contractAddr2, id: tokenId2 });

            const keysToCheck: DbTokenKey[] = [
                { contractAddress: contractAddr1, id: tokenId1 },
                { contractAddress: contractAddr1, id: new BigNumber(33) },
                { contractAddress: contractAddr2, id: tokenId2 },
                { contractAddress: 'KT1TxqZ8QtKvLu3V3JH7Gx58n7Co8pgtpQU5', id: tokenId2 },
            ];

            const existingKeys = await db.executeCmd(new SelectExistingTokenKeys(keysToCheck));

            expect(existingKeys).toEqual([keysToCheck[0], keysToCheck[2]]);
        });
    });
});
