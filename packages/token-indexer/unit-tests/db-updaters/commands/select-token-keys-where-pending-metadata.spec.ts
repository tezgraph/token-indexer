import '../../../../../test-utilities/mocks/set-sqlite-db-types';

import { BigNumber } from 'bignumber.js';

import { describeMember } from '../../../../../test-utilities/mocks';
import {
    SelectTokenKeysWherePendingMetadata,
} from '../../../src/db-updaters/commands/select-token-keys-where-pending-metadata';
import { DbTokenKey } from '../../../src/entities/db-token';
import { MetadataState } from '../../../src/entities/metadata-state';
import { getCmdTestHelper, TestDatabase } from '../../test-database';

describe(SelectTokenKeysWherePendingMetadata.name, () => {
    describeMember<SelectTokenKeysWherePendingMetadata>('description', () => {
        it('should be class name', () => {
            const target = new SelectTokenKeysWherePendingMetadata();

            expect(target.description).toBe(SelectTokenKeysWherePendingMetadata.name);
        });
    });

    describeMember<SelectTokenKeysWherePendingMetadata>('execute', () => {
        let db: TestDatabase;

        beforeEach(async () => {
            db = await getCmdTestHelper();
        });

        afterEach(async () => {
            await db.destroy();
        });

        it('should select tokens with metadata pending metadata', async () => {
            const contractAddr = 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9';
            const tokenId1 = new BigNumber(11);
            const tokenId2 = new BigNumber(22);
            const tokenId3 = new BigNumber(33);
            const tokenId4 = new BigNumber(44);

            await db.insertContract({ addr: contractAddr });
            await db.insertToken({ contractAddr, id: tokenId1, metadata: MetadataState.Ok });
            await db.insertToken({ contractAddr, id: tokenId2, metadata: MetadataState.Pending });
            await db.insertToken({ contractAddr, id: tokenId3, metadata: MetadataState.Failed });
            await db.insertToken({ contractAddr, id: tokenId4, metadata: MetadataState.Pending });

            const tokenKeys = await db.executeCmd(new SelectTokenKeysWherePendingMetadata());

            expect(tokenKeys).toEqual<DbTokenKey[]>([
                { contractAddress: contractAddr, id: tokenId2 },
                { contractAddress: contractAddr, id: tokenId4 },
            ]);
        });
    });
});
