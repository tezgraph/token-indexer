import { DbDataSourceProvider, DbExecutor } from '@tezos-dappetizer/database';
import { Writable } from 'ts-essentials';
import { anyOfClass, anything, instance, mock, verify, when } from 'ts-mockito';
import { DataSource } from 'typeorm';

import { describeMember, TestLogger } from '../../../../../test-utilities/mocks';
import { TokenIndexerInternalConfig } from '../../../src/config/token-indexer-internal-config';
import {
    SelectTokenKeysWherePendingMetadata,
} from '../../../src/db-updaters/commands/select-token-keys-where-pending-metadata';
import { DbTokenKey } from '../../../src/entities/db-token';
import { TokenMetadataBackgroundDispatcher } from '../../../src/metadata/token/token-metadata-background-dispatcher';
import { TokenMetadataBackgroundWorker } from '../../../src/metadata/token/token-metadata-background-worker';

describe(TokenMetadataBackgroundWorker.name, () => {
    let target: TokenMetadataBackgroundWorker;
    let dispatcher: TokenMetadataBackgroundDispatcher;
    let dbExecutor: DbExecutor;
    let dbDataSourceProvider: DbDataSourceProvider;
    let logger: TestLogger;
    let config: Writable<TokenIndexerInternalConfig>;

    beforeEach(() => {
        [dispatcher, dbExecutor, dbDataSourceProvider] = [mock(), mock(), mock()];
        logger = new TestLogger();
        config = { skipGetMetadata: false } as typeof config;
        target = new TokenMetadataBackgroundWorker(instance(dispatcher), instance(dbExecutor), instance(dbDataSourceProvider), logger, config);
    });

    describeMember<typeof target>('name', () => {
        it('should be class name', () => {
            expect(target.name).toBe(TokenMetadataBackgroundWorker.name);
        });
    });

    describeMember<typeof target>('start', () => {
        it('should select tokens pending metadata and trigger update', async () => {
            const dbDataSource = { manager: {} } as DataSource;
            const tokenKey1 = {} as DbTokenKey;
            const tokenKey2 = {} as DbTokenKey;
            when(dbDataSourceProvider.getDataSource()).thenResolve(dbDataSource);
            when(dbExecutor.execute(dbDataSource.manager, anyOfClass(SelectTokenKeysWherePendingMetadata))).thenResolve([tokenKey1, tokenKey2]);

            await target.start();

            verify(dispatcher.updateOnBackground(anything())).times(2);
            verify(dispatcher.updateOnBackground(tokenKey1)).once();
            verify(dispatcher.updateOnBackground(tokenKey2)).once();
        });

        it('should not trigger updating if metadata is disabled in config', async () => {
            config.skipGetMetadata = true;

            await target.start();

            verify(dbDataSourceProvider.getDataSource()).never();
        });
    });

    describeMember<typeof target>('onToBlockLevelReached', () => {
        it('should wait for all remaining requests', async () => {
            when(dispatcher.remainingCount).thenReturn(66);

            await target.onToBlockLevelReached();

            logger.loggedSingle().verify('Information', { remainingCount: 66 });
            verify(dispatcher.completeAllRemaining()).once();
        });
    });
});
