import { DbCommand, DbDataSourceProvider, DbExecutor } from '@tezos-dappetizer/database';
import { Contract, ContractProvider } from '@tezos-dappetizer/indexer';
import { BigNumber } from 'bignumber.js';
import { anything, capture, instance, mock, verify, when } from 'ts-mockito';
import { DataSource, EntityManager } from 'typeorm';

import { TestLogger } from '../../../../../test-utilities/mocks';
import { UpdateTokenMetadataCommand } from '../../../src/db-updaters/commands/update-token-metadata-command';
import { DbTokenKey } from '../../../src/entities/db-token';
import { TokenMetadata } from '../../../src/metadata/token/token-metadata';
import { TokenMetadataBackgroundUpdater } from '../../../src/metadata/token/token-metadata-background-updater';
import { TokenMetadataProvider } from '../../../src/metadata/token/token-metadata-provider';

describe(TokenMetadataBackgroundUpdater.name, () => {
    let target: TokenMetadataBackgroundUpdater;
    let contractProvider: ContractProvider;
    let tokenMetadataProvider: TokenMetadataProvider;
    let dbExecutor: DbExecutor;
    let dbDataSourceProvider: DbDataSourceProvider;
    let logger: TestLogger;

    const act = async () => target.update(tokenKey);

    let tokenKey: DbTokenKey;
    let contract: Contract;
    let dbDataSource: DataSource;
    let metadata: TokenMetadata;

    beforeEach(() => {
        contractProvider = mock<ContractProvider>();
        tokenMetadataProvider = mock<TokenMetadataProvider>();
        dbExecutor = mock<DbExecutor>();
        dbDataSourceProvider = mock<DbDataSourceProvider>();
        logger = new TestLogger();
        target = new TokenMetadataBackgroundUpdater(
            instance(contractProvider),
            instance(tokenMetadataProvider),
            instance(dbExecutor),
            instance(dbDataSourceProvider),
            logger,
        );

        tokenKey = Object.freeze({
            contractAddress: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9',
            id: new BigNumber(111),
        });
        contract = { abstraction: {} } as Contract;
        dbDataSource = { manager: {} } as DataSource;
        metadata = {} as TokenMetadata;

        when(contractProvider.getContract(tokenKey.contractAddress)).thenResolve(contract);
        when(tokenMetadataProvider.get(contract.abstraction, tokenKey.id)).thenResolve(metadata);
        when(dbDataSourceProvider.getDataSource()).thenResolve(dbDataSource);
    });

    it('should download token metadata and writen them to db', async () => {
        when(dbExecutor.execute(anything(), anything())).thenCall(async () => {
            logger.logDebug('should put token key to SCOPE');
            return Promise.resolve();
        });

        await act();

        verify(dbExecutor.execute(dbDataSource.manager, anything())).once();
        const dbCommand = capture<EntityManager, DbCommand>(dbExecutor.execute).first()[1] as UpdateTokenMetadataCommand;
        expect(dbCommand).toBeInstanceOf(UpdateTokenMetadataCommand);
        expect(dbCommand.tokenKey).toEqual(tokenKey);
        expect(dbCommand.metadataToSet).toBe(metadata);

        logger.verifyLoggedCount(3);
        logger.logged(0).verify('Debug', { updatedToken: tokenKey }).verifyMessage('Updating');
        logger.logged(1).verify('Debug', { updatedToken: tokenKey }).verifyMessage('SCOPE');
        logger.logged(2).verify('Debug', { updatedToken: tokenKey }).verifyMessage('Updated');
    });

    it('should handle errors', async () => {
        const error = new Error('oups');
        when(dbExecutor.execute(anything(), anything())).thenReject(error);

        await act();

        logger.verifyLoggedCount(2);
        logger.logged(0).verify('Debug', { updatedToken: tokenKey }).verifyMessage('Updating');
        logger.logged(1).verify('Error', { updatedToken: tokenKey, error });
    });
});
