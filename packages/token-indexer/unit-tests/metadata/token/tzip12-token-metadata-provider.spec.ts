import * as taquitoTzip12 from '@taquito/tzip12';
import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import BigNumber from 'bignumber.js';
import { anything, instance, mock, when } from 'ts-mockito';

import { TestLogger } from '../../../../../test-utilities/mocks';
import { MetadataState } from '../../../src/entities/metadata-state';
import { MetadataSanitizer } from '../../../src/metadata/helpers/metadata-sanitizer';
import { emptyTokenMetadata, TokenMetadata } from '../../../src/metadata/token/token-metadata';
import { Tzip12TokenMetadataProvider } from '../../../src/metadata/token/tzip12-token-metadata-provider';

describe(Tzip12TokenMetadataProvider.name, () => {
    let target: Tzip12TokenMetadataProvider;
    let sanitizer: MetadataSanitizer;
    let logger: TestLogger;

    let contract: ContractAbstraction;
    let tokenId: BigNumber;
    let tzip12: taquitoTzip12.Tzip12ContractAbstraction;
    let rawMetadata: taquitoTzip12.TokenMetadata;

    const act = async () => target.get(instance(contract), tokenId);

    beforeEach(() => {
        sanitizer = mock();
        logger = new TestLogger();
        target = new Tzip12TokenMetadataProvider(instance(sanitizer), logger);

        [contract, tzip12] = [mock(), mock()];
        tokenId = new BigNumber(123);
        rawMetadata = {
            decimals: 222,
            token_id: 0,
            name: 'Insane Memecoin',
            symbol: 'Insane LOL',
        };

        when(contract.address).thenReturn('mockedAddr');
        when(contract.tzip12()).thenReturn(instance(tzip12));
        when(tzip12.getTokenMetadata(123)).thenResolve(rawMetadata);
        when(sanitizer.sanitizeInteger(222)).thenReturn(333);
        when(sanitizer.sanitizeString('Insane Memecoin')).thenReturn('Memecoin');
        when(sanitizer.sanitizeString('Insane LOL')).thenReturn('LOL');
    });

    it('should get metadata correctly', async () => {
        const metadata = await act();

        expect(metadata).toEqual<TokenMetadata>({
            metadataState: MetadataState.Ok,
            decimals: 333,
            metadata: JSON.stringify(rawMetadata),
            name: 'Memecoin',
            symbol: 'LOL',
        });
        logger.verifyNothingLogged();
    });

    it('should execute in performance section', async () => {
        when(contract.tzip12()).thenCall(() => {
            logger.logDebug('TEST');
            return instance(tzip12) as any;
        });

        await act();

        logger.loggedSingle().verify('Debug', { performanceSection: logger.category });
    });

    it.each([
        ['TokenMetadataNotFound', new taquitoTzip12.TokenMetadataNotFound('')],
        ['TokenIdNotFound', new taquitoTzip12.TokenIdNotFound(123)],
    ])('should return ok empty metadata if %s error', async (_desc, error) => {
        when(tzip12.getTokenMetadata(anything())).thenReject(error);

        const metadata = await act();

        expect(metadata).toBe(emptyTokenMetadata.Ok);
        logger.loggedSingle().verify('Debug', { contract: 'mockedAddr', token: tokenId }).verifyMessage('not-found');
    });

    it('should return failed empty metadata if token id is greater than max supported integer', async () => {
        tokenId = new BigNumber('50071954740991666');

        await runFailedTest(['max safe integer', '9007199254740991', '@taquito']);
    });

    it('should return failed empty metadata if tzip error', async () => {
        when(contract.tzip12()).thenThrow(new Error('tzip err'));

        await runFailedTest(['tzip err']);
    });

    it('should return failed empty metadata if download error', async () => {
        when(tzip12.getTokenMetadata(anything())).thenReject(new Error('download err'));

        await runFailedTest(['download err']);
    });

    it('should return failed empty metadata if deserialization failed', async () => {
        when(sanitizer.sanitizeString(anything())).thenThrow(new Error('oups'));

        await runFailedTest(['oups', JSON.stringify(rawMetadata)]);
    });

    async function runFailedTest(expectedError: string[]) {
        const metadata = await act();

        expect(metadata).toBe(emptyTokenMetadata.Failed);
        const loggedError = logger.loggedSingle()
            .verify('Error', { contract: 'mockedAddr', token: tokenId, error: expect.any(Error) })
            .data.error;
        expect(loggedError.message).toIncludeMultiple(expectedError);
    }
});
