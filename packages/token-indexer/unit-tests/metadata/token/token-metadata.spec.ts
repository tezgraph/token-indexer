import { MetadataState } from '../../../src/entities/metadata-state';
import { emptyTokenMetadata, TokenMetadata } from '../../../src/metadata/token/token-metadata';

describe('TokenMetadata', () => {
    it('should provide correct empty metadata', () => {
        expect(emptyTokenMetadata).toEqual<Record<MetadataState, TokenMetadata>>({
            Failed: {
                metadataState: MetadataState.Failed,
                decimals: null,
                metadata: null,
                name: null,
                symbol: null,
            },
            Ok: {
                metadataState: MetadataState.Ok,
                decimals: null,
                metadata: null,
                name: null,
                symbol: null,
            },
            Pending: {
                metadataState: MetadataState.Pending,
                decimals: null,
                metadata: null,
                name: null,
                symbol: null,
            },
        });
    });
});
