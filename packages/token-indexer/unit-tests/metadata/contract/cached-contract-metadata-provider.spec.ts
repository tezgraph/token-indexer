import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { times } from 'lodash';
import { anything, instance, mock, verify, when } from 'ts-mockito';

import { sleep } from '../../../../../test-utilities/mocks';
import { TokenIndexerInternalConfig } from '../../../src/config/token-indexer-internal-config';
import { CachedContractMetadataProvider } from '../../../src/metadata/contract/cached-contract-metadata-provider';
import { ContractMetadata } from '../../../src/metadata/contract/contract-metadata';
import { ContractMetadataProvider } from '../../../src/metadata/contract/contract-metadata-provider';

describe(CachedContractMetadataProvider.name, () => {
    let target: CachedContractMetadataProvider;
    let nextProvider: ContractMetadataProvider;

    let contractX: ContractAbstraction;
    let contractY: ContractAbstraction;
    let metadataX: ContractMetadata;
    let metadataY: ContractMetadata;

    beforeEach(() => {
        nextProvider = mock();
        target = new CachedContractMetadataProvider(
            instance(nextProvider),
            { contractMetadataCacheMillis: 100 } as TokenIndexerInternalConfig,
        );

        contractX = { address: 'KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9' } as typeof contractX;
        contractY = { address: 'KT19kgnqC5VWoxktLRdRUERbyUPku9YioE8W' } as typeof contractY;
        metadataX = 'mockedMetadataX' as any;
        metadataY = 'mockedMetadataY' as any;
        when(nextProvider.get(contractX)).thenResolve(metadataX);
        when(nextProvider.get(contractY)).thenResolve(metadataY);
    });

    it('should cache created contract', async () => {
        await Promise.all(times(5, async () => {
            expect(await target.get(contractX)).toBe(metadataX);
        }));
        verify(nextProvider.get(anything())).once();
    });

    it('should cache on promise level', () => {
        const promise1 = target.get(contractX);
        const promise2 = target.get(contractX);

        expect(promise1).toBe(promise2);
    });

    it('should cache contracts by address', async () => {
        expect(await target.get(contractX)).toBe(metadataX);
        expect(await target.get(contractY)).toBe(metadataY);
        expect(await target.get(contractX)).toBe(metadataX);

        verify(nextProvider.get(anything())).times(2);
    });

    it('should expire after configured time', async () => {
        expect(await target.get(contractX)).toBe(metadataX);

        await sleep(150);

        expect(await target.get(contractX)).toBe(metadataX);
        verify(nextProvider.get(anything())).times(2);
    });

    it('should slide expiration', async () => {
        expect(await target.get(contractX)).toBe(metadataX);

        await sleep(70);
        expect(await target.get(contractX)).toBe(metadataX);
        await sleep(70);

        expect(await target.get(contractX)).toBe(metadataX);
        verify(nextProvider.get(anything())).once();
    });
});
