import * as taquitoTzip16 from '@taquito/tzip16';
import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { anything, instance, mock, when } from 'ts-mockito';

import { TestLogger } from '../../../../../test-utilities/mocks';
import { MetadataState } from '../../../src/entities/metadata-state';
import { ContractMetadata, emptyContractMetadata } from '../../../src/metadata/contract/contract-metadata';
import { Tzip16ContractMetadataProvider } from '../../../src/metadata/contract/tzip16-contract-metadata-provider';
import { MetadataSanitizer } from '../../../src/metadata/helpers/metadata-sanitizer';

describe(Tzip16ContractMetadataProvider.name, () => {
    let target: Tzip16ContractMetadataProvider;
    let sanitizer: MetadataSanitizer;
    let logger: TestLogger;

    let contract: ContractAbstraction;
    let tzip16: taquitoTzip16.Tzip16ContractAbstraction;
    let rawEnvelope: taquitoTzip16.MetadataEnvelope;

    const act = async () => target.get(instance(contract));

    beforeEach(() => {
        sanitizer = mock();
        logger = new TestLogger();
        target = new Tzip16ContractMetadataProvider(instance(sanitizer), logger);

        [contract, tzip16] = [mock(), mock()];
        rawEnvelope = {
            metadata: {
                name: 'Insane Foo',
                description: 'Insane Foo bar.',
            },
            uri: 'ipfs://token/metadata',
        } as typeof rawEnvelope;

        when(contract.address).thenReturn('mockedAddr');
        when(contract.tzip16()).thenReturn(instance(tzip16));
        when(tzip16.getMetadata()).thenResolve(rawEnvelope);
        when(sanitizer.sanitizeString('Insane Foo')).thenReturn('Foo');
        when(sanitizer.sanitizeString('Insane Foo bar.')).thenReturn('Foo bar.');
    });

    it('should get metadata correctly', async () => {
        const metadata = await act();

        expect(metadata).toEqual<ContractMetadata>({
            metadataState: MetadataState.Ok,
            name: 'Foo',
            description: 'Foo bar.',
        });
        logger.verifyNothingLogged();
    });

    it('should execute in performance section', async () => {
        when(contract.tzip16()).thenCall(() => {
            logger.logDebug('TEST');
            return instance(tzip16) as any;
        });

        await act();

        logger.loggedSingle().verify('Debug', { performanceSection: logger.category });
    });

    it.each([
        ['BigMapMetadataNotFound', new taquitoTzip16.BigMapContractMetadataNotFoundError('123')],
        ['MetadataNotFound', new taquitoTzip16.ContractMetadataNotFoundError('')],
        ['UriNotFound', new taquitoTzip16.UriNotFoundError()],
    ])('should return ok empty metadata if %s error', async (_desc, error) => {
        when(tzip16.getMetadata()).thenReject(error);

        const metadata = await act();

        expect(metadata).toBe(emptyContractMetadata.Ok);
        logger.loggedSingle().verify('Debug', { contract: 'mockedAddr' }).verifyMessage('not-found');
    });

    it('should return failed empty metadata if tzip error', async () => {
        when(contract.tzip16()).thenThrow(new Error('tzip err'));

        await runFailedTest(['tzip err']);
    });

    it('should return failed empty metadata if download error', async () => {
        when(tzip16.getMetadata()).thenReject(new Error('download err'));

        await runFailedTest(['download err']);
    });

    it('should return failed empty metadata if deserialization failed', async () => {
        when(sanitizer.sanitizeString(anything())).thenThrow(new Error('oups'));

        await runFailedTest(['oups', 'ipfs://token/metadata', JSON.stringify(rawEnvelope.metadata)]);
    });

    async function runFailedTest(expectedError: string[]) {
        const metadata = await act();

        expect(metadata).toBe(emptyContractMetadata.Failed);
        const loggedError = logger.loggedSingle()
            .verify('Error', { contract: 'mockedAddr', error: expect.any(Error) })
            .data.error;
        expect(loggedError.message).toIncludeMultiple(expectedError);
    }
});
