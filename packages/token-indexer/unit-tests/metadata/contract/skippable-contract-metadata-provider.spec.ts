import { ContractAbstraction } from '@tezos-dappetizer/indexer';
import { Writable } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { TokenIndexerInternalConfig } from '../../../src/config/token-indexer-internal-config';
import { ContractMetadata, emptyContractMetadata } from '../../../src/metadata/contract/contract-metadata';
import { ContractMetadataProvider } from '../../../src/metadata/contract/contract-metadata-provider';
import { SkippableContractMetadataProvider } from '../../../src/metadata/contract/skippable-contract-metadata-provider';

describe(SkippableContractMetadataProvider.name, () => {
    let target: SkippableContractMetadataProvider;
    let nextProvider: ContractMetadataProvider;
    let config: Writable<TokenIndexerInternalConfig>;

    let contract: ContractAbstraction;
    let nextMetadata: ContractMetadata;

    beforeEach(() => {
        nextProvider = mock();
        config = { skipGetMetadata: false } as typeof config;
        target = new SkippableContractMetadataProvider(instance(nextProvider), config);

        contract = 'mockedContract' as any;
        nextMetadata = 'mockedMetadata' as any;
        when(nextProvider.get(contract)).thenResolve(nextMetadata);
    });

    it('should get empty metadata from next provider if skip enabled in config', async () => {
        config.skipGetMetadata = true;

        const metadata = await target.get(contract);

        expect(metadata).toBe(emptyContractMetadata.Ok);
    });

    it('should get metadata from next provider if skip disabled in config', async () => {
        const metadata = await target.get(contract);

        expect(metadata).toBe(nextMetadata);
    });
});
