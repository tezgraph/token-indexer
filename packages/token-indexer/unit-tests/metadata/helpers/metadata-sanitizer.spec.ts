import { describeMember, expectToThrow } from '../../../../../test-utilities/mocks';
import { MetadataSanitizer } from '../../../src/metadata/helpers/metadata-sanitizer';

describe(MetadataSanitizer.name, () => {
    const target = new MetadataSanitizer();

    describeMember<typeof target>('sanitizeString', () => {
        it.each([
            ['regular string', 'foo bar', 'foo bar'],
            ['undefined', undefined, null],
            ['null', null, null],
            ['empty string', '', ''],
            ['white-space string', '  ', '  '],
            ['non-printable chars', 'wt\u0001f o\u0005mg', 'wtf omg'],
        ])('should handle %s correctly', (_desc, input, expected) => {
            const str = target.sanitizeString(input);

            expect(str).toBe(expected);
        });

        it('should throw if not string', () => {
            expectToThrow(() => target.sanitizeString(123 as any));
        });
    });

    describeMember<typeof target>('sanitizeInteger', () => {
        it.each([
            ['regular number', 123, 123],
            ['undefined', undefined, null],
            ['null', null, null],
            ['empty string', '', null],
            ['white-space string', '  ', null],
            ['string with number', '123', 123],
        ])('should handle %s correctly', (_desc, input: any, expected) => {
            const str = target.sanitizeInteger(input);

            expect(str).toBe(expected);
        });

        it.each([
            ['NaN', NaN],
            ['string without number', 'wtf'],
        ])('should throw if %s', (_desc, input: any) => {
            expectToThrow(() => target.sanitizeInteger(input));
        });
    });
});
