import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';

import { TestClock } from '../../../../../test-utilities/mocks';
import { MetadataState } from '../../../src/entities/metadata-state';
import { TokenIndexerMetrics } from '../../../src/helpers/token-indexer-metrics';
import { MetricsReportingMetadataProvider } from '../../../src/metadata/helpers/metrics-reporting-metadata-provider';
import { TzipMetadata, TzipMetadataProvider } from '../../../src/metadata/helpers/tzip-metadata-provider';

describe(MetricsReportingMetadataProvider.name, () => {
    let target: MetricsReportingMetadataProvider<TzipMetadata, [string, string]>;
    let nextProvider: TzipMetadataProvider<TzipMetadata, [string, string]>;
    let getMetadataCallDuration: TokenIndexerMetrics['getMetadataCallDuration'];
    let getMetadataOkCount: TokenIndexerMetrics['getMetadataOkCount'];
    let getMetadataFailedCount: TokenIndexerMetrics['getMetadataFailedCount'];
    let clock: TestClock;

    beforeEach(() => {
        [nextProvider, getMetadataCallDuration, getMetadataOkCount, getMetadataFailedCount] = [mock(), mock(), mock(), mock()];
        const metrics = {
            getMetadataCallDuration: instance(getMetadataCallDuration),
            getMetadataOkCount: instance(getMetadataOkCount),
            getMetadataFailedCount: instance(getMetadataFailedCount),
        } as TokenIndexerMetrics;
        clock = new TestClock();
        target = new MetricsReportingMetadataProvider(instance(nextProvider), metrics, clock, { feature: 'foo' });
    });

    it(`should collect metrics correctly if ${MetadataState.Ok} metadata`, async () => {
        await runTest(MetadataState.Ok);

        verify(getMetadataOkCount.inc(deepEqual({ feature: 'foo' }))).once();
        verify(getMetadataFailedCount.inc(anything())).never();
    });

    it(`should collect metrics correctly if ${MetadataState.Failed} metadata`, async () => {
        await runTest(MetadataState.Failed);

        verify(getMetadataFailedCount.inc(deepEqual({ feature: 'foo' }))).once();
        verify(getMetadataOkCount.inc(anything())).never();
    });

    async function runTest(metadataState: MetadataState) {
        const nextMetadata = { metadataState, value: 123 } as TzipMetadata;
        when(nextProvider.get('a1', 'a2')).thenCall(async () => {
            clock.tick(666);
            return Promise.resolve(nextMetadata);
        });

        const metadata = await target.get('a1', 'a2');

        expect(metadata).toBe(nextMetadata);
        verify(getMetadataCallDuration.observe(deepEqual({ feature: 'foo' }), 666)).once();
    }
});
