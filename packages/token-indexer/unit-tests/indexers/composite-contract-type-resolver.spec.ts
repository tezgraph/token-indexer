import { BigMapInfo, Contract, MichelsonSchema } from '@tezos-dappetizer/indexer';
import { asReadonly } from '@tezos-dappetizer/utils';

import { CompositeContractTypeResolver } from '../../src/indexers/composite-contract-type-resolver';
import { testContracts } from '../tested-contracts';

describe(CompositeContractTypeResolver.name, () => {
    const target = new CompositeContractTypeResolver();

    for (const testContract of testContracts) {
        it(`should resolve contract type of ${testContract.name} correctly`, () => {
            const contract = {
                address: testContract.address,
                bigMaps: asReadonly(testContract.ledger
                    ? [{
                        name: testContract.bigMapName,
                        keySchema: new MichelsonSchema(testContract.ledger.schema.args![0]!),
                        valueSchema: new MichelsonSchema(testContract.ledger.schema.args![1]!),
                    } as BigMapInfo]
                    : []),
                parameterSchemas: {
                    entrypoints: {
                        ...testContract.burn ? { transfer: new MichelsonSchema(testContract.burn.schema) } : {},
                        ...testContract.mint ? { transfer: new MichelsonSchema(testContract.mint.schema) } : {},
                        ...testContract.transfer ? { transfer: new MichelsonSchema(testContract.transfer.schema) } : {},
                    },
                },
            } as Contract;

            const contractType = target.resolveContractType(contract);

            expect(contractType).toBe(testContract.contractType);
        });
    }
});
