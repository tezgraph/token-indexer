import { BigMapInfo, MichelsonSchema } from '@tezos-dappetizer/indexer';
import BigNumber from 'bignumber.js';
import { toArray } from 'iter-tools';

import { describeMember } from '../../../../../../test-utilities/mocks';
import { prepareContract, prepareUnknownBigMap } from '../../../../../../test-utilities/mocks/utils';
import { SINGLE_ASSET_TOKEN_ID } from '../../../../src/helpers/token-constants';
import { ContractType } from '../../../../src/indexers/contract-type-resolver';
import { RawLedgerUpdate } from '../../../../src/indexers/ledger/ledger-processor';
import { LedgerValue, Tzip7LedgerProcessor } from '../../../../src/indexers/ledger/processors/tzip7-ledger-processor';
import { TokenBigMapUpdateIndexingContext } from '../../../../src/token-indexing-data';

describe(Tzip7LedgerProcessor.name, () => {
    const target = new Tzip7LedgerProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        it('should process when schema matches', () => {
            const bigMap = prepareValidBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');

            expect(currentShouldProcess).toBeTrue();
        });

        it('should not process when big map name does not match', () => {
            const bigMap = prepareUnknownBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');

            expect(currentShouldProcess).toBeFalse();
        });

        it('should not process when key schema does not match', () => {
            const bigMap = prepareInvalidKeySchemaBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');

            expect(currentShouldProcess).toBeFalse();
        });

        it('should not process when value schema does not match', () => {
            const bigMap = prepareInvalidValueSchemaBigMap();

            const currentShouldProcess = target.shouldProcess(bigMap, '');

            expect(currentShouldProcess).toBeFalse();
        });
    });

    describeMember<typeof target>('deserializeRawUpdatesFromNoAnnots', () => {
        it('should deserialize raw changes', () => {
            const key = 'th1';
            const value = {
                0: new BigNumber(34),
            } as LedgerValue;
            const context = {} as TokenBigMapUpdateIndexingContext;

            const updates = toArray(target.deserializeRawUpdatesFromNoAnnots(key, value, context));

            expect(updates).toEqual<RawLedgerUpdate[]>([{
                tokenId: SINGLE_ASSET_TOKEN_ID,
                ownerAddress: key,
                amount: value[0],
            }]);
        });
    });

    describeMember<typeof target>('resolveContractType', () => {
        it('should resolve contract type', () => {
            const contract = prepareContract([
                prepareValidBigMap(),
                prepareUnknownBigMap(),
            ]);

            expect(target.resolveContractType(contract)).toBe(ContractType.FA1_2_like);
        });

        it('should not resolve contract type - unknown big map name', () => {
            const contract = prepareContract([
                prepareUnknownBigMap(),
                prepareUnknownBigMap(),
            ]);

            expect(target.resolveContractType(contract)).toBeNull();
        });

        it('should not resolve contract type - incompatible schema', () => {
            const contract = prepareContract([
                prepareInvalidKeySchemaBigMap(),
                prepareInvalidValueSchemaBigMap(),
                prepareUnknownBigMap(),
            ]);

            expect(target.resolveContractType(contract)).toBeNull();
        });
    });
});

function prepareValidBigMap(): BigMapInfo {
    return {
        name: 'ledger',
        keySchema: new MichelsonSchema({
            prim: 'address', annots: ['%owner'],
        }),
        valueSchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'nat', annots: ['%amount'] },
                {
                    prim: 'map',
                    args: [
                        { prim: 'address', annots: ['%a'] },
                        { prim: 'nat', annots: ['%b'] },
                    ],
                    annots: ['%allowances'],
                },
            ],
        }),
    } as BigMapInfo;
}

function prepareInvalidKeySchemaBigMap(): BigMapInfo {
    return {
        name: 'ledger',
        keySchema: new MichelsonSchema({
            prim: 'nat', annots: ['%something'],
        }),
        valueSchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'nat', annots: ['%amount'] },
                {
                    prim: 'map',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                    annots: ['%allowances'],
                },
            ],
        }),
    } as BigMapInfo;
}

function prepareInvalidValueSchemaBigMap(): BigMapInfo {
    return {
        name: 'ledger',
        keySchema: new MichelsonSchema({
            prim: 'address', annots: ['%owner'],
        }),
        valueSchema: new MichelsonSchema({
            prim: 'pair',
            args: [
                { prim: 'address', annots: ['%something'] },
                {
                    prim: 'map',
                    args: [
                        { prim: 'address' },
                        { prim: 'nat' },
                    ],
                    annots: ['%allowances'],
                },
            ],
        }),
    } as BigMapInfo;
}
