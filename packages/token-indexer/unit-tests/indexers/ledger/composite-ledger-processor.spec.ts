import { BigMapInfo, LazyMichelsonValue, MichelsonSchema } from '@tezos-dappetizer/indexer';
import { MarkRequired } from 'ts-essentials';

import { CompositeLedgerProcessor } from '../../../src/indexers/ledger/composite-ledger-processor';
import { RawLedgerUpdate } from '../../../src/indexers/ledger/ledger-processor';
import { TokenBigMapUpdateIndexingContext } from '../../../src/token-indexing-data';
import { testContracts } from '../../tested-contracts';
import { TestContract } from '../../tested-contracts/test-contract';

describe(CompositeLedgerProcessor.name, () => {
    const target = new CompositeLedgerProcessor();

    describe('should match big map and deserialize sample value correctly', () => {
        for (const testContract of testContracts.filter(c => c.ledger) as MarkRequired<TestContract, 'ledger'>[]) {
            it(testContract.name, async () => {
                const ledgerBigMap = {
                    name: testContract.bigMapName,
                    keySchema: new MichelsonSchema(testContract.ledger.schema.args![0]!),
                    valueSchema: new MichelsonSchema(testContract.ledger.schema.args![1]!),
                } as BigMapInfo;

                const processorMatch = target.findProcessor([ledgerBigMap], testContract.address);
                const processor = processorMatch?.processor;

                expect(processor?.constructor.name).toBe(testContract.ledger.expectedProcessor?.name);

                if (testContract.ledger.sample) {
                    const key = {
                        michelson: testContract.ledger.sample.input.key,
                        convert: () => ledgerBigMap.keySchema.execute(testContract.ledger.sample!.input.key),
                    } as LazyMichelsonValue;
                    const value = {
                        michelson: testContract.ledger.sample.input.value,
                        convert: () => ledgerBigMap.valueSchema.execute(testContract.ledger.sample!.input.value),
                    } as LazyMichelsonValue;
                    const context = {} as TokenBigMapUpdateIndexingContext;

                    const updates: RawLedgerUpdate[] = [];
                    for await (const rawLedgerUpdate of processor!.deserializeRawUpdates(key, value, context)) {
                        updates.push(rawLedgerUpdate);
                    }

                    expect(updates).toEqual(testContract.ledger.sample.expected);
                }
            });
        }
    });
});
