import { LogLevel } from '@tezos-dappetizer/utils';

import { TestLogger } from '../../../../test-utilities/mocks';
import { TokenIndexerInternalConfig } from '../../src/config/token-indexer-internal-config';
import { IncompatibleContractReporter } from '../../src/indexers/incompatible-contract-reporter';

describe(IncompatibleContractReporter.name, () => {
    let logger: TestLogger;
    let target: IncompatibleContractReporter;

    beforeEach(() => {
        logger = new TestLogger();
    });

    it('should report incompatible contract once', () => {
        const config = { logIncompatibleContracts: true, logIncompatibleContractsThreshold: 5 } as TokenIndexerInternalConfig;
        target = new IncompatibleContractReporter(config, logger);

        for (let x = 1; x <= 7; x++) {
            target.report('address', 'mint', {});
        }

        logger.loggedSingle(LogLevel.Warning)
            .verifyData({
                contract: 'address',
                feature: 'mint',
                schema: '{}',
            })
            .verifyMessage('No matching processor found');
    });

    it('should not report incompatible contract - reporting turned off', () => {
        const config = { logIncompatibleContracts: false, logIncompatibleContractsThreshold: 5 } as TokenIndexerInternalConfig;
        target = new IncompatibleContractReporter(config, logger);

        for (let x = 1; x <= 6; x++) {
            target.report('address', 'mint', {});
        }

        logger.verifyNothingLogged();
    });

    it('should not report incompatible contract - threshold not reached', () => {
        const config = { logIncompatibleContracts: true, logIncompatibleContractsThreshold: 5 } as TokenIndexerInternalConfig;
        target = new IncompatibleContractReporter(config, logger);

        for (let x = 1; x <= 4; x++) {
            target.report('address', 'mint', {});
        }

        logger.verifyNothingLogged();
    });
});
