import { Contract, MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { getConstructorName } from '@tezos-dappetizer/utils';
import BigNumber from 'bignumber.js';
import { Counter } from 'prom-client';
import { anything, instance, mock, when } from 'ts-mockito';

import { describeMember } from '../../../../../../test-utilities/mocks';
import { TokenIndexerMetrics } from '../../../../src/helpers/token-indexer-metrics';
import { ContractFeature, IndexedChangeType } from '../../../../src/indexed-data';
import { EntrypointData } from '../../../../src/indexers/entrypoints/entrypoint-indexer';
import { EntrypointProcessor } from '../../../../src/indexers/entrypoints/entrypoint-processor';
import { CompositeMintProcessor } from '../../../../src/indexers/entrypoints/mint/composite-mint-processor';
import { MintIndexer } from '../../../../src/indexers/entrypoints/mint/mint-indexer';
import { RawMint } from '../../../../src/indexers/entrypoints/mint/raw-mint';
import { IncompatibleContractReporter } from '../../../../src/indexers/incompatible-contract-reporter';
import { PartialIndexedChange } from '../../../../src/indexers/partial-token-indexer';
import { TokenTransactionIndexingContext } from '../../../../src/token-indexing-data';

describe(MintIndexer.name, () => {
    let target: MintIndexer;
    let compositeProcessor: CompositeMintProcessor;
    let reporter: IncompatibleContractReporter;
    let metrics: TokenIndexerMetrics;
    let processor: EntrypointProcessor<RawMint>;

    beforeEach(() => {
        [compositeProcessor, reporter, metrics, processor] = [mock(), mock(), mock(), mock()];
        target = new MintIndexer(instance(compositeProcessor), instance(reporter), instance(metrics));

        when(metrics.processorUsage).thenReturn(instance(mock<Counter>()));
        when(compositeProcessor.findProcessor(anything(), anything())).thenReturn(instance(processor));
    });

    describeMember<typeof target>('createIndexedChange', () => {
        it('should create indexed change', () => {
            const rawMint = {
                tokenId: new BigNumber(1),
                toAddress: 'tk1',
                amount: new BigNumber(55),
            } as RawMint;

            const currentChange = target.createIndexedChange(rawMint);

            const expectedChange = {
                type: IndexedChangeType.Mint,
                tokenId: rawMint.tokenId,
                toAddress: rawMint.toAddress,
                amount: rawMint.amount,
            };

            expect(currentChange).toEqual(expectedChange);
        });
    });

    describeMember<typeof target>('shouldIndex', () => {
        it('should recognize that contract should be indexed with corresponding features', () => {
            when(processor.contractFeatures).thenReturn([
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
            ]);

            const processorName = getConstructorName(instance(processor));

            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        mint: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            const currentShouldIndex: any = target.shouldIndex(contract);

            const expectedContractFeatures = [
                ContractFeature.Mint,
                'SomeAssetType' as ContractFeature,
                'SomeFAType' as ContractFeature,
                processorName as ContractFeature,
            ];

            expect(currentShouldIndex.contractFeatures).toEqual(expectedContractFeatures);
            expect(currentShouldIndex.data.toJSON()).toEqual({ entrypoint: 'mint', processor: processorName });
        });

        it('should recognize that contract should not be indexed - no entrypoint', () => {
            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        custom_mint: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            expect(target.shouldIndex(contract)).toBe(false);
        });

        it('should recognize that contract should not be indexed - no processor', () => {
            when(compositeProcessor.findProcessor(anything(), anything())).thenReturn(null);

            const contract = {
                parameterSchemas: {
                    entrypoints: {
                        mint: {} as MichelsonSchema,
                    } as Record<string, MichelsonSchema>,
                },
            } as Contract;

            expect(target.shouldIndex(contract)).toBe(false);
        });
    });

    describeMember<typeof target>('indexTransaction', () => {
        it('should index entrypoint', () => {
            const rawMints: RawMint[] = [
                {
                    tokenId: new BigNumber(1),
                    toAddress: 'tk1',
                    amount: new BigNumber(55),
                }, {
                    tokenId: new BigNumber(3),
                    toAddress: 'tk2',
                    amount: new BigNumber(122),
                },
            ];

            when(processor.deserializeRawChanges(anything(), anything()))
                .thenReturn(rawMints);

            const transactionParameter = {
                entrypoint: 'mint',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'mint',
                    processor: instance(processor),
                } as EntrypointData<RawMint>,
            } as TokenTransactionIndexingContext<EntrypointData<RawMint>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));

            const expectedChanges: PartialIndexedChange[] = rawMints.map(c => {
                return {
                    type: IndexedChangeType.Mint,
                    tokenId: c.tokenId,
                    toAddress: c.toAddress,
                    amount: c.amount,
                } as PartialIndexedChange;
            });

            expect(currentChanges).toEqual(expectedChanges);
        });

        it('should not index entrypoint - unknown entrypoint', () => {
            const transactionParameter = {
                entrypoint: 'custom_mint',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'mint',
                } as EntrypointData<RawMint>,
            } as TokenTransactionIndexingContext<EntrypointData<RawMint>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));
            expect(currentChanges).toBeEmpty();
        });

        it('should not index entrypoint - no changes', () => {
            when(processor.deserializeRawChanges(anything(), anything()))
                .thenReturn([]);

            const transactionParameter = {
                entrypoint: 'mint',
            } as TransactionParameter;

            const context = {
                contractData: {
                    entrypoint: 'mint',
                    processor: instance(processor),
                } as EntrypointData<RawMint>,
            } as TokenTransactionIndexingContext<EntrypointData<RawMint>>;

            const currentChanges = Array.from(target.indexTransaction(transactionParameter, context));
            expect(currentChanges).toBeEmpty();
        });
    });
});
