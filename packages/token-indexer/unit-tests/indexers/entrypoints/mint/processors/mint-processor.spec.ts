import BigNumber from 'bignumber.js';

import { describeMember } from '../../../../../../../test-utilities/mocks';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../../src/helpers/token-constants';
import { MintProcessor } from '../../../../../src/indexers/entrypoints/mint/processors/mint-processor';
import { shouldDeserializeIf, shouldProcessIf, shouldThrowIf } from '../../entrypoint-processor-test-helpers';

describe(MintProcessor.name, () => {
    const target = new MintProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        shouldProcessIf('schema corresponds', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%something'] },
                    { prim: 'address', annots: ['%to'] },
                ],
            },
            expectedProcessing: true,
        });

        shouldProcessIf('schema is missing "to" field', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%something'] },
                    { prim: 'address', annots: ['%from'] },
                ],
            },
            expectedProcessing: false,
        });
    });

    describeMember<typeof target>('deserializeRawChanges', () => {
        shouldThrowIf('missing parameter "to" field', target, {
            transactionParam: { amount: new BigNumber(122) },
        });

        shouldDeserializeIf('multi asset contract', target, {
            transactionParam: {
                to: 'mockedAddr',
                amount: new BigNumber(122),
                token_id: new BigNumber(2),
            },
            expectedChanges: [{
                tokenId: new BigNumber(2),
                toAddress: 'mockedAddr',
                amount: new BigNumber(122),
            }],
        });

        shouldDeserializeIf('single or nft asset contract', target, {
            transactionParam: {
                to: 'mockedAddr',
            },
            expectedChanges: [{
                toAddress: 'mockedAddr',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: NFT_DEFAULT_AMOUNT,
            }],
        });

        shouldDeserializeIf('nft asset contract with defaulting to sender address', target, {
            transactionParam: {
                to: null,
                token_id: new BigNumber(2),
            },
            context: { operationWithResult: { sourceAddress: 'mockedSrcAddr' } },
            expectedChanges: [{
                tokenId: new BigNumber(2),
                toAddress: 'mockedSrcAddr',
                amount: NFT_DEFAULT_AMOUNT,
            }],
        });
    });
});
