import { Contract, LazyMichelsonValue, Michelson, MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { NonNullish } from '@tezos-dappetizer/utils';
import { toArray } from 'iter-tools';
import { mapValues } from 'lodash';
import { DeepPartial } from 'ts-essentials';
import { instance, mock, when } from 'ts-mockito';

import { expectToThrow } from '../../../../../test-utilities/mocks';
import { ContractType, ContractTypeResolver } from '../../../src/indexers/contract-type-resolver';
import { EntrypointProcessor } from '../../../src/indexers/entrypoints/entrypoint-processor';
import { TokenTransactionIndexingContext } from '../../../src/token-indexing-data';

export function shouldProcessIf(
    testDesc: string,
    target: EntrypointProcessor<unknown>,
    testCase: {
        parameterSchema: Michelson;
        expectedProcessing: boolean;
    },
) {
    it(`should return ${testCase.expectedProcessing} if ${testDesc}`, () => {
        const schema = new MichelsonSchema(testCase.parameterSchema);

        const shouldProcess = target.shouldProcess(schema, 'KT1WvzYHCNBvDSdwafTHv7nJ1dWmZ8GCYuuC');

        expect(shouldProcess).toBe(testCase.expectedProcessing);
    });
}

export function shouldThrowIf(
    testDesc: string,
    target: EntrypointProcessor<unknown>,
    testCase: { transactionParam: object },
) {
    it(`should throw if ${testDesc}`, () => {
        const transactionParamValue = mock<LazyMichelsonValue>();
        const transactionParam = { value: instance(transactionParamValue) } as TransactionParameter;
        const context = {} as TokenTransactionIndexingContext;
        when(transactionParamValue.convert<NonNullish>()).thenReturn(testCase.transactionParam);

        expectToThrow(() => toArray(target.deserializeRawChanges(transactionParam, context)));
    });
}

export function shouldDeserializeIf<TRawChange>(
    testDesc: string,
    target: EntrypointProcessor<TRawChange>,
    testCase: {
        transactionParam: object;
        expectedChanges: TRawChange[];
        context?: DeepPartial<TokenTransactionIndexingContext>;
    },
) {
    it(`should deserialize correctly if ${testDesc}`, () => {
        const transactionParamValue = mock<LazyMichelsonValue>();
        const transactionParam = { value: instance(transactionParamValue) } as TransactionParameter;
        const context = (testCase.context ?? {}) as unknown as TokenTransactionIndexingContext;
        when(transactionParamValue.convert<NonNullish>()).thenReturn(testCase.transactionParam);

        const changes = toArray(target.deserializeRawChanges(transactionParam, context));

        expect(changes).toEqual(testCase.expectedChanges);
    });
}

export function shouldResolveTypeIf(
    testDesc: string,
    target: ContractTypeResolver,
    testCase: {
        entrypointSchemas: Record<string, Michelson>;
        expectedType: ContractType | null;
    },
) {
    it(`should return ${testCase.expectedType} if ${testDesc}`, () => {
        const contract = {
            parameterSchemas: {
                entrypoints: mapValues(testCase.entrypointSchemas, s => new MichelsonSchema(s)),
            },
        } as Contract;

        const contractType = target.resolveContractType(contract);

        expect(contractType).toBe(testCase.expectedType);
    });
}
