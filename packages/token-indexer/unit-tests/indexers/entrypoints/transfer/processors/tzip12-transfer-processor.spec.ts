import BigNumber from 'bignumber.js';
import { toArray } from 'iter-tools';

import { describeMember } from '../../../../../../../test-utilities/mocks';
import { ContractType } from '../../../../../src/indexers/contract-type-resolver';
import {
    Tzip12TransferProcessor,
} from '../../../../../src/indexers/entrypoints/transfer/processors/tzip12-transfer-processor';
import { RawTransfer } from '../../../../../src/indexers/entrypoints/transfer/transfer-processor-no-annots';
import { shouldProcessIf, shouldResolveTypeIf } from '../../entrypoint-processor-test-helpers';

describe(Tzip12TransferProcessor.name, () => {
    const target = new Tzip12TransferProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        shouldProcessIf('schema matches', target, {
            parameterSchema: {
                prim: 'list',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'address', annots: ['%from'] },
                            {
                                prim: 'list',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'address', annots: ['%to'] },
                                            {
                                                prim: 'pair',
                                                args: [
                                                    { prim: 'nat', annots: ['%token_id'] },
                                                    { prim: 'nat', annots: ['%amount'] },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                                annots: ['%destinations'],
                            },
                        ],
                    },
                ],
            },
            expectedProcessing: true,
        });

        shouldProcessIf('schema does not match', target, {
            parameterSchema: {
                prim: 'list',
                args: [
                    {
                        prim: 'pair',
                        args: [
                            { prim: 'address', annots: ['%from'] },
                            {
                                prim: 'list',
                                args: [
                                    {
                                        prim: 'pair',
                                        args: [
                                            { prim: 'address', annots: ['%to'] },
                                            {
                                                prim: 'pair',
                                                args: [
                                                    { prim: 'address', annots: ['%something'] },
                                                    { prim: 'nat', annots: ['%token_id'] },
                                                    { prim: 'nat', annots: ['%amount'] },
                                                ],
                                            },
                                        ],
                                    },
                                ],
                                annots: ['%destinations'],
                            },
                        ],
                    },
                ],
            },
            expectedProcessing: false,
        });
    });

    describeMember<typeof target>('deserializeRawChangesFromNoAnnots', () => {
        it('should deserialize raw changes', () => {
            const transferDtos = [{
                0: 'mockedFromAddr',
                1: [
                    {
                        0: 'mockedToAddr',
                        1: new BigNumber(2),
                        2: new BigNumber(55),
                    },
                ],
            }];

            const currentTransfers = toArray(target.deserializeRawChangesFromNoAnnots(transferDtos, null!));

            expect(currentTransfers).toEqual<RawTransfer[]>([{
                tokenId: new BigNumber(2),
                fromAddress: 'mockedFromAddr',
                toAddress: 'mockedToAddr',
                amount: new BigNumber(55),
            }]);
        });
    });

    describeMember<typeof target>('resolveContractType', () => {
        shouldResolveTypeIf('valid', target, {
            entrypointSchemas: {
                transfer: {
                    prim: 'list',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%from'] },
                                {
                                    prim: 'list',
                                    args: [
                                        {
                                            prim: 'pair',
                                            args: [
                                                { prim: 'address', annots: ['%to'] },
                                                {
                                                    prim: 'pair',
                                                    args: [
                                                        { prim: 'nat', annots: ['%token_id'] },
                                                        { prim: 'nat', annots: ['%amount'] },
                                                    ],
                                                },
                                            ],
                                        },
                                    ],
                                    annots: ['%destinations'],
                                },
                            ],
                        },
                    ],
                },
            },
            expectedType: ContractType.FA2_like,
        });

        shouldResolveTypeIf('unknown entrypoint', target, {
            entrypointSchemas: {
                custom_transfer: {
                    prim: 'list',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%from'] },
                                {
                                    prim: 'list',
                                    args: [
                                        {
                                            prim: 'pair',
                                            args: [
                                                { prim: 'address', annots: ['%to'] },
                                                {
                                                    prim: 'pair',
                                                    args: [
                                                        { prim: 'nat', annots: ['%token_id'] },
                                                        { prim: 'nat', annots: ['%amount'] },
                                                    ],
                                                },
                                            ],
                                        },
                                    ],
                                    annots: ['%destinations'],
                                },
                            ],
                        },
                    ],
                },
            },
            expectedType: null,
        });

        shouldResolveTypeIf('incompatible schema', target, {
            entrypointSchemas: {
                transfer: {
                    prim: 'list',
                    args: [
                        {
                            prim: 'pair',
                            args: [
                                { prim: 'address', annots: ['%from'] },
                                {
                                    prim: 'list',
                                    args: [
                                        {
                                            prim: 'pair',
                                            args: [
                                                { prim: 'address', annots: ['%to'] },
                                                {
                                                    prim: 'pair',
                                                    args: [
                                                        { prim: 'address', annots: ['%something'] },
                                                        { prim: 'nat', annots: ['%token_id'] },
                                                        { prim: 'nat', annots: ['%amount'] },
                                                    ],
                                                },
                                            ],
                                        },
                                    ],
                                    annots: ['%destinations'],
                                },
                            ],
                        },
                    ],
                },
            },
            expectedType: null,
        });
    });
});
