import BigNumber from 'bignumber.js';

import { describeMember } from '../../../../../../../test-utilities/mocks';
import { NFT_DEFAULT_AMOUNT, SINGLE_ASSET_TOKEN_ID } from '../../../../../src/helpers/token-constants';
import { ListBurnProcessor } from '../../../../../src/indexers/entrypoints/burn/processors/list-burn-processor';
import { shouldDeserializeIf, shouldProcessIf, shouldThrowIf } from '../../entrypoint-processor-test-helpers';

describe(ListBurnProcessor.name, () => {
    const target = new ListBurnProcessor();

    describeMember<typeof target>('shouldProcess', () => {
        shouldProcessIf('schema corresponds', target, {
            parameterSchema: {
                prim: 'list',
                args: [{
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%something'] },
                        { prim: 'address', annots: ['%from'] },
                    ],
                }],
            },
            expectedProcessing: true,
        });

        shouldProcessIf('schema is not list', target, {
            parameterSchema: {
                prim: 'pair',
                args: [
                    { prim: 'address', annots: ['%something'] },
                    { prim: 'address', annots: ['%from'] },
                ],
            },
            expectedProcessing: false,
        });

        shouldProcessIf('schema is missing "from" field', target, {
            parameterSchema: {
                prim: 'list',
                args: [{
                    prim: 'pair',
                    args: [
                        { prim: 'address', annots: ['%something'] },
                        { prim: 'address', annots: ['%to'] },
                    ],
                }],
            },
            expectedProcessing: false,
        });
    });

    describeMember<typeof target>('deserializeRawChanges', () => {
        shouldThrowIf('missing parameter "from" field', target, {
            transactionParam: [{ amount: new BigNumber(122) }],
        });

        shouldDeserializeIf('multi asset contract', target, {
            transactionParam: [
                {
                    from: 'mockedAddr1',
                    token_id: new BigNumber(1),
                    amount: new BigNumber(111),
                },
                {
                    from: 'mockedAddr2',
                    token_id: new BigNumber(2),
                    amount: new BigNumber(222),
                },
            ],
            expectedChanges: [
                {
                    fromAddress: 'mockedAddr1',
                    tokenId: new BigNumber(1),
                    amount: new BigNumber(111),
                },
                {
                    fromAddress: 'mockedAddr2',
                    tokenId: new BigNumber(2),
                    amount: new BigNumber(222),
                },
            ],
        });

        shouldDeserializeIf('single or nft asset contract', target, {
            transactionParam: [{
                from: 'mockedAddr',
            }],
            expectedChanges: [{
                fromAddress: 'mockedAddr',
                tokenId: SINGLE_ASSET_TOKEN_ID,
                amount: NFT_DEFAULT_AMOUNT,
            }],
        });
    });
});
