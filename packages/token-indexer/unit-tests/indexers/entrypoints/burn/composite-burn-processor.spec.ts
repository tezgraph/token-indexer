import { MichelsonSchema, TransactionParameter } from '@tezos-dappetizer/indexer';
import { MarkRequired } from 'ts-essentials';

import { CompositeBurnProcessor } from '../../../../src/indexers/entrypoints/burn/composite-burn-processor';
import { TokenTransactionIndexingContext } from '../../../../src/token-indexing-data';
import { testContracts } from '../../../tested-contracts';
import { TestContract } from '../../../tested-contracts/test-contract';

describe(CompositeBurnProcessor.name, () => {
    const target = new CompositeBurnProcessor();

    describe('should match schema and deserialize sample value correctly', () => {
        for (const testContract of testContracts.filter(c => c.burn) as MarkRequired<TestContract, 'burn'>[]) {
            it(testContract.name, () => {
                const schema = new MichelsonSchema(testContract.burn.schema);
                const processor = target.findProcessor(schema, testContract.address);

                expect(processor?.constructor.name).toBe(testContract.burn.expectedProcessor?.name);

                if (testContract.burn.sample) {
                    const transactionParameter = {
                        value: {
                            michelson: testContract.burn.sample.input,
                            convert: () => {
                                return schema.execute<any>(testContract.burn.sample!.input);
                            },
                        },
                    } as TransactionParameter;
                    const context = {
                        operationWithResult: { sourceAddress: 'operation result parent source' },
                        mainOperation: { sourceAddress: 'operation source' },
                    } as TokenTransactionIndexingContext;

                    const param = Array.from(processor!.deserializeRawChanges(transactionParameter, context));

                    expect(param).toEqual(testContract.burn.sample.expected);
                }
            });
        }
    });
});
